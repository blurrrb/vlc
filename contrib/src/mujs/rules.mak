# MuJS 1.0.5

MUJS_VERSION := 1.0.5
MUJS_URL := https://mujs.com/downloads/mujs-$(MUJS_VERSION).tar.gz

$(TARBALLS)/mujs-$(MUJS_VERSION).tar.gz:
	$(call download_pkg,$(MUJS_URL),mujs)

.sum-mujs: mujs-$(MUJS_VERSION).tar.gz

mujs: mujs-$(MUJS_VERSION).tar.gz .sum-mujs
	$(UNPACK)
	$(APPLY) $(SRC)/mujs/fpic.patch
	$(MOVE)

.mujs: mujs
	cd $< && $(MAKE) prefix=$(PREFIX) install-static
	touch $@

/*****************************************************************************
 * js.c: entry point for js module
 *****************************************************************************
 * Copyright (C) 2009-2019 VideoLAN and authors
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>,
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
 
#include <stdlib.h>
#include "js.h"

/*****************************************************************************
 * Module descriptor
 *****************************************************************************/
#define INTF_TEXT N_("JS Interface")
#define INTF_LONGTEXT N_("JS interface module to load")

#define CONFIG_TEXT N_("JS interface configuration")
#define CONFIG_LONGTEXT N_("JS interface configuration string. Format is: '[\"<interface module name>\"] = { <option> = <value>, ...}, ...'.")
#define PASS_TEXT N_( "Password" )
#define PASS_LONGTEXT N_( "A single password restricts access to this interface." )
#define SRC_TEXT N_( "Source directory" )
#define SRC_LONGTEXT N_( "Source directory" )
#define INDEX_TEXT N_( "Directory index" )
#define INDEX_LONGTEXT N_( "Allow to build directory index" )

#define TELNETHOST_TEXT N_( "Host" )
#define TELNETHOST_LONGTEXT N_( "This is the host on which the " \
    "interface will listen. It defaults to all network interfaces (0.0.0.0)." \
    " If you want this interface to be available only on the local " \
    "machine, enter \"127.0.0.1\"." )
#define TELNETPORT_TEXT N_( "Port" )
#define TELNETPORT_LONGTEXT N_( "This is the TCP port on which this " \
    "interface will listen. It defaults to 4212." )
#define TELNETPWD_TEXT N_( "Password" )
#define TELNETPWD_LONGTEXT N_( "A single password restricts access " \
    "to this interface." )
#define RCHOST_TEXT N_("TCP command input")
#define RCHOST_LONGTEXT N_("Accept commands over a socket rather than stdin. " \
            "You can set the address and port the interface will bind to." )
#define CLIHOST_TEXT N_("CLI input")
#define CLIHOST_LONGTEXT N_( "Accept commands from this source. " \
    "The CLI defaults to stdin (\"*console\"), but can also bind to a " \
    "plain TCP socket (\"localhost:4212\") or use the telnet protocol " \
    "(\"telnet://0.0.0.0:4212\")" )
 
/*****************************************
 * Registering modules with VLC
 *****************************************/
vlc_module_begin ()
        set_shortname( N_("JS") )
        set_description( N_("JS Interpreter") )
        set_category( CAT_INTERFACE )
        set_subcategory( SUBCAT_INTERFACE_MAIN )

        add_string( "js-intf", "interpreter", INTF_TEXT, INTF_LONGTEXT, false )
        add_string( "js-config", "", CONFIG_TEXT, CONFIG_LONGTEXT, false )
        set_capability( "interface", 0 )
        set_callbacks( Open_JS_Intf, Close_JS_Intf )
        add_shortcut( "JS Interpreter" )

    add_submodule ()
        add_shortcut( "jsplaylist" )
        set_shortname( N_("JS Playlist") )
        set_description( N_("JS Playlist Parser Interface") )
        set_capability( "stream_filter", 302 )
        set_callbacks( Import_JSPlaylist, Close_JSPlaylist )


    add_submodule ()
        set_shortname( N_("JS Extension") )
        set_description( N_("JS Extension") )
        add_shortcut( "jsextension" )
        set_capability( "extension", 1 )
        set_callbacks( Open_Extension, Close_Extension )

vlc_module_end ()


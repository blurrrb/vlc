﻿# Integrate JS in VLC
I am Aakash Singh, an undergraduate student at NITK, Surathkal, India and this is the project I worked on as part of GSoC 2019 and VLC.
This project is aimed at integrating a JS engine into VLC to enable scripting support and extensions inside VLC in JS. The project is complete with a playlist parser and a youtube parse script written in JS to demonstrate the playlist parser module. The extension module is also available.

## Code structure
This folder contains the module's source code. The libs folder contains the vlcjs API calls, their implementation and mechanism to register these calls inside the JS runtime.
Other files in this folder include the entry point for the interpreter module, extensions module and the playlist parser module along with an entry point file (js.c) to inform VLC about the available modules.
The youtube parser script is available in ../share/js/playlist relative to this folder.
Other modifications include some code in contribs folder in root folder to simplify installating of MuJS and some code in configure.ac in root folder to tell VLC to check for MuJS library availability before proceeding to compile any module which requires the MuJS library. This also enable support for the '--enable-js' and '--disable-js' flags.


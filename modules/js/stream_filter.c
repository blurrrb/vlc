/*****************************************************************************
 * stream_filter.c :  JS playlist stream filter module
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>,
 *          Thomas Guillem <tguillem at videolan dot org>,
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#include "libs/general.h"
#include <vlc_access.h>

/*****************************************************************************
 * Demux specific functions
 *****************************************************************************/
struct vlcjs_playlist
{
    js_State *J;
    char *filename;
    char *access;
    const char *path;
};

static void vlcjs_demux_peek( js_State *J )
{
    stream_t *s = (stream_t *) vlcjs_get_this(J);
    int n = js_tointeger( J, 1 );
    const uint8_t *p_peek;

    ssize_t val = vlc_stream_Peek(s->s, &p_peek, n);
    if (val > 0)
        js_pushlstring(J, (const char *)p_peek, val);
    else
        js_pushnull( J );
}

static void vlcjs_demux_read( js_State *J )
{
    stream_t *s = (stream_t *)vlcjs_get_this(J);
    int n = js_tointeger( J, 1 );
    char *buf = malloc(n);

    if (buf != NULL)
    {
        ssize_t val = vlc_stream_Read(s->s, buf, n);
        if (val > 0)
            js_pushlstring(J, buf, val);
        else
            js_pushnull(J);
        free(buf);
    }
    else
        js_pushnull(J);
}

static void vlcjs_demux_readline( js_State *J )
{
    stream_t *s = (stream_t *)vlcjs_get_this(J);
    char *line = vlc_stream_ReadLine(s->s);

    if (line != NULL)
    {
        js_pushstring(J, line);
        free(line);
    }
    else
        js_pushnull(J);
}

/*****************************************************************************
 * Register vlcjs functions
 *****************************************************************************/
/* Functions to register */
static const vlcjs_Reg p_reg[] =
{
    { "peek", vlcjs_demux_peek },
    { NULL, NULL }
};

/* Functions to register for parse() function call only */
static const vlcjs_Reg p_reg_parse[] =
{
    { "read", vlcjs_demux_read },
    { "readline", vlcjs_demux_readline },
    { NULL, NULL }
};

/*****************************************************************************
 * Called through vlsjs_scripts_batch_execute to call 'probe' on
 * the script pointed by psz_filename.
 *****************************************************************************/
static int probe_jsscript(vlc_object_t *obj, const char *filename)
{
    stream_t *s = (stream_t *)obj;
    struct vlcjs_playlist *sys = s->p_sys;

    /* Initialise JS state structure */
    js_State *J = js_newstate(NULL, NULL, JS_STRICT);
    if( !J )
        return VLC_ENOMEM;

    sys->J = J;

    vlcjs_set_this(J, s);

    js_newobject( J );
    js_setglobal( J, "vlc" );

    vlcjs_register_msg( J );
    vlcjs_register_io( J );
    vlcjs_register_strings( J );
    vlcjs_register_stream( J );
    vlcjs_register_xml( J );
    
    js_getglobal(J, "vlc");
    if (sys->path != NULL)
        js_pushstring(J, sys->path);
    else
        js_pushnull(J);
    js_setproperty( J, -2, "path" );

    if (sys->access != NULL)
        js_pushstring(J, sys->access);
    else
        js_pushnull(J);
    js_setproperty( J, -2, "access" );

    js_pop( J, 1 );

    /* Load and run the script(s) */
    if (vlcjs_dofile(VLC_OBJECT(s), J, filename))
    {
        msg_Warn(obj, "error loading script %s: %s", filename,
                 js_tostring(J, -1));
        goto error;
    }

    js_getglobal( J, "probe" );
    if( !js_iscallable( J, -1 ) )
    {
        msg_Warn(obj, "error running script %s: function %s(): %s",
                 filename, "probe", "not found");
        js_pop( J, 1 );
        goto error;
    }
    js_pushnull(J); // push the "this" to the stack, to be used by "probe"

    if( js_pcall( J, 0 ) )
    {
        msg_Warn(obj, "error running script %s: function %s(): %s",
                 filename, "probe", js_tostring(J, -1));
        goto error;
    }

    if( js_gettop( J ) )
    {
        if( js_toboolean( J, -1 ) )
        {
            msg_Dbg(obj, "JS playlist script %s's "
                    "probe() function was successful", filename );
            js_pop( J, 1 );
            sys->filename = strdup(filename);
            if( !sys->filename ) goto error;
            return VLC_SUCCESS;
        }
    }

error:
    js_freestate(sys->J);
    return VLC_EGENERIC;
}

static int ReadDir(stream_t *s, input_item_node_t *node)
{
    struct vlcjs_playlist *sys = s->p_sys;
    js_State *J = sys->J;

    js_getglobal(J, "vlc");
    vlcjs_register(J, p_reg_parse);
    js_pop(J, 1);

    js_getglobal( J, "parse" );

    if( !js_iscallable( J, -1 ) )
    {
        msg_Warn(s, "error running script %s: function %s(): %s",
                 sys->filename, "parse", "not found");
        return VLC_ENOITEM;
    }

    js_pushundefined(J);

    if( js_pcall(J, 0) )
    {
        msg_Warn(s, "error running script %s: function %s(): %s",
                 sys->filename, "parse", js_tostring(J, -1));
        return VLC_ENOITEM;
    }
    
    if (!js_gettop(J))
    {
        msg_Err(s, "script went completely foobar");
        return VLC_ENOITEM;
    }
    if (!js_isobject(J, -1))
    {
        msg_Warn(s, "Playlist should be an object. js_isobject() = %d", js_isobject(J, -1));
        return VLC_ENOITEM;
    }

    js_pushiterator(J, -1, 0);
    char *key;
    /* playlist nil */
    while ( (key = js_nextiterator(J, -1)) != NULL )
    {
        js_getproperty(J, -2, key);
        input_item_t *item = vlcjs_read_input_item(VLC_OBJECT(s), J);
        if (item != NULL)
        {
            /* copy the original URL to the meta data,
             * if "URL" is still empty */
            char *url = input_item_GetURL(item);
            if (url == NULL && s->psz_url != NULL)
                input_item_SetURL(item, s->psz_url);
            free(url);

            input_item_node_AppendItem(node, item);
            input_item_Release(item);
        }
        js_pop(J, 1);
    }
    /* playlist */
    js_pop(J, 2);

    return VLC_SUCCESS;
}

/*****************************************************************************
 * Import_JSPlaylist: main import function
 *****************************************************************************/
int Import_JSPlaylist(vlc_object_t *obj)
{
    stream_t *s = (stream_t *)obj;
    if( s->s->pf_readdir != NULL )
        return VLC_EGENERIC;

    struct vlcjs_playlist *sys = malloc(sizeof (*sys));
    if (unlikely(sys == NULL))
        return VLC_ENOMEM;

    s->p_sys = sys;
    sys->access = NULL;
    sys->path = NULL;

    if (s->psz_url != NULL)
    {
        const char *p = strstr(s->psz_url, "://");
        if (p != NULL)
        {
            sys->access = strndup(s->psz_url, p - s->psz_url);
            sys->path = p + 3;
        }
    }

    int ret = vlcjs_scripts_batch_execute(VLC_OBJECT(s), "playlist",
                                           probe_jsscript);
    if (ret != VLC_SUCCESS)
    {
        free(sys->access);
        free(sys);
        return ret;
    }

    s->pf_readdir = ReadDir;
    s->pf_control = access_vaDirectoryControlHelper;
    return VLC_SUCCESS;
}

void Close_JSPlaylist(vlc_object_t *obj)
{
    stream_t *s = (stream_t *)obj;
    struct vlcjs_playlist *sys = s->p_sys;

    free(sys->filename);
    js_freestate(sys->J);
    free(sys->access);
    free(sys);
}

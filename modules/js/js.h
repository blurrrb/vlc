/*****************************************************************************
 * Copyright (C) 2009-2019 VideoLAN and authors
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>,
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/******************************************************
 * header files on which the api depends
 ******************************************************/
#include <mujs.h>
#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_interface.h>


/******************************************************
 * module callbacks
 ******************************************************/

int Open_JS_Intf( vlc_object_t * );
void Close_JS_Intf( vlc_object_t * );

int Import_JSPlaylist( vlc_object_t * );
void Close_JSPlaylist( vlc_object_t * );

int Open_Extension( vlc_object_t * );
void Close_Extension( vlc_object_t * );


/******************************************************
 * File descriptors table
 ******************************************************/
typedef struct
{
    struct vlc_interrupt *interrupt;
    int *fdv;
    unsigned fdc;
} vlcjs_dtable_t;


/************************************************************
 * vlcjs libraries
 ************************************************************/
void vlcjs_register_config( js_State * );
void vlcjs_register_dialog( js_State *, void * );
void vlcjs_register_equalizer( js_State * );
void vlcjs_register_errno( js_State * );
void vlcjs_register_gettext( js_State * );
void vlcjs_register_io( js_State * );
void vlcjs_register_msg( js_State * );
void vlcjs_register_misc( js_State * );
void vlcjs_register_net( js_State *J, vlcjs_dtable_t *dt );
void vlcjs_register_objects( js_State * );
void vlcjs_register_osd( js_State * );
void vlcjs_register_playlist( js_State * );
void vlcjs_register_rand( js_State * );
void vlcjs_register_stream( js_State * );
void vlcjs_register_strings( js_State * );
void vlcjs_register_video( js_State * );
void vlcjs_register_vlm( js_State * );
void vlcjs_register_volume( js_State * );
void vlcjs_register_xml( js_State * );



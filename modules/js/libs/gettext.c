/*****************************************************************************
 * gettext.c
 *****************************************************************************
 * Copyright (C) 2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"

/*****************************************************************************
 * Libvlc gettext support
 *****************************************************************************/
static void vlcjs_gettext( js_State *J )
{
    js_pushstring( J, _( js_tostring( J, 1 ) ) );
}

static void vlcjs_gettext_noop( js_State *J )
{
    js_pushstring( J, N_( js_tostring( J, 1) ) );
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_gettext_reg[] = {
    { "_", vlcjs_gettext },
    { "N_", vlcjs_gettext_noop },

    { NULL, NULL }
};

void vlcjs_register_gettext( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_gettext_reg );
    js_setproperty( J, -2, "gettext" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

/*****************************************************************************
 * stream.c: stream functions
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Pierre d'Herbemont <pdherbemont # videolan.org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"

/*****************************************************************************
 * Stream handling
 *****************************************************************************/
static void vlcjs_stream_read( js_State * );
static void vlcjs_stream_readline( js_State * );
static void vlcjs_stream_delete__gc( js_State *, void * );
static void vlcjs_stream_add_filter( js_State *J );
static void vlcjs_stream_getsize( js_State *J );
static void vlcjs_stream_seek( js_State *J );

static const vlcjs_Reg vlcjs_stream_reg[] = {
    { "read", vlcjs_stream_read },
    { "readline", vlcjs_stream_readline },
    { "addfilter", vlcjs_stream_add_filter },
    { "getsize", vlcjs_stream_getsize },
    { "seek", vlcjs_stream_seek },
    { NULL, NULL }
};

static void vlcjs_stream_new_inner( js_State *J, stream_t *p_stream )
{
    if( !p_stream )
    {
        js_error( J, "Error when opening stream" );
        return;
    }

    stream_t **pp_stream = malloc( sizeof( stream_t * ) );
    if( !pp_stream ) return js_error( J, "memory error" );
    *pp_stream = p_stream;

    js_currentfunction( J );
    js_getproperty( J, -2, "prototype" );
    js_newuserdata( J, "stream", pp_stream, vlcjs_stream_delete__gc);
    vlcjs_register( J, vlcjs_stream_reg );
}

static void vlcjs_stream_new( js_State *J )
{
    vlc_object_t * p_this = vlcjs_get_this( J );
    const char * psz_url = js_tostring( J, 1 );
    stream_t *p_stream = vlc_stream_NewMRL( p_this, psz_url );
    vlcjs_stream_new_inner( J, p_stream );
}

static void vlcjs_memory_stream_new( js_State *J )
{
    vlc_object_t * p_this = vlcjs_get_this( J );
    /* FIXME: duplicating the whole buffer is suboptimal. Keeping a reference to the string so that it doesn't get garbage collected would be better */
    char * psz_content = strdup( js_tostring( J, 1 ) );
    if( !psz_content ){
        return js_error( J, "no memory" );
    }
    stream_t *p_stream = vlc_stream_MemoryNew( p_this, (uint8_t *)psz_content, strlen( psz_content ), false );
    vlcjs_stream_new_inner( J, p_stream );
}

static void vlcjs_directory_stream_new( js_State *J )
{
    vlc_object_t * p_this = vlcjs_get_this( J );
    const char * psz_url = js_tostring( J, 1 );
    stream_t *p_stream = vlc_stream_NewURL( p_this, psz_url );
    if( !p_stream )
    {
        js_error( J, "error creating stream" );
        return;
    }
    if( vlc_stream_directory_Attach( &p_stream, NULL ) != VLC_SUCCESS )
    {
        vlc_stream_Delete( p_stream );
        js_error( J, "error creating stream" );
        return;
    }
    vlcjs_stream_new_inner( J, p_stream );
}

static void vlcjs_stream_read( js_State *J )
{
    int i_read;
    stream_t **pp_stream = (stream_t **)js_touserdata( J, 0, "stream" );
    int n = js_tointeger( J, 1 );
    uint8_t *p_read = malloc( n );
    if( !p_read )
    {
        js_error( J, "memory error" );
        return;
    }

    i_read = vlc_stream_Read( *pp_stream, p_read, n );
    if( i_read > 0 )
        js_pushlstring( J, (const char *)p_read, i_read );
    else
        js_pushnull( J );
    free( p_read );
}

static void vlcjs_stream_readline( js_State *J )
{
    stream_t **pp_stream = (stream_t **)js_touserdata( J, 0, "stream" );
    char *psz_line = vlc_stream_ReadLine( *pp_stream );
    if( psz_line )
    {
        js_pushstring( J, psz_line );
        free( psz_line );
    }
    else
        js_pushnull( J );
}

static void vlcjs_stream_add_filter( js_State *J )
{
    vlc_object_t *p_this = vlcjs_get_this( J );

    stream_t **pp_stream = (stream_t **)js_touserdata( J, 0, "stream" );
    if( !*pp_stream ) return js_error( J, "no stream" ); //current function cant be called if pp_stream is NULL
    const char *psz_filter = NULL;

    if( js_gettop( J ) > 1 && js_isstring( J, 1 ) )
        psz_filter = js_tostring( J, 1 );

    if( !psz_filter || !*psz_filter )
    {
        msg_Dbg( p_this, "adding all automatic stream filters" );
        while( true )
        {
            /* Add next automatic stream */
            stream_t *p_filtered = vlc_stream_FilterNew( *pp_stream, NULL );
            if( !p_filtered )
                break;
            else
            {
                msg_Dbg( p_this, "inserted an automatic stream filter" );
                *pp_stream = p_filtered;
            }
        }
    }
    else
    {
        /* Add a named filter */
        stream_t *p_filter = vlc_stream_FilterNew( *pp_stream, psz_filter );
        if( !p_filter )
            msg_Dbg( p_this, "Unable to open requested stream filter '%s'",
                     psz_filter );
        else
        {
            *pp_stream = p_filter;
        }
    }

    js_pushundefined( J );
}

static void vlcjs_stream_getsize( js_State *J )
{
    stream_t **pp_stream = (stream_t **)js_touserdata( J, 0, "stream" );
    uint64_t i_size;
    int i_res = vlc_stream_GetSize( *pp_stream, &i_size );
    if ( i_res != 0 )
    {
        js_error( J, "Failed to get stream size" );
        return;
    }
    js_pushnumber( J, i_size );
}

static void vlcjs_stream_seek( js_State *J )
{
    stream_t **pp_stream = (stream_t **)js_touserdata( J, 0, "stream" );
    int i_offset = js_tointeger( J, 1 );
    if ( i_offset < 0 )
    {
        js_error( J, "Invalid negative seek offset" );
        return;
    }
    int i_res = vlc_stream_Seek( *pp_stream, (uint64_t)i_offset );
    js_pushboolean( J, i_res == 0 );
}

static void vlcjs_stream_delete__gc( js_State *J, void *data )
{
    stream_t **pp_stream = data;
    vlc_stream_Delete( *pp_stream );
}

/*****************************************************************************
 * 
 *****************************************************************************/
void vlcjs_register_stream( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newcfunction( J, vlcjs_stream_new, "stream", 0 );
    js_setproperty( J, -2, "stream" );
    js_newcfunction( J, vlcjs_memory_stream_new, "memory_stream", 0 );
    js_setproperty( J, -2, "memory_stream" );
    js_newcfunction( J, vlcjs_directory_stream_new, "directory_stream", 0 );
    js_setproperty( J, -2, "directory_stream" );
    js_pop( J, 1 ); // pop the vlc object

}

/*****************************************************************************
 * strings.c
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Pierre d'Herbemont <pdherbemont # videolan.org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"
#include <vlc_strings.h>
#include <vlc_url.h>
#include <vlc_xml.h>

/*****************************************************************************
 * String transformations
 *****************************************************************************/
static void vlcjs_decode_uri( js_State *J )
{
    const char *psz_cstring = js_tostring( J, 1 );
    char *psz_string = vlc_uri_decode_duplicate( psz_cstring );
    js_pushstring( J, psz_string );
    free( psz_string );
}

static void vlcjs_encode_uri_component( js_State *J )
{    
    const char *psz_cstring = js_tostring( J, 1 );
    char *psz_string = vlc_uri_encode( psz_cstring );
    js_pushstring( J, psz_string );
    free( psz_string );
}

static void vlcjs_make_uri( js_State *J )
{
    const char *psz_input = js_tostring( J, 1 );
    const char *psz_scheme;
    if( js_isdefined( J, 2 ) && js_isstring( J, 2 ) ) 
        psz_scheme = js_tostring( J, 2 );
    else 
        psz_scheme = NULL;

    if( strstr( psz_input, "://" ) == NULL )
    {
        char *psz_uri = vlc_path2uri( psz_input, psz_scheme );
        js_pushstring( J, psz_uri );
        free( psz_uri );
    }
    else
        js_pushstring( J, psz_input );
}

static void vlcjs_make_path( js_State *J )
{
    const char *psz_input = js_tostring( J, 1 );
    char *psz_path = vlc_uri2path( psz_input);
    js_pushstring( J, psz_path );
    free( psz_path );
}

static void vlcjs_url_parse( js_State *J )
{
    const char *psz_url = js_tostring( J, 1 );
    vlc_url_t url;

    vlc_UrlParse( &url, psz_url );

    js_newobject( J );
    {
        js_pushstring( J, url.psz_protocol );
        js_setproperty( J, -2, "protocol" );
        js_pushstring( J, url.psz_username );
        js_setproperty( J, -2, "username" );
        js_pushstring( J, url.psz_password );
        js_setproperty( J, -2, "password" );
        js_pushstring( J, url.psz_host );
        js_setproperty( J, -2, "host" );
        js_pushnumber( J, url.i_port );
        js_setproperty( J, -2, "port" );
        js_pushstring( J, url.psz_path );
        js_setproperty( J, -2, "path" );
        js_pushstring( J, url.psz_option );
        js_setproperty( J, -2, "option" );
    }

    vlc_UrlClean( &url );
}

static void vlcjs_resolve_xml_special_chars( js_State *J )
{
    const char *psz_cstring = js_tostring( J, 1 );
    char *psz_string = strdup( psz_cstring );

    if( !psz_string ) return js_error( J, "no memory" );

    vlc_xml_decode( psz_string );
    js_pushstring( J, psz_string );
    free( psz_string );
}

static void vlcjs_convert_xml_special_chars( js_State *J )
{
    char *psz_string = vlc_xml_encode( js_tostring( J, 1 ) );
    js_pushstring( J, psz_string );
    free( psz_string );
}

static void vlcjs_from_charset( js_State *J )
{
    if( js_gettop( J ) < 2 ) return js_error( J, "error" ); 

    const char *psz_input = js_tostring( J, 2 );
    size_t i_in_bytes = strlen( psz_input );
    if( i_in_bytes == 0 ) return js_error( J, "error" );

    const char *psz_charset = js_tostring( J, 1 );
    char *psz_output = FromCharset( psz_charset, psz_input, i_in_bytes );
    js_pushstring( J, psz_output ? psz_output : "" );
    free( psz_output );
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_strings_reg[] = {
    { "decode_uri", vlcjs_decode_uri },
    { "encode_uri_component", vlcjs_encode_uri_component },
    { "make_uri", vlcjs_make_uri },
    { "make_path", vlcjs_make_path },
    { "url_parse", vlcjs_url_parse },
    { "resolve_xml_special_chars", vlcjs_resolve_xml_special_chars },
    { "convert_xml_special_chars", vlcjs_convert_xml_special_chars },
    { "from_charset", vlcjs_from_charset },
    { NULL, NULL }
};

void vlcjs_register_strings( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_strings_reg );
    js_setproperty( J, -2, "strings" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

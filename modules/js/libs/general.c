/*****************************************************************
 * js <-> c functions
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Hugo Beauzée-Luyssen <hugo@beauzee.fr>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "general.h"

void vlcjs_set( js_State* J, void* obj, const char* name )
{
    js_pushundefined( J );
    js_newuserdata( J, "void*", obj, NULL);
    js_setregistry( J, name );
}


void* vlcjs_get( js_State* J, const char* name )
/**
 * get object named "name" from js state J, else return NULL if object unset
 */
{
    js_getregistry( J, name );
    void *p_data;
    if( !js_isundefined( J, -1 ) )
        p_data = js_touserdata( J, -1 , "void*" );
    else
        p_data = NULL;
    js_pop( J, 1 );  // leave stack in original state
    return p_data;
}


void vlcjs_set_this( js_State* J, void *this )
/**
 * set the "this" object in js state
 */
{
    vlcjs_set( J, this, "this");
}


vlc_object_t* vlcjs_get_this( js_State* J )
/**
 * get the "this" object from js state J, else return NULL
 */
{
    return (vlc_object_t*) vlcjs_get( J, "this");
}

vlc_playlist_t *vlcjs_get_playlist( js_State* J )
{
    return (vlc_playlist_t*) vlc_intf_GetMainPlaylist( vlcjs_get_this( J ) ); 
}

vlc_player_t *vlcjs_get_player( js_State *J )
{
    return (vlc_player_t *) vlc_playlist_GetPlayer( vlcjs_get_playlist( J ) );
}

vout_thread_t *vlcjs_get_vout( js_State *J){
    return vlc_player_vout_Hold( vlcjs_get_player( J ) );
}
audio_output_t *vlcjs_get_aout( js_State *J)
{
    return vlc_player_aout_Hold( vlcjs_get_player( J ) );
}

void vlcjs_register( js_State *J, const vlcjs_Reg *fn_list)
/**
 * registers functions and their corresponding names described by fn_list
 */
{
    for( ; fn_list -> fn_name != NULL && fn_list -> fn != NULL; fn_list ++ )
    {
        js_newcfunction( J, fn_list -> fn, fn_list -> fn_name, 0 );
        js_setproperty( J, -2, fn_list -> fn_name );
    }
}


/*****************************************************************************
 *
 *****************************************************************************/
static const char *ppsz_js_exts[] = { ".js", NULL };

static int file_select( const char *file )
{
    int i = strlen( file );
    int j;
    for( j = 0; ppsz_js_exts[j]; j++ )
    {
        int l = strlen( ppsz_js_exts[j] );
        if( i >= l && !strcmp( file+i-l, ppsz_js_exts[j] ) )
            return 1;
    }
    return 0;
}

static int file_compare( const char **a, const char **b )
{
    return strcmp( *a, *b );
}

static char **vlcjs_dir_list_append( char **restrict list, char *basedir,
                                      const char *jsdirname )
{
    if (unlikely(basedir == NULL))
        return list;

    if (likely(asprintf(list, "%s"DIR_SEP"js"DIR_SEP"%s",
                        basedir, jsdirname) != -1))
        list++;

    free(basedir);
    return list;
}

int vlcjs_dir_list(const char *jsdirname, char ***restrict listp)
{
    char **list = malloc(4 * sizeof(char *));
    if (unlikely(list == NULL))
        return VLC_EGENERIC;

    *listp = list;

    /* JS scripts in user-specific data directory */
    list = vlcjs_dir_list_append(list, config_GetUserDir(VLC_USERDATA_DIR),
                                  jsdirname);

    char *libdir = config_GetSysPath(VLC_PKG_LIBEXEC_DIR, NULL);
    char *datadir = config_GetSysPath(VLC_PKG_DATA_DIR, NULL);
    bool both = libdir != NULL && datadir != NULL && strcmp(libdir, datadir);

    /* Tokenized js scripts in architecture-specific data directory */
    list = vlcjs_dir_list_append(list, libdir, jsdirname);

    /* Source js Scripts in architecture-independent data directory */
    if (both || libdir == NULL)
        list = vlcjs_dir_list_append(list, datadir, jsdirname);
    else
        free(datadir);

    *list = NULL;
    return VLC_SUCCESS;
}

void vlcjs_dir_list_free( char **ppsz_dir_list )
{
    for( char **ppsz_dir = ppsz_dir_list; *ppsz_dir; ppsz_dir++ )
        free( *ppsz_dir );
    free( ppsz_dir_list );
}

input_item_t *vlcjs_read_input_item(vlc_object_t *obj, js_State *J)
{
    if (!js_isobject(J, -1))
    {
        msg_Warn(obj, "Playlist item should be an object" );
        return NULL;
    }

    js_getproperty(J, -1, "path");

    /* playlist key item path */
    if (!js_isstring(J, -1))
    {
        js_pop(J, 1); /* pop "path" */
        msg_Warn(obj, "Playlist item's path should be a string");
        return NULL;
    }

    /* Read path and name */
    const char *path = js_tostring(J, -1);
    msg_Dbg(obj, "Path: %s", path);
    js_pop(J, 1); // pop off the path

    const char *name = NULL;
    js_getproperty(J, -1, "name");
    if (js_isstring(J, -1))
    {
        name = js_tostring(J, -1);
        msg_Dbg(obj, "Name: %s", name);
    }
    else
        msg_Warn(obj, "Playlist item name should be a string" );
    js_pop(J, 1); // pop off the name

    /* Read duration */
    vlc_tick_t duration = INPUT_DURATION_INDEFINITE;

    js_getproperty( J, -1, "duration" );
    if (js_isnumber(J, -1))
        duration = vlc_tick_from_sec( js_tonumber(J, -1) );
    else
        msg_Warn(obj, "Playlist item duration should be a number (seconds)");
    js_pop( J, 1 ); /* pop "duration" */

    /* Read options: item must be on top of stack */
    char **optv = NULL;
    int optc = 0;

    // works on playlist item on top of stack
    vlcjs_read_options(obj, J, &optc, &optv);

    /* Create input item */
    input_item_t *item = input_item_NewExt(path, name, duration,
                                           ITEM_TYPE_UNKNOWN,
                                           ITEM_NET_UNKNOWN);
    if (likely(item != NULL))
    {
        input_item_AddOptions(item, optc, (const char **)optv,
                            VLC_INPUT_OPTION_TRUSTED);
        /* playlist key item */

        /* Read meta data: item must be on top of stack */
        vlcjs_read_meta_data(obj, J, item);

        /* copy the psz_name to the meta data, if "Title" is still empty */
        char* title = input_item_GetTitle(item);
        if (title == NULL)
            input_item_SetTitle(item, name);
        free(title);

        /* Read custom meta data: item must be on top of stack*/
        vlcjs_read_custom_meta_data(obj, J, item);
    }
    while (optc > 0)
           free(optv[--optc]);
    free(optv);
    return item;
}

void vlcjs_read_options( vlc_object_t *p_this, js_State *J, int *pi_options, char ***pppsz_options )
{
    js_getproperty( J, -1, "options" );
    if( js_isobject( J, -1 ) )
    {
        const char *key;
        js_pushiterator(J, -1, 0);
        while( key = js_nextiterator(J, -1) != NULL )
        {   
            js_getproperty(J, -2, key);
            if( js_isstring( J, -1 ) )
            {
                char *psz_option = js_tostring( J, -1 );
                msg_Dbg( p_this, "Option: %s", psz_option );
                TAB_APPEND( *pi_options, *pppsz_options, psz_option );
            }
            else
            {
                msg_Warn( p_this, "Option should be a string" );
            }
            js_pop( J, 1 ); /* pop property */
        }
        js_pop(J, 1); // pop iterator
    }
    js_pop( J, 1 ); /* pop "options" */
}

void vlcjs_read_meta_data( vlc_object_t *p_this, js_State *J,
                            input_item_t *p_input )
{
#define TRY_META( a, b )                                        \
    js_getproperty( J, -1, a );                                 \
    if( js_isstring( J, -1 ) &&                                 \
        strcmp( js_tostring( J, -1 ), "" ) )                    \
    {                                                           \
        char *psz_value = js_tostring( J, -1 );                 \
        EnsureUTF8( psz_value );                                \
        input_item_Set ## b ( p_input, psz_value );             \
    }                                                           \
    js_pop( J, 1 ); /* pop a */

    TRY_META( "title", Title );
    TRY_META( "artist", Artist );
    TRY_META( "genre", Genre );
    TRY_META( "copyright", Copyright );
    TRY_META( "album", Album );
    TRY_META( "tracknum", TrackNum );
    TRY_META( "description", Description );
    TRY_META( "rating", Rating );
    TRY_META( "date", Date );
    TRY_META( "setting", Setting );
    TRY_META( "url", URL );
    TRY_META( "language",  Language );
    TRY_META( "nowplaying", NowPlaying );
    TRY_META( "publisher",  Publisher );
    TRY_META( "encodedby",  EncodedBy );
    TRY_META( "arturl",     ArtURL );
    TRY_META( "trackid",    TrackID );
    TRY_META( "director",   Director );
    TRY_META( "season",     Season );
    TRY_META( "episode",    Episode );
    TRY_META( "show_name",  ShowName );
    TRY_META( "actors",     Actors );
}

void vlcjs_read_custom_meta_data( vlc_object_t *p_this, js_State *J,
                                     input_item_t *p_input )
{
    /* Lock the input item and create the meta table if needed */
    vlc_mutex_lock( &p_input->lock );

    if( !p_input->p_meta )
        p_input->p_meta = vlc_meta_New();

    /* ... item */
    js_getproperty( J, -1, "meta" );
    /* ... item meta */
    if( js_isobject( J, -1 ) )
    {
        js_pushiterator(J, -1, 0);
        const char *key;
        while( key = js_nextiterator(J, -1) != NULL )
        {
            js_getproperty(J, -2, key);
            if( !js_isstring( J, -1 ))
            {
                msg_Err( p_this, "'meta' keys and values must be strings");
                js_pop( J, 1 ); /* pop "value" */
                continue;
            }
            const char *psz_key = key;
            const char *psz_value = js_tostring(J, -1);

            vlc_meta_AddExtra( p_input->p_meta, psz_key, psz_value );

            js_pop( J, 1 ); /* pop "value" */
        }
        js_pop(J, 1); // pop iterator
    }
    js_pop( J, 1 ); /* pop "meta" */
    /* ... item -> back to original stack */

    vlc_mutex_unlock( &p_input->lock );
}

/*****************************************************************************
 * Will execute func on all scripts in jsdirname, and stop if func returns
 * success.
 *****************************************************************************/
int vlcjs_scripts_batch_execute( vlc_object_t *p_this,
                                  const char * jsdirname,
                                  int (*func)(vlc_object_t *, const char *))
{
    char **ppsz_dir_list = NULL;
    int i_ret;

    if( (i_ret = vlcjs_dir_list( jsdirname, &ppsz_dir_list )) != VLC_SUCCESS)
        return i_ret;

    i_ret = VLC_EGENERIC;
    for( char **ppsz_dir = ppsz_dir_list; *ppsz_dir; ppsz_dir++ )
    {
        char **ppsz_filelist;

        msg_Info( p_this, "Trying JS scripts in %s", *ppsz_dir );
        int i_files = vlc_scandir( *ppsz_dir, &ppsz_filelist, file_select,
                                   file_compare );
        if( i_files < 0 )
            continue;

        char **ppsz_file = ppsz_filelist;
        char **ppsz_fileend = ppsz_filelist + i_files;

        while( ppsz_file < ppsz_fileend )
        {
            char *psz_filename;

            if( asprintf( &psz_filename,
                          "%s" DIR_SEP "%s", *ppsz_dir, *ppsz_file ) == -1 )
                psz_filename = NULL;
            free( *(ppsz_file++) );

            if( likely(psz_filename != NULL) )
            {
                msg_Info( p_this, "Trying JS playlist script %s", psz_filename );
                i_ret = func( p_this, psz_filename);
                msg_Info( p_this, "Probed JS playlist script %s", psz_filename );
                if( i_ret == VLC_SUCCESS )
                    break;
            }
        }

        while( ppsz_file < ppsz_fileend )
            free( *(ppsz_file++) );
        free( ppsz_filelist );

        if( i_ret == VLC_SUCCESS )
            break;
    }
    vlcjs_dir_list_free( ppsz_dir_list );
    return i_ret;
}

char *vlcjs_find_file( const char *psz_jsdirname, const char *psz_name )
{
    char **ppsz_dir_list = NULL;
    vlcjs_dir_list( psz_jsdirname, &ppsz_dir_list );

    for( char **ppsz_dir = ppsz_dir_list; *ppsz_dir; ppsz_dir++ )
    {
        const char *ppsz_ext = ".js";
        char *psz_filename;
        struct stat st;

        if( asprintf( &psz_filename, "%s"DIR_SEP"%s%s", *ppsz_dir,
                        psz_name, ppsz_ext ) < 0 )
        {
            vlcjs_dir_list_free( ppsz_dir_list );
            return NULL;
        }

        if( vlc_stat( psz_filename, &st ) == 0
            && S_ISREG( st.st_mode ) )
        {
            vlcjs_dir_list_free( ppsz_dir_list );
            return psz_filename;
        }
        free( psz_filename );
    }
    vlcjs_dir_list_free( ppsz_dir_list );
    return NULL;
}

static int vlcjs_add_modules_path_inner( js_State *J, const char *psz_path )
{
    int count = 0;
    const char *ppsz_ext = ".js";
    char *str;
    if( asprintf(&str, "%s"DIR_SEP"modules"DIR_SEP"?%s;", psz_path, *ppsz_ext) < 0)
        return 0;
    js_pushstring(J, str);
    free(str);
    count ++;

    return count;
}

int vlcjs_add_modules_path( js_State *J, const char *psz_filename )
{
    /* Setup the module search path:
     *   * "The script's directory"/modules
     *   * "The script's parent directory"/modules
     *   * and so on for all the next directories in the directory list
     */
    char *psz_path = strdup( psz_filename );
    if( !psz_path )
        return 1;

    char *psz_char = strrchr( psz_path, DIR_SEP_CHAR );
    if( !psz_char )
    {
        free( psz_path );
        return 1;
    }
    *psz_char = '\0';

    /* psz_path now holds the file's directory */
    psz_char = strrchr( psz_path, DIR_SEP_CHAR );
    if( !psz_char )
    {
        free( psz_path );
        return 1;
    }
    *psz_char = '\0';

    /* Push package on stack */
    int count = 0;
    js_getglobal( J, "package" ); // TODO: check if package exists or not

    /* psz_path now holds the file's parent directory */
    count += vlcjs_add_modules_path_inner( J, psz_path );
    *psz_char = DIR_SEP_CHAR;

    /* psz_path now holds the file's directory */
    count += vlcjs_add_modules_path_inner( J, psz_path );

    char **ppsz_dir_list = NULL;
    vlcjs_dir_list( psz_char+1, &ppsz_dir_list );
    char **ppsz_dir = ppsz_dir_list;

    for( ; *ppsz_dir && strcmp( *ppsz_dir, psz_path ); ppsz_dir++ );
    free( psz_path );

    for( ; *ppsz_dir; ppsz_dir++ )
    {
        psz_path = *ppsz_dir;
        /* FIXME: doesn't work well with meta/... modules due to the double
         * directory depth */
        psz_char = strrchr( psz_path, DIR_SEP_CHAR );
        if( !psz_char )
        {
            vlcjs_dir_list_free( ppsz_dir_list );
            return 1;
        }

        *psz_char = '\0';
        count += vlcjs_add_modules_path_inner( J, psz_path );
        *psz_char = DIR_SEP_CHAR;
        count += vlcjs_add_modules_path_inner( J, psz_path );
    }
    vlcjs_dir_list_free( ppsz_dir_list );
    return 1;
}

int vlcjs_dofile( vlc_object_t *p_this, js_State *J, const char *curi )
{
    char *uri = ToLocaleDup( curi );
    if( !strstr( uri, "://" ) ) {
        int ret = js_dofile( J, uri );
        free( uri );
        return ret;
    }
    if( !strncasecmp( uri, "file://", 7 ) ) {
        int ret = js_dofile( J, uri + 7 );
        free( uri );
        return ret;
    }
    return 0;
}

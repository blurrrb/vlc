/*****************************************************************************
 * Copyright (C) 2009-2019 VideoLAN and authors
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "general.h"

typedef struct vlcjs_queue_element
{
    char *fn;
    char *this;
    char *data;
    struct vlcjs_queue_element *next;
} vlcjs_queue_element;

typedef struct
{
    vlcjs_queue_element *front, *back;
    vlc_mutex_t lock;
    vlc_cond_t cond;
    bool stop_loop;
    vlc_thread_t thread;
    vlc_mutex_t *execution_lock;
} vlcjs_queue;


void vlcjs_registerCallback( js_State *, char*, char *, char * );

void vlcjs_setupLoop( js_State *, vlc_mutex_t * );

void vlcjs_enterLoop( js_State * );

void vlcjs_terminateLoop( js_State * );
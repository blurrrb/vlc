/*****************************************************************************
 * xml.c: XML related functions
 *****************************************************************************
 * Copyright (C) 2010-2019 Antoine Cellerier
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"
#include <vlc_xml.h>


static void vlcjs_xml_reader_next_node( js_State *J );
static void vlcjs_xml_reader_next_attr( js_State *J );
static void vlcjs_xml_reader_node_empty( js_State *J );
/*****************************************************************************
 * XML Reader
 *****************************************************************************/


static void vlcjs_xml_reader_delete__gc( js_State *J, void *data )
{
    stream_t **pp_reader = data;
    xml_ReaderDelete( *pp_reader );
    free( pp_reader );
}

static const vlcjs_Reg vlcjs_xml_reader_reg[] = {
    { "next_node", vlcjs_xml_reader_next_node },
    { "next_attr", vlcjs_xml_reader_next_attr },
    { "node_empty", vlcjs_xml_reader_node_empty },
    { NULL, NULL }
};

static void vlcjs_xml_create_reader( js_State *J )
{
    vlc_object_t *obj = vlcjs_get_this( J );
    stream_t *p_stream = *(stream_t **)js_touserdata( J, 1, "stream" );

    xml_reader_t *p_reader = xml_ReaderCreate( obj, p_stream );
    if( !p_reader )
    {
        js_error( J, "XML reader creation failed." );
        return;
    }
    js_currentfunction( J );
    js_getproperty( J, -1, "prototype" );
    xml_reader_t **pp_reader = malloc( sizeof( xml_reader_t * ) );

    *pp_reader = p_reader;
    js_newuserdata( J, "xml_reader", pp_reader, vlcjs_xml_reader_delete__gc);
    vlcjs_register( J, vlcjs_xml_reader_reg );
}

static void vlcjs_xml_reader_next_node( js_State *J )
{
    xml_reader_t *p_reader = *(xml_reader_t**)js_touserdata( J, 0, "xml_reader" );
    const char *psz_name;
    int i_type = xml_ReaderNextNode( p_reader, &psz_name );
    js_newobject( J );
    if( i_type <= 0 )
        js_pushnumber( J, 0 );
    else
        js_pushnumber( J, i_type );
    js_setproperty( J, -2, "type" );
    
    if( i_type <= 0 )
        js_pushnull( J );
    else
        js_pushstring( J, psz_name );
    js_setproperty( J, -2, "name" );

    // object is at top of the stack and is returned automatically
}

static void vlcjs_xml_reader_next_attr( js_State *J )
{
    xml_reader_t *p_reader = *(xml_reader_t**)js_touserdata( J, 0, "xml_reader" );
    const char *psz_value;
    const char *psz_name = xml_ReaderNextAttr( p_reader, &psz_value );
    if( !psz_name )
    {
        js_pushnull( J );
        return;
    }

    js_newobject( J );
    js_pushstring( J, psz_name );
    js_setproperty( J, -2, "name" );
    js_pushstring( J, psz_value );
    js_setproperty( J, -2, "value" );

    // the object is at the top of the stack and is returned
}

static void vlcjs_xml_reader_node_empty( js_State *J )
{
    xml_reader_t *p_reader = *(xml_reader_t**)js_touserdata( J, 0, "xml_reader" );

    js_pushnumber( J, xml_ReaderIsEmptyElement( p_reader ) );
}

static const vlcjs_Reg vlcjs_xml_reg[] = {
    { "create_reader", vlcjs_xml_create_reader },
    { NULL, NULL }
};

static void vlcjs_xml_create( js_State *J )
{
    js_newobject( J );
    vlcjs_register( J, vlcjs_xml_reg );

}


void vlcjs_register_xml( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newcfunction( J, vlcjs_xml_create, "xml", 0);
    js_setproperty( J, -2, "xml" );
    js_pop( J, 1 ); // pop the vlc object
}
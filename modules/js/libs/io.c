/*****************************************************************************
 * misc.c
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Hugo Beauzée-Luyssen <hugo@beauzee.fr>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "general.h"

static void vlcjs_io_file_read_chars( js_State *J, size_t i_len, FILE* p_file )
{
    size_t f_pos = ftell( p_file );
    fseek( p_file, 0, SEEK_END );
    size_t end_pos = ftell( p_file );
    size_t max_buffer_size = end_pos - f_pos;
    fseek( p_file, f_pos, SEEK_SET );

    size_t bytes_to_be_read;
    if( i_len < max_buffer_size ) bytes_to_be_read = i_len;
    else bytes_to_be_read = max_buffer_size;

    char* buffer = ( char * ) malloc( ( bytes_to_be_read + 1 ) * sizeof(char) ); // +1 for the null character
    fread( buffer, sizeof(char), bytes_to_be_read, p_file );

    buffer[bytes_to_be_read] = 0;

    js_pushstring( J, buffer );
    free( buffer );
}

static void vlcjs_io_file_read_number( js_State *J )
{
    FILE **pp_file = js_touserdata( J, 0, "file**" );
    if ( !*pp_file )
    {
        js_error( J, "attempt to use a closed file" );
        return;
    }
    double num;
    if ( fscanf( *pp_file, "%lf", &num ) != 1 ){
        js_pushnull( J );
        return;
    }
    js_pushnumber( J, num );
}
static int vlcjs_io_file_read_line( js_State *J )
{
    FILE **pp_file = js_touserdata( J, 0, "file**" );
    if ( !*pp_file )
    {
        js_error( J, "attempt to use a closed file" );
        return;
    }
    char* psz_line = NULL;
    size_t i_buffer;
    ssize_t i_len = getline( &psz_line, &i_buffer, *pp_file );
    if ( i_len == -1 )
    {
        // empty string
        js_pushnull( J );
    }
    if( psz_line[i_len - 1] == '\n' )
        psz_line[--i_len] = 0;
    js_pushstring( J, psz_line );
    free( psz_line );
}

static void vlcjs_io_file_read( js_State *J )
/**
 * reads number of characters if argument is provided else reads the whole file
 */
{
    FILE **pp_file = js_touserdata( J, 0, "file**" );
    if ( !*pp_file )
    {
        js_error( J, "attempt to use a closed file" );
        return;
    }
    if( js_isdefined( J, 1) && js_isnumber( J, 1 ) )
    {
        vlcjs_io_file_read_chars( J, (size_t)js_tointeger( J, 1 ), *pp_file );
        return;
    }
    // const char* psz_mode;
    
    // if( js_isdefined( J, 1 ) && js_isstring( J, 1 ) )
    //     psz_mode = js_tostring( J, 1 );
    // else psz_mode = "*l";

    // if ( *psz_mode != '*' )
    // {
    //     js_error( J, "Invalid file.read() format: %s", psz_mode );
    //     return;
    // }
    // switch ( psz_mode[1] )
    // {
    //     case 'l':
    //         vlcjs_io_file_read_line( J, *pp_file ); return;
    //     case 'n':
    //         vlcjs_io_file_read_number( J, *pp_file ); return;
    //     case 'a':
    //         vlcjs_io_file_read_chars( J, SIZE_MAX, *pp_file ); return;
    //     default:
    //         break;
    // }
    vlcjs_io_file_read_chars( J, SIZE_MAX, *pp_file );
}

static void vlcjs_io_file_write( js_State *J )
{
    FILE **pp_file = js_touserdata( J, 0, "file**" );
    if ( !*pp_file )
    {
        js_error( J, "attempt to read a closed file" );
        return;
    }
    int i_nb_args = js_gettop( J );
    bool b_success = true;
    for ( int i = 1; i < i_nb_args; ++i )
    {
        bool i_res;
        if ( js_isnumber( J, i ) )
            // javascript numbers are always 64bit float
            i_res = fprintf(*pp_file, "%lf", js_tonumber( J, i ) ) > 0;
        else
        {
            const char* psz_value = js_tostring( J, i );
            size_t i_len = strlen( psz_value );
            i_res = fwrite(psz_value, sizeof(*psz_value), i_len, *pp_file ) > 0;
        }
        b_success = b_success && i_res;
    }
    if( !b_success ) js_error( J, "error writing to file" );
    else js_pushundefined( J );
}

static void vlcjs_io_file_seek( js_State *J )
{
    FILE **pp_file = js_touserdata( J, 0, "file**" );
    if ( !*pp_file )
    {
        js_error( J, "attempt to use a closed file" );
        return;
    }

    const char* psz_mode;
    // whence at position 1 in stack, default NULL
    if( js_isdefined( J, 1 ) && js_isstring( J, 1 ) )
        psz_mode = js_tostring( J, 1 );
    else psz_mode = NULL;
    if ( psz_mode != NULL )
    {
        long i_offset;
        // offset at position 2 in stack, default 0
        if( js_isdefined( J, 2 ) && js_isnumber( J, 2 ) )
            i_offset = js_toint32( J, 2 );
        else psz_mode = NULL;
        int i_mode;
        if ( !strcmp( psz_mode, "set" ) )
            i_mode = SEEK_SET;
        else if ( !strcmp( psz_mode, "end" ) )
            i_mode = SEEK_END;
        else
            i_mode = SEEK_CUR;
        if( fseek( *pp_file, i_offset, i_mode ) != 0 )
        {
            js_error( J, "failed to seek" );
            return;
        }
    }
    js_pushnumber( J, ftell( *pp_file ) );
}

static void vlcjs_io_file_tell( js_State *J )
{
    FILE **pp_file = js_touserdata( J, 0, "file**" );
    if ( !*pp_file )
    {
        js_error( J, "attempt to flush a closed file");
        return;
    }
    js_pushnumber( J, ftell( *pp_file ) );
}

static void vlcjs_io_file_flush( js_State *J )
{
    FILE **pp_file = js_touserdata( J, 0, "file**" );
    if ( !*pp_file )
    {
        js_error( J, "attempt to flush a closed file");
        return;
    }
    fflush( *pp_file );
    js_pushundefined( J );
}

static void vlcjs_io_file_close( js_State *J )
{
    FILE **pp_file = js_touserdata(J, 0, "file**"); // 0 refers to this object in stack
    if ( *pp_file )
    {
        fclose( *pp_file );
        *pp_file = NULL;
    }
    else js_error( J, "attempt to close a closed file");
    js_pushundefined( J );
}

void vlcjs_io_file_close__gc( js_State *J, void *data )
{
    FILE **pp_file = (FILE**) data;
    if ( *pp_file )
    {
        fclose( *pp_file );
        *pp_file = NULL;
    }
    js_pushundefined( J );
}

static const vlcjs_Reg vlc_io_file_reg[] = {
    { "read", vlcjs_io_file_read },
    { "read_number", vlcjs_io_file_read_number },
    { "read_line", vlcjs_io_file_read_line },
    { "write", vlcjs_io_file_write },
    { "seek", vlcjs_io_file_seek },
    { "tell", vlcjs_io_file_tell },
    { "flush", vlcjs_io_file_flush },
    { "close", vlcjs_io_file_close },
    { NULL, NULL }
};

static void vlcjs_io_open( js_State *J )
{
    // check if file path provided or not
    if( js_gettop( J ) < 1 )
    {
        js_error( J, "Usage: vlc.io.open(file_path [, mode])" );
        return;
    }
    const char* psz_path = js_tostring( J, 1 );
    const char* psz_mode;
    
    // default psz_mode = "r"
    if( js_isdefined( J, 2) && js_isstring( J, 2 ) )
    {
        psz_mode = js_tostring( J, 2 );
    }
    else psz_mode = "r";
    msg_Dbg(vlcjs_get_this(J), "io %s %s", psz_path, psz_mode); //---------
    FILE *p_f =  vlc_fopen( psz_path, psz_mode );
    if ( p_f == NULL )
    {
        js_error( J, "error opening file" );
        return;
    }

    FILE** pp_file = (FILE **)malloc( sizeof( pp_file ) );
    if( !pp_file ) return js_error( J, "memory error");
    *pp_file = p_f;
    // registering the file discriptor as private userdata
    js_currentfunction( J );
	js_getproperty(J, -1, "prototype");
	js_newuserdata(J, "file**", pp_file, vlcjs_io_file_close__gc);

    // registring the associated functions
    vlcjs_register( J, vlc_io_file_reg );
}

static void vlcjs_io_listdir( js_State *J )
{
    // check if path provided
    if( js_gettop( J ) < 1 )
    {
        js_error( J, "Usage: vlc.io.listdir(path)" );
        return;
    }
    const char* psz_path = js_tostring( J, 1 );
    DIR* p_dir =  vlc_opendir( psz_path );
    if ( p_dir == NULL )
    {
        js_error( J, "error opening directory for reading");
        return;
    }

    // creating a js array
    js_newarray( J );
    const char* psz_entry;
    int idx = 0;
    while ( ( psz_entry = vlc_readdir( p_dir ) ) != NULL )
    {
        js_pushstring( J, psz_entry );
        js_setindex( J, -2, idx++ ); // -2 refers to position of array in stack
    }
    closedir( p_dir );
    // the array is at the top of the stack, and is passed as return value
}

static void vlcjs_io_unlink( js_State *J )
{
    // check if no file provided for deletion
    if( js_gettop( J ) < 1 )
    {
        js_error( J, "Usage: vlc.io.unlink(path)" );
        return;
    }
    const char* psz_path = js_tostring( J, 1 );
    // msg_Info((intf_thread_t*)vlcjs_get_this( J ), psz_path);
    int i_res = vlc_unlink( psz_path );
    if( i_res != 0 )
    {
        // msg_Info((intf_thread_t*)vlcjs_get_this( J ), psz_path);
        js_error( J, "file deletion failed");
        return;
    }
    js_pushundefined( J );
}

static void vlcjs_io_mkdir( js_State *J )
{   
    // if less than 2 args provided, then error
    if( js_gettop( J ) < 2 )
    {
        js_error( J, "Usage: vlc.io.mkdir(path, mode)");
        return;
    }

    const char* psz_dir = js_tostring( J, 1 );
    const char* psz_mode = js_tostring( J, 2 );
    if ( !psz_dir || !psz_mode )
    {
        js_error( J, "path and mode are not both strings");
        return;
    }
    int i_res = vlc_mkdir( psz_dir, strtoul( psz_mode, NULL, 0 ) );
    if( i_res != 0)
    {
        js_error( J, "directory creation failed");
    }
    js_pushundefined( J );    
}

static void vlcjs_io_rmdir( js_State *J )
{   
    // if less than 2 args provided, then error
    if( js_gettop( J ) < 1 )
    {
        js_error( J, "Usage: vlc.io.rmdir(path)" );
        return;
    }

    const char* psz_dir = js_tostring( J, 1 );
    if ( !psz_dir)
    {
        js_error( J, "cannot convert provided arg to path");
        return;
    }
    int i_res = rmdir( psz_dir );
    if( i_res != 0)
    {
        js_error( J, "directory deletion failed, folder may not be empty");
    }
    js_pushundefined( J );    
}

static const vlcjs_Reg vlcjs_io_reg[] = {
    { "mkdir", vlcjs_io_mkdir },
    { "open", vlcjs_io_open },
    { "listdir", vlcjs_io_listdir },
    { "unlink", vlcjs_io_unlink },
    { "rmdir", vlcjs_io_rmdir},
    { NULL, NULL }
};

void vlcjs_register_io( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_io_reg );
    js_setproperty( J, -2, "io" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

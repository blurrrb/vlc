/*****************************************************************************
 * net.c: Network related functions
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>
#include <errno.h>
#ifdef _WIN32
#include <io.h>
#endif
#ifdef HAVE_POLL
#include <poll.h>       /* poll structures and defines */
#endif
#include <sys/stat.h>

#include <vlc_common.h>
#include <vlc_network.h>
#include <vlc_url.h>
#include <vlc_fs.h>
#include <vlc_interrupt.h>

#include "general.h"

static vlcjs_dtable_t *vlcjs_get_dtable( js_State *J )
{
    return vlcjs_get( J, "d_table" );
}

vlc_interrupt_t *vlcjs_set_interrupt( js_State *J )
{
    vlcjs_dtable_t *dt = vlcjs_get_dtable( J );
    return vlc_interrupt_set( dt->interrupt );
}

/** Maps an OS file descriptor to a VLC JS file descriptor */
static int vlcjs_fd_map( js_State *J, int fd )
{
    vlcjs_dtable_t *dt = vlcjs_get_dtable( J );

    if( (unsigned)fd < 3u )
        return -1;

#ifndef NDEBUG
    for( unsigned i = 0; i < dt->fdc; i++ )
        assert( dt->fdv[i] != fd );
#endif

    for( unsigned i = 0; i < dt->fdc; i++ )
    {
        if( dt->fdv[i] == -1 )
        {
            dt->fdv[i] = fd;
            return 3 + i;
        }
    }

    if( dt->fdc >= 64 )
        return -1;

    int *fdv = realloc( dt->fdv, (dt->fdc + 1) * sizeof (dt->fdv[0]) );
    if( unlikely(fdv == NULL) )
        return -1;

    dt->fdv = fdv;
    dt->fdv[dt->fdc] = fd;
    fd = 3 + dt->fdc;
    dt->fdc++;
    return fd;
}

static int vlcjs_fd_map_safe( js_State *J, int fd )
{
    int jsfd = vlcjs_fd_map( J, fd );
    if( jsfd == -1 )
        net_Close( fd );
    return jsfd;
}

/** Gets the OS file descriptor mapped to a VLC JS file descriptor */
static int vlcjs_fd_get( js_State *J, unsigned idx )
{
    vlcjs_dtable_t *dt = vlcjs_get_dtable( J );

    if( idx < 3u )
        return idx;
    idx -= 3;
    return (idx < dt->fdc) ? dt->fdv[idx] : -1;
}

/** Gets the VLC JS file descriptor mapped from an OS file descriptor */
static int vlcjs_fd_get_js( js_State *J, int fd )
{
    vlcjs_dtable_t *dt = vlcjs_get_dtable( J );

    if( (unsigned)fd < 3u )
        return fd;
    for( unsigned i = 0; i < dt->fdc; i++ )
        if( dt->fdv[i] == fd )
            return 3 + i;
    return -1;
}

/** Unmaps an OS file descriptor from VLC JS */
static void vlcjs_fd_unmap( js_State *J, unsigned idx )
{
    vlcjs_dtable_t *dt = vlcjs_get_dtable( J );
    int fd;

    if( idx < 3u )
        return; /* Never close stdin/stdout/stderr. */

    idx -= 3;
    if( idx >= dt->fdc )
        return;

    fd = dt->fdv[idx];
    dt->fdv[idx] = -1;
    while( dt->fdc > 0 && dt->fdv[dt->fdc - 1] == -1 )
        dt->fdc--;
    /* realloc() not really needed */
#ifndef NDEBUG
    for( unsigned i = 0; i < dt->fdc; i++ )
        assert( dt->fdv[i] != fd );
#else
    (void) fd;
#endif
}

static void vlcjs_fd_unmap_safe( js_State *J, unsigned idx )
{
    int fd = vlcjs_fd_get( J, idx );

    vlcjs_fd_unmap( J, idx );
    if( fd != -1 )
        net_Close( fd );
}

/*****************************************************************************
 * Net listen
 *****************************************************************************/
static void vlcjs_net_listen_close__gc( js_State *, void * );
static void vlcjs_net_listen_close( js_State * );
static void vlcjs_net_accept( js_State * );
static void vlcjs_net_fds( js_State * );

static const vlcjs_Reg vlcjs_net_listen_reg[] = {
    { "accept", vlcjs_net_accept },
    { "fds", vlcjs_net_fds },
    { NULL, NULL }
};

static void vlcjs_net_listen_tcp( js_State *J )
{
    vlc_object_t *p_this = vlcjs_get_this( J );
    const char *psz_host = js_tostring( J, 1 );
    int i_port = js_tointeger( J, 2 );
    int *pi_fd = net_ListenTCP( p_this, psz_host, i_port );
    if( pi_fd == NULL )
        return js_error( J, "Cannot listen on %s:%d", psz_host, i_port );

    for( unsigned i = 0; pi_fd[i] != -1; i++ )
        if( vlcjs_fd_map( J, pi_fd[i] ) == -1 )
        {
            while( i > 0 )
                vlcjs_fd_unmap( J, vlcjs_fd_get_js( J, pi_fd[--i] ) );

            net_ListenClose( pi_fd );
            return js_error( J, "Cannot listen on %s:%d", psz_host, i_port );
        }

    int **ppi_fd = malloc( sizeof( int * ) );
    if( !ppi_fd ) return js_error( J, "memory error" );
    *ppi_fd = pi_fd;

    js_pushundefined( J );
    js_newuserdata( J, "net_listen", ppi_fd, vlcjs_net_listen_close__gc );

    vlcjs_register( J, vlcjs_net_listen_reg );
}

static void vlcjs_net_listen_close__gc( js_State *J, void *data )
{
    int **ppi_fd = (int**) data;
    int *pi_fd = *ppi_fd;

    if( *ppi_fd == NULL )
        return js_error( J, "attempt to close a closed fd" );

    for( unsigned i = 0; pi_fd[i] != -1; i++ )
        vlcjs_fd_unmap( J, vlcjs_fd_get_js( J, pi_fd[i] ) );

    net_ListenClose( pi_fd );
    *ppi_fd = NULL;
    js_pushundefined( J );
}

static void vlcjs_net_fds( js_State *J )
{
    int **ppi_fd = (int**) js_touserdata( J, 0, "net_listen" );
    int *pi_fd = *ppi_fd;

    int i_count = 0;
    js_newarray( J );
    while( pi_fd[i_count] != -1 )
    {
        js_pushnumber( J, vlcjs_fd_get_js( J, pi_fd[i_count] ) );
        js_setindex( J, -2, i_count++ );
    }
}

static void vlcjs_net_accept( js_State *J )
{
    vlc_object_t *p_this = vlcjs_get_this( J );
    int **ppi_fd = (int**) js_touserdata( J, 0, "net_listen" );
    int i_fd = net_Accept( p_this, *ppi_fd );

    js_pushnumber( J, vlcjs_fd_map_safe( J, i_fd ) );
}

/*****************************************************************************
 *
 *****************************************************************************/
static void vlcjs_net_connect_tcp( js_State *J )
{
    vlc_object_t *p_this = vlcjs_get_this( J );
    const char *psz_host = js_tostring( J, 1 );
    int i_port = js_tointeger( J, 2 );
    int i_fd = net_ConnectTCP( p_this, psz_host, i_port );
    js_pushnumber( J, vlcjs_fd_map_safe( J, i_fd ) );
}

static void vlcjs_net_close( js_State *J )
{
    int i_fd = js_tointeger( J, 1 );
    vlcjs_fd_unmap_safe( J, i_fd );
    js_pushundefined( J );
}

static void vlcjs_net_send( js_State *J )
{
    int fd = vlcjs_fd_get( J, js_tointeger( J, 1 ) );
    const char *psz_buffer = js_tostring( J, 2 );
    size_t i_len;

    if( js_isdefined( J, 3) && js_isnumber( J, 3 ) )
        i_len = (size_t)js_tointeger( J, 3);
    else
        i_len = strlen( psz_buffer );
    
    js_pushnumber( J,
        (fd != -1) ? send( fd, psz_buffer, i_len, MSG_NOSIGNAL ) : -1 );
}

static void vlcjs_net_recv( js_State *J )
{
    int fd = vlcjs_fd_get( J, js_tointeger( J, 1 ) );
    size_t i_len;

    if( js_isdefined( J, 2 ) && js_isnumber( J, 2 ) )
        i_len = js_tointeger( J, 2 );
    else
        i_len = 1;
    
    char psz_buffer[i_len];

    ssize_t i_ret = (fd != -1) ? recv( fd, psz_buffer, i_len, 0 ) : -1;
    if( i_ret > 0 )
        js_pushlstring( J, psz_buffer, i_ret );
    else
        js_pushnull( J );
}

/*****************************************************************************
 *
 *****************************************************************************/
/* Takes a { fd : events } table as first arg and modifies it to { fd : revents } */
static void vlcjs_net_poll( js_State *J )
{
    if( !js_isarray( J, 1 ) )
        return js_error( J, "usage: vlc.net.poll( {fd1 : event1, fd2 : event2, ... } )");

    int i_fds = 0;
    js_pushiterator( J, 1, 0 );
    while( js_nextiterator( J, -1 ) )
    {
        i_fds++;
        js_pop( J, 1 );
    }
    js_pop( J, 1 );

    struct pollfd *p_fds = malloc( i_fds * sizeof( *p_fds ) );
    if( !p_fds ) return js_error( J, "memory error" );
    int *jsfds = malloc( i_fds * sizeof( int * ) );
    if( !jsfds ) return js_error( J, "memory error" );

    js_pushiterator( J, 1, 0 );
    for( int i = 0; js_nextiterator( J, -1 ); i++ )
    {
        js_getproperty( J, 1, js_tostring( J, -1 ) );

        jsfds[i] = js_tointeger( J, -2 );
        p_fds[i].fd = vlcjs_fd_get( J, jsfds[i] );
        p_fds[i].events = js_tointeger( J, -1 );
        p_fds[i].events &= POLLIN | POLLOUT | POLLPRI;
        js_pop( J, 2 );
    }
    js_pop( J, 1 );

    vlc_interrupt_t *oint = vlcjs_set_interrupt( J );
    int ret = 1, val = -1;

    do
    {
        if( vlc_killed() )
            break;
        val = vlc_poll_i11e( p_fds, i_fds, -1 );
    }
    while( val == -1 && errno == EINTR );

    vlc_interrupt_set( oint );

    for( int i = 0; i < i_fds; i++ )
    {
        js_pushnumber( J, (val >= 0) ? p_fds[i].revents : 0 );
        char * fd_str;
        sprintf(fd_str, "%d", jsfds[i]);
        js_setproperty( J, 1, fd_str );
    }
    js_pushnumber( J, val );

    free( jsfds );
    free( p_fds );

    if( val == -1 )
        return js_error( J, "Interrupted." );
}

/*****************************************************************************
 *
 *****************************************************************************/
/*
static void vlcjs_fd_open( js_State *J )
{
}
*/

#ifndef _WIN32
static void vlcjs_fd_write( js_State *J )
{
    int fd = vlcjs_fd_get( J, js_tointeger( J, 1 ) );
    size_t i_len;
    const char *psz_buffer = js_tostring( J, 2 );
    i_len = strlen( psz_buffer );

    i_len = (size_t) js_isnumber( J, 3 ) ? js_tointeger( J, 3 ) : i_len;
    js_pushnumber( J, (fd != -1) ? vlc_write( fd, psz_buffer, i_len ) : -1 );
}

static void vlcjs_fd_read( js_State *J )
{
    int fd = vlcjs_fd_get( J, js_tointeger( J, 1 ) );
    size_t i_len = (size_t) js_isnumber( J, 2 ) ? js_tointeger( J, 2 ) : 1;
    char psz_buffer[i_len];

    ssize_t i_ret = (fd != -1) ? read( fd, psz_buffer, i_len ) : -1;
    if( i_ret > 0 )
        js_pushlstring( J, psz_buffer, i_ret );
    else
        js_pushnull( J );
}
#endif

/*****************************************************************************
 *
 *****************************************************************************/
static void vlcjs_stat( js_State *J )
{
    const char *psz_path = js_tostring( J, 1 );
    struct stat s;
    if( vlc_stat( psz_path, &s ) )
        return js_error( J, "Couldn't stat %s.", psz_path );
    
    js_newobject( J );

    if( S_ISREG( s.st_mode ) )
        js_pushliteral( J, "file" );
    else if( S_ISDIR( s.st_mode ) )
        js_pushliteral( J, "dir" );
#ifdef S_ISCHR
    else if( S_ISCHR( s.st_mode ) )
        js_pushliteral( J, "character device" );
#endif
#ifdef S_ISBLK
    else if( S_ISBLK( s.st_mode ) )
        js_pushliteral( J, "block device" );
#endif
#ifdef S_ISFIFO
    else if( S_ISFIFO( s.st_mode ) )
        js_pushliteral( J, "fifo" );
#endif
#ifdef S_ISLNK
    else if( S_ISLNK( s.st_mode ) )
        js_pushliteral( J, "symbolic link" );
#endif
#ifdef S_ISSOCK
    else if( S_ISSOCK( s.st_mode ) )
        js_pushliteral( J, "socket" );
#endif
    else
        js_pushliteral( J, "unknown" );
    js_setproperty( J, -2, "type" );
    js_pushnumber( J, s.st_mode );
    js_setproperty( J, -2, "mode" );
    js_pushnumber( J, s.st_uid );
    js_setproperty( J, -2, "uid" );
    js_pushnumber( J, s.st_gid );
    js_setproperty( J, -2, "gid" );
    js_pushnumber( J, s.st_size );
    js_setproperty( J, -2, "size" );
    js_pushnumber( J, s.st_atime );
    js_setproperty( J, -2, "access_time" );
    js_pushnumber( J, s.st_mtime );
    js_setproperty( J, -2, "modification_time" );
    js_pushnumber( J, s.st_ctime );
    js_setproperty( J, -2, "creation_time" );
}

static void vlcjs_opendir( js_State *J )
{
    const char *psz_dir = js_tostring( J, 1 );
    DIR *p_dir;

    if( ( p_dir = vlc_opendir( psz_dir ) ) == NULL )
        return js_error( J, "cannot open directory `%s'.", psz_dir );

    js_newarray( J );
    int i;
    for( i=0 ; ; i++ )
    {
        const char *psz_filename = vlc_readdir( p_dir );
        if( !psz_filename ) break;
        js_pushstring( J, psz_filename );
        js_setindex( J, -2, i );
    }
    closedir( p_dir );
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_net_intf_reg[] = {
    { "listen_tcp", vlcjs_net_listen_tcp },
    { "connect_tcp", vlcjs_net_connect_tcp },
    { "close", vlcjs_net_close },
    { "send", vlcjs_net_send },
    { "recv", vlcjs_net_recv },
    { "poll", vlcjs_net_poll },
#ifndef _WIN32
    { "read", vlcjs_fd_read },
    { "write", vlcjs_fd_write },
#endif
    /* The following functions do not depend on intf_thread_t and do not really
     * belong in net.* but are left here for backward compatibility: */
    // { "url_parse", vlcjs_url_parse /* deprecated since 3.0.0 */ },
    // { "stat", vlcjs_stat }, /* Not really "net" */
    // { "opendir", vlcjs_opendir }, /* Not really "net" */
    { NULL, NULL }
};

static void vlcjs_register_net_intf( js_State *J )
{
    js_getglobal( J, "vlc" );
    js_newobject( J );
    vlcjs_register( J, vlcjs_net_intf_reg );
#define ADD_CONSTANT( value )    \
    js_pushnumber( J, POLL##value ); \
    js_setproperty( J, -2, "POLL"#value );
    ADD_CONSTANT( IN )
    ADD_CONSTANT( PRI )
    ADD_CONSTANT( OUT )
    ADD_CONSTANT( ERR )
    ADD_CONSTANT( HUP )
    ADD_CONSTANT( NVAL )
    js_setproperty( J, -2, "net" );
    js_pop( J, 1 ); // remove the vlc object from stack
}

void vlcjs_register_net( js_State *J, vlcjs_dtable_t *dt )
{
    dt->interrupt = vlc_interrupt_create();
    if( unlikely(dt->interrupt == NULL) )
        return -1;
    dt->fdv = NULL;
    dt->fdc = 0;
    vlcjs_set( J, vlcjs_get_dtable, dt );
    vlcjs_register_net_intf( J );
}

int vlcjs_fd_init( js_State *J, vlcjs_dtable_t *dt )
{
    dt->interrupt = vlc_interrupt_create();
    if( unlikely(dt->interrupt == NULL) )
        return -1;
    dt->fdv = NULL;
    dt->fdc = 0;
    js_newuserdata( J, "void*", dt, NULL);
    js_setregistry( J, "vlcjs_get_dtable");
    vlcjs_register_net_intf( J );
    return 0;
}

void vlcjs_fd_interrupt( vlcjs_dtable_t *dt )
{
    vlc_interrupt_kill( dt->interrupt );
}


void vlcjs_fd_cleanup( vlcjs_dtable_t *dt )
{
    for( unsigned i = 0; i < dt->fdc; i++ )
        if( dt->fdv[i] != -1 )
            net_Close( dt->fdv[i] );
    free( dt->fdv );
    vlc_interrupt_destroy(dt->interrupt);
}

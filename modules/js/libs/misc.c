/*****************************************************************************
 * misc.c  - this file is incomplete
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Pierre d'Herbemont <pdherbemont # videolan.org>
 *          Rémi Duraffort <ivoire # videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "general.h"


/*****************************************************************************
 * Get the VLC version string
 *****************************************************************************/
static void vlcjs_version( js_State *J )
{
    js_pushstring( J, VERSION_MESSAGE );
}

/*****************************************************************************
 * Get the VLC copyright
 *****************************************************************************/
static void vlcjs_copyright( js_State *J )
{
    js_pushstring( J, COPYRIGHT_MESSAGE );
}

/*****************************************************************************
 * Get the VLC license msg/disclaimer
 *****************************************************************************/
static void vlcjs_license( js_State *J )
{
    js_pushstring( J, LICENSE_MSG );
}

/*****************************************************************************
 * Quit VLC
 *****************************************************************************/
static void vlcjs_quit( js_State *J )
{
    vlc_object_t *p_this = vlcjs_get_this( J );
    /* The rc.c code also stops the playlist ... not sure if this is needed
     * though. */
    libvlc_Quit( vlc_object_instance(p_this) );
}

static void vlcjs_mdate( js_State *J )
{
    js_pushnumber( J, vlc_tick_now() );
}

static void vlcjs_mwait( js_State *J )
{
    double f = js_tonumber( J, 1 );
    
    vlc_interrupt_t *oint = NULL;  // comment this line when the below line is implemented
    // vlc_interrupt_t *oint = vlcjs_set_interrupt( J ); // complete this line with all dependencies
    int ret = vlc_mwait_i11e( llround(f) );

    vlc_interrupt_set( oint );
    if( ret ) // may require changes
    {   
        js_newerror( J, "Interrupted." );
    }
    js_pushundefined( J );
}

static void vlcjs_action_id( js_State *J )
{
    vlc_action_id_t i_key = vlc_actions_get_id( js_tostring( J, 1 ) );
    if (i_key == 0)
        js_pushundefined( J );
    js_pushnumber( J, i_key );
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_misc_reg[] = {
    { "version", vlcjs_version },
    { "copyright", vlcjs_copyright },
    { "license", vlcjs_license },

    { "action_id", vlcjs_action_id },

    { "mdate", vlcjs_mdate },
    { "mwait", vlcjs_mwait },

    { "quit", vlcjs_quit },

    { NULL, NULL }
};

void vlcjs_register_misc( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_misc_reg );
    js_setproperty( J, -2, "misc" );
    js_pop( J, 1 ); // pop the vlc object from stack
}



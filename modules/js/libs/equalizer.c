/*****************************************************************************
 * equalizer.c
 *****************************************************************************
 * Copyright (C) 2019 VideoLAN and VLC authors
 *
 * Authors: Akash Mehrotra < mehrotra <dot> akash <at> gmail <dot> com >
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"
#include "../../audio_filter/equalizer_presets.h"

#if !defined _WIN32
# include <locale.h>
#else
# include <windows.h>
#endif

#ifdef __APPLE__
#   include <string.h>
#   include <xlocale.h>
#endif

/*****************************************************************************
* Get the preamp level
*****************************************************************************/
static void vlcjs_preamp_get( js_State *J )
{
    audio_output_t *p_aout = vlc_player_aout_Hold( vlcjs_get_player( J ) );
    if( p_aout == NULL )
    {
        js_error( J, "unable to get aout" );
        return;
    }

    char *psz_af = var_GetNonEmptyString( p_aout, "audio-filter" );

    if( !psz_af || strstr ( psz_af, "equalizer" ) == NULL )
    {
        free( psz_af );
        aout_Release(p_aout);
        js_pushnull( J );
    }
    free( psz_af );

    js_pushnumber( J, var_GetFloat( p_aout, "equalizer-preamp") );
    aout_Release(p_aout);
}


/*****************************************************************************
* Set the preamp level
*****************************************************************************/
static void vlcjs_preamp_set( js_State *J )
{
    audio_output_t *p_aout = vlc_player_aout_Hold( vlcjs_get_player( J ) );
    if( p_aout == NULL )
    {
        js_error( J, "unable to get aout" );
        return;
    }

    char *psz_af = var_GetNonEmptyString( p_aout, "audio-filter" );
    if( !psz_af || strstr ( psz_af, "equalizer" ) == NULL )
    {
        free( psz_af );
        aout_Release(p_aout);
        js_pushundefined( J );
    }
    free( psz_af );

    var_SetFloat( p_aout, "equalizer-preamp", js_tonumber( J, 1 ) );
    aout_Release(p_aout);
    js_pushundefined( J );
}


/*****************************************************************************
Bands:
Band 0:  60 Hz
Band 1: 170 Hz
Band 2: 310 Hz
Band 3: 600 Hz
Band 4:  1 kHz
Band 5:  3 kHz
Band 6:  6 kHz
Band 7: 12 kHz
Band 8: 14 kHz
Band 9: 16 kHz
*****************************************************************************/
/*****************************************************************************
* Return EQ level for all bands as a Table
*****************************************************************************/
static void vlcjs_equalizer_get( js_State *J )
{
    const unsigned bands = 10;

    audio_output_t *p_aout = vlc_player_aout_Hold( vlcjs_get_player( J ) );
    if( p_aout == NULL )
    {
        js_error( J, "unable to get aout" );
        return;
    }

    char *psz_af = var_GetNonEmptyString( p_aout, "audio-filter" );
    if( !psz_af || strstr ( psz_af, "equalizer" ) == NULL )
    {
        free( psz_af );
        aout_Release(p_aout);
        js_pushnull( J );
        return;
    }
    free( psz_af );

    char *psz_bands_origin, *psz_bands;
    psz_bands_origin = psz_bands = var_GetNonEmptyString( p_aout, "equalizer-bands" );
    if( !psz_bands )
    {
        aout_Release(p_aout);
        js_pushnull( J );
        return;
    }

    locale_t loc = newlocale (LC_NUMERIC_MASK, "C", NULL);
    locale_t oldloc = uselocale (loc);
    js_newarray( J );
    for( unsigned i = 0; i < bands; i++ )
    {
        float level = strtof( psz_bands, &psz_bands );
        js_pushnumber( J, level );
        js_setindex( J , -2 , i );
    }

    free( psz_bands_origin );
    if( loc != (locale_t)0 )
    {
        uselocale (oldloc);
        freelocale (loc);
    }
    aout_Release(p_aout);
    // array is at top of the stack and is returned
}


/*****************************************************************************
* Set the equalizer level for the specified band
*****************************************************************************/
static void vlcjs_equalizer_set( js_State *J )
{
    int bandid = js_tonumber( J, 1 );
    if( bandid < 0 || bandid > 9)
    {
        js_error( J, "wrong aout band number" );
        return;
    }

    audio_output_t *p_aout = vlc_player_aout_Hold( vlcjs_get_player( J ) );
    if( p_aout == NULL )
    {
        js_error( J, "unable to get aout" );
        return;
    }

    char *psz_af = var_GetNonEmptyString( p_aout, "audio-filter" );
    if( !psz_af || strstr ( psz_af, "equalizer" ) == NULL )
    {
        free( psz_af );
        aout_Release(p_aout);
        js_pushundefined( J );
    }
    free( psz_af );

    float level = js_tonumber( J, 2 );
    char *bands = var_GetString( p_aout, "equalizer-bands" );

    locale_t loc = newlocale (LC_NUMERIC_MASK, "C", NULL);
    locale_t oldloc = uselocale (loc);
    char *b = bands;
    while( bandid > 0 )
    {
        float dummy = strtof( b, &b );
        (void)dummy;
        bandid--;
    }
    if( *b != '\0' )
        *b++ = '\0';
    float dummy = strtof( b, &b );
    (void)dummy;

    char *newstr;
    if( asprintf( &newstr, "%s %.1f%s", bands, level, b ) != -1 )
    {
        var_SetString( p_aout, "equalizer-bands", newstr );
        free( newstr );
    }

    if( loc != (locale_t)0 )
    {
        uselocale (oldloc);
        freelocale (loc);
    }
    free( bands );
    aout_Release(p_aout);
    js_pushundefined( J );
}

/*****************************************************************************
* Set the preset specified by preset id
*****************************************************************************/
static void vlcjs_equalizer_setpreset( js_State *J )
{
    int presetid = js_tointeger( J, 1 );
    if( presetid >= NB_PRESETS || presetid < 0 )
    {
        js_error( J, "wrong preset id" );
        return;
    }

    audio_output_t *p_aout = vlc_player_aout_Hold( vlcjs_get_player( J ) );
    if( p_aout == NULL )
    {
        js_error( J, "unable to get aout" );
        return;
    }

    int ret = 0;
    char *psz_af = var_InheritString( p_aout, "audio-filter" );
    if( psz_af != NULL && strstr ( psz_af, "equalizer" ) != NULL )
    {
        var_SetString( p_aout , "equalizer-preset" , preset_list[presetid] );
    }
    free( psz_af );
    aout_Release(p_aout);
    js_pushundefined( J );
}
 
/****************************************************************************
* Enable/disable Equalizer
*****************************************************************************/
static void vlcjs_equalizer_enable(js_State *J)
{
    bool state = js_toboolean( J , 1 );
    audio_output_t *aout = vlc_player_aout_Hold( vlcjs_get_player( J ) );
    if (aout)
    {
        aout_EnableFilter(aout, "equalizer", state);
        aout_Release (aout);
    }
    js_pushundefined( J );
}
/*****************************************************************************
* Get preset names
*****************************************************************************/
static void vlcjs_equalizer_get_presets( js_State *J )
{
    js_newarray( J );
    for( int i = 0 ; i < NB_PRESETS ; i++ )
    {
        js_pushstring( J, preset_list_text[i] );
        js_setindex( J , -2 , i );
    }
}

static const vlcjs_Reg vlcjs_equalizer_reg[] = {
    { "preampget", vlcjs_preamp_get },
    { "preampset", vlcjs_preamp_set },
    { "equalizerget", vlcjs_equalizer_get },
    { "equalizerset", vlcjs_equalizer_set },
    { "enable", vlcjs_equalizer_enable },
    { "presets",vlcjs_equalizer_get_presets },
    { "setpreset", vlcjs_equalizer_setpreset },
    { NULL, NULL }
};


void vlcjs_register_equalizer( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_equalizer_reg );
    js_setproperty( J, -2, "equalizer" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

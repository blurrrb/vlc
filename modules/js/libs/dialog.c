/*****************************************************************************
 * dialog.c: Functions to create interface dialogs from JS extensions
 *****************************************************************************
 * Copyright (C) 2009-2019 VideoLAN and authors
 *
 * Authors: Jean-Philippe André < jpeg # videolan.org >
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"
#include "../extension.h"
#include <vlc_dialog.h>
#include <vlc_extensions.h>
#include <stdio.h>
#include <stdlib.h>


#include "assert.h"

/*****************************************************************************
 *
 *****************************************************************************/

/* Dialog functions */
static void vlcjs_dialog_create( js_State *J );
static void vlcjs_dialog_delete( js_State *J );
static void vlcjs_dialog_show( js_State *J );
static void vlcjs_dialog_hide( js_State *J );
static void vlcjs_dialog_set_title( js_State *J );
static void vlcjs_dialog_update( js_State *J );
static void vlcjs_SetDialogUpdate( js_State *J, int flag );
static int vlcjs_GetDialogUpdate( js_State *J );

static void vlcjs_dialog_add_button( js_State *J );
static void vlcjs_dialog_add_label( js_State *J );
static void vlcjs_dialog_add_html( js_State *J );
static void vlcjs_dialog_add_text_inner( js_State *J, int );
static void vlcjs_dialog_add_text_input( js_State *J )
{
    vlcjs_dialog_add_text_inner( J, EXTENSION_WIDGET_TEXT_FIELD );
}
static void vlcjs_dialog_add_password( js_State *J )
{
    vlcjs_dialog_add_text_inner( J, EXTENSION_WIDGET_PASSWORD );
}
static void vlcjs_dialog_add_html( js_State *J )
{
    vlcjs_dialog_add_text_inner( J, EXTENSION_WIDGET_HTML );
}
static void vlcjs_dialog_add_check_box( js_State *J );
static void vlcjs_dialog_add_list( js_State *J );
static void vlcjs_dialog_add_dropdown( js_State *J );
static void vlcjs_dialog_add_image( js_State *J );
static void vlcjs_dialog_add_spin_icon( js_State *J );
static void vlcjs_dialog_delete__gc( js_State *J, void *data );
static void vlcjs_create_widget_inner( js_State *J, int i_args,
                                       extension_widget_t *p_widget);

static void vlcjs_dialog_delete_widget( js_State *J );

/* Widget methods */
static void vlcjs_widget_set_text( js_State *J );
static void vlcjs_widget_get_text( js_State *J );
static void vlcjs_widget_set_checked( js_State *J );
static void vlcjs_widget_get_checked( js_State *J );
static void vlcjs_widget_add_value( js_State *J );
static void vlcjs_widget_get_value( js_State *J );
static void vlcjs_widget_clear( js_State *J );
static void vlcjs_widget_get_selection( js_State *J );
static void vlcjs_widget_animate( js_State *J );
static void vlcjs_widget_stop( js_State *J );

/* Helpers */
static void AddWidget( extension_dialog_t *p_dialog,
                       extension_widget_t *p_widget );
static int DeleteWidget( extension_dialog_t *p_dialog,
                         extension_widget_t *p_widget );

static const vlcjs_Reg vlcjs_dialog_reg[] = {
    { "show", vlcjs_dialog_show },
    { "hide", vlcjs_dialog_hide },
    { "delete", vlcjs_dialog_delete },
    { "set_title", vlcjs_dialog_set_title },
    { "update", vlcjs_dialog_update },

    { "add_button", vlcjs_dialog_add_button },
    { "add_label", vlcjs_dialog_add_label },
    { "add_html", vlcjs_dialog_add_html },
    { "add_text_input", vlcjs_dialog_add_text_input },
    { "add_password", vlcjs_dialog_add_password },
    { "add_check_box", vlcjs_dialog_add_check_box },
    { "add_dropdown", vlcjs_dialog_add_dropdown },
    { "add_list", vlcjs_dialog_add_list },
    { "add_image", vlcjs_dialog_add_image },
    { "add_spin_icon", vlcjs_dialog_add_spin_icon },

    { "del_widget", vlcjs_dialog_delete_widget },
    { NULL, NULL }
};

static const vlcjs_Reg vlcjs_widget_reg[] = {
    { "set_text", vlcjs_widget_set_text },
    { "get_text", vlcjs_widget_get_text },
    { "set_checked", vlcjs_widget_set_checked },
    { "get_checked", vlcjs_widget_get_checked },
    { "add_value", vlcjs_widget_add_value },
    { "get_value", vlcjs_widget_get_value },
    { "clear", vlcjs_widget_clear },
    { "get_selection", vlcjs_widget_get_selection },
    { "animate", vlcjs_widget_animate },
    { "stop", vlcjs_widget_stop },
    { NULL, NULL }
};

/**
 * Open dialog library for Lua
 * @param L lua_State
 * @param opaque Object associated to this lua state
 * @note opaque will be p_ext for extensions, p_sd for service discoveries
 **/
void vlcjs_register_dialog( js_State *J, void *opaque )
{
    js_getglobal( J, "vlc" );
    js_newcfunction( J, vlcjs_dialog_create, "dialog", 0 );
    js_setproperty( J, -2, "dialog" );


    /* Add a private pointer (opaque) in the registry
     * The &key pointer is used to have a unique entry in the registry
     */
    vlcjs_set( J, opaque, "key_opaque" );

    /* Add private data: dialog update flag */
    vlcjs_SetDialogUpdate( J, 0 );
}

static void vlcjs_dialog_create( js_State *J )
{
    if( !js_isstring( J, 1 ) )
    {
        js_error( J, "usage: vlc.dialog(title)" );
        return;
    }
    const char *psz_title = js_tostring( J, 1 );

    vlc_object_t *p_this = vlcjs_get_this( J );

    extension_dialog_t *p_dlg = malloc( sizeof( extension_dialog_t ) );
    if( !p_dlg )
    {
        js_error( J, "out of memory" );
        return;
    }

    
    if( vlcjs_get( J, "__dialog" ) == NULL )
    {
        free( p_dlg );
        js_error( J, "Only one dialog allowed per extension!" );
        return;
    }

    p_dlg->p_object = p_this;
    p_dlg->psz_title = strdup( psz_title );

    if( !p_dlg->psz_title ){
        free( p_dlg );
        return js_error( J, "no memory" );
    }

    p_dlg->b_kill = false;
    ARRAY_INIT( p_dlg->widgets );

    /* Read the opaque value stored while loading the dialog library */
    
    p_dlg->p_sys = (void*) vlcjs_get( J, "key_opaque"); // "const" discarded

    vlc_mutex_init( &p_dlg->lock );
    vlc_cond_init( &p_dlg->cond );

    vlcjs_set( J, p_dlg, "__dialog" );

    extension_dialog_t **pp_dlg = malloc( sizeof( extension_dialog_t* ) );
    *pp_dlg = p_dlg;

    js_currentfunction( J );
    js_getproperty( J, -1, "prototype" );
    js_newuserdata( J, "dialog", pp_dlg, vlcjs_dialog_delete__gc);
    
    vlcjs_register( J, vlcjs_dialog_reg );

    msg_Dbg( p_this, "Creating dialog '%s'", psz_title );
    vlcjs_SetDialogUpdate( J, 0 );
}

static void vlcjs_dialog_delete( js_State *J ){
    vlcjs_dialog_delete__gc( J, js_touserdata( J, 0, "dialog"));
}

static void vlcjs_dialog_delete__gc( js_State *J, void *data )
{
    vlc_object_t *p_mgr = vlcjs_get_this( J );

    /* Get dialog descriptor */
    extension_dialog_t **pp_dlg =
            (extension_dialog_t**) data;

    if( !pp_dlg || !*pp_dlg )
    {
        js_error( J, "Can't get pointer to dialog" );
        return;
    }

    extension_dialog_t *p_dlg = *pp_dlg;
    *pp_dlg = NULL;

    /* Remove private __dialog field */
    vlcjs_set( J, NULL, "__dialog" );

    assert( !p_dlg->b_kill );

    /* Immediately deleting the dialog */
    msg_Dbg( p_mgr, "Deleting dialog '%s'", p_dlg->psz_title );
    p_dlg->b_kill = true;
    vlcjs_SetDialogUpdate( J, 0 ); // Reset the update flag
    vlc_ext_dialog_update( p_mgr, p_dlg );

    /* After vlc_ext_dialog_update, the UI thread must take the lock asap and
     * then signal us when it's done deleting the dialog.
     */
    msg_Dbg( p_mgr, "Waiting for the dialog to be deleted..." );
    vlc_mutex_lock( &p_dlg->lock );
    while( p_dlg->p_sys_intf != NULL )
    {
        vlc_cond_wait( &p_dlg->cond, &p_dlg->lock );
    }
    vlc_mutex_unlock( &p_dlg->lock );

    free( p_dlg->psz_title );
    p_dlg->psz_title = NULL;

    /* Destroy widgets */
    extension_widget_t *p_widget;
    ARRAY_FOREACH( p_widget, p_dlg->widgets )
    {
        if( !p_widget )
            continue;
        free( p_widget->psz_text );

        /* Free data */
        struct extension_widget_value_t *p_value, *p_next;
        for( p_value = p_widget->p_values; p_value != NULL; p_value = p_next )
        {
            p_next = p_value->p_next;
            free( p_value->psz_text );
            free( p_value );
        }
        free( p_widget );
    }

    ARRAY_RESET( p_dlg->widgets );

    /* Note: At this point, the UI must not use these resources */
    vlc_mutex_destroy( &p_dlg->lock );
    vlc_cond_destroy( &p_dlg->cond );
    free( p_dlg );
}

static void _vlcjs_dialog_show_hide( js_State *J, int b)
{
    extension_dialog_t **pp_dlg =
            (extension_dialog_t**) js_touserdata( J, 0, "dialog" );
    if( !pp_dlg || !*pp_dlg )
    {
        js_error( J, "Can't get pointer to dialog" );
        return;
    }
    extension_dialog_t *p_dlg = *pp_dlg;

    p_dlg->b_hide = b;
    vlcjs_SetDialogUpdate( J, 1 );
    js_pushundefined( J );
}

/** Show the dialog */
static void vlcjs_dialog_show( js_State *J )
{
    _vlcjs_dialog_show_hide( J, true );
}

/** Hide the dialog */
static void vlcjs_dialog_hide( js_State *J )
{
    _vlcjs_dialog_show_hide( J, false );
}


/** Set the dialog's title */
static void vlcjs_dialog_set_title( js_State *J )
{
    extension_dialog_t **pp_dlg =
            (extension_dialog_t**) js_touserdata( J, 0, "dialog" );
    if( !pp_dlg || !*pp_dlg )
    {
        js_error( J, "Can't get pointer to dialog" );
        return;
    }
    extension_dialog_t *p_dlg = *pp_dlg;

    if( !js_isstring( J, 1 ) )
        return js_error( J, "no title supplied");

    vlc_mutex_lock( &p_dlg->lock );

    free( p_dlg->psz_title );

    p_dlg->psz_title = strdup( js_tostring( J, 1 ) );

    vlc_mutex_unlock( &p_dlg->lock );

    if( !p_dlg->psz_title )
        return js_error( J, "no memory" );

    vlcjs_SetDialogUpdate( J, 1 );
    js_pushundefined( J );

}

/** Update the dialog immediately */
static void vlcjs_dialog_update( js_State *J )
{
    vlc_object_t *p_mgr = vlcjs_get_this( J );

    extension_dialog_t **pp_dlg =
            (extension_dialog_t**) js_touserdata( J, 0, "dialog" );
    if( !pp_dlg || !*pp_dlg )
    {
        js_error( J, "Can't get pointer to dialog" );
        return;
    }
    extension_dialog_t *p_dlg = *pp_dlg;

    // Updating dialog immediately
    vlc_ext_dialog_update( p_mgr, p_dlg );

    // Reset update flag
    vlcjs_SetDialogUpdate( J, 0 );

    js_pushundefined( J );
}

static void vlcjs_SetDialogUpdate( js_State *J, int flag )
{
    /* Set entry in the Lua registry */
    js_pushnumber( J, flag );
    js_setregistry( J, "key_update" );
}

static int vlcjs_GetDialogUpdate( js_State *J )
{
    /* Read entry in the Lua registry */
    js_getregistry( J, "key_update" );
    int flag = js_tointeger( J, -1 );
    js_pop( J, 1 );
    return flag;
}

/** Manually update a dialog
 * This can be called after a lua_pcall
 * @return SUCCESS if there is no dialog or the update was successful
 * @todo If there can be multiple dialogs, this function will have to
 * be fixed (lookup for dialog)
 */
int vlcjs_DialogFlush( js_State *J )
{
    extension_dialog_t *p_dlg = ( extension_dialog_t* ) vlcjs_get( J, "__dialog" );

    if( !p_dlg )
    {
        return VLC_SUCCESS;
    }

    int i_ret = VLC_SUCCESS;
    if( vlcjs_GetDialogUpdate( J ) )
    {
        i_ret = vlc_ext_dialog_update( vlcjs_get_this( J ), p_dlg );
        vlcjs_SetDialogUpdate( J, 0 );
    }
    return i_ret;
}

/**
 * Create a button: add_button
 * Arguments: text, function (as string)
 * Qt: QPushButton
 **/
static void vlcjs_dialog_add_button( js_State *J )
{
    /* Verify arguments */
    if( !js_isstring( J, 1 ) || !js_iscallable( J, 2 ) )
    {
        js_error( J, "usage: dialog.add_button(text, func)" );
        return;
    }

    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );
    p_widget->type = EXTENSION_WIDGET_BUTTON;
    p_widget->psz_text = strdup( js_tostring( J, 1 ) );
    if( !p_widget->psz_text ){
        free( p_widget );
        return js_error( J, "no memory" );
    }
    
    vlcjs_create_widget_inner( J, 2, p_widget );

    char* widget_ref = js_ref( J );
    p_widget->p_sys = widget_ref;
    js_getregistry( J, widget_ref );
}

/**
 * Create a text label: add_label
 * Arguments: text
 * Qt: QLabel
 **/
static void vlcjs_dialog_add_label( js_State *J )
{
    /* Verify arguments */
    if( !js_isstring( J, 1 ) )
    {
        js_error( J, "usage: dialog.add_label(text)" );
        return;
    }
    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );
    p_widget->type = EXTENSION_WIDGET_LABEL;
    p_widget->psz_text = strdup( js_tostring( J, 1 ) );

    if( !p_widget->psz_text ){
        free( p_widget );
        return js_error( J, "no memory" );
    }

    vlcjs_create_widget_inner( J, 1, p_widget );
}

/**
 * Create a text area: add_html, add_text_input, add_password
 * Arguments: text (may be nil)
 * Qt: QLineEdit (Text/Password) or QTextArea (HTML)
 **/
static void vlcjs_dialog_add_text_inner( js_State *J, int i_type )
{
    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );
    p_widget->type = i_type;
    if( !js_isnull( J, 1 ) )
        p_widget->psz_text = strdup( js_tostring( J, 1 ) );
    
    if( !p_widget->psz_text ){
        free( p_widget );
        return VLC_ENOMEM;
    }

    vlcjs_create_widget_inner( J, 1, p_widget );
}

/**
 * Create a checkable box: add_check_box
 * Arguments: text, checked (as bool)
 * Qt: QCheckBox
 **/
static void vlcjs_dialog_add_check_box( js_State *J )
{
    /* Verify arguments */
    if( !js_isstring( J, 1 ) )
    {
        js_error( J, "usage: dialog.add_check_box(text, checked)" );
        return;
    }

    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );

    if( !p_widget ){
        return js_error( J, "no memory" );
    }

    p_widget->type = EXTENSION_WIDGET_CHECK_BOX;
    p_widget->psz_text = strdup( js_tostring( J, 1 ) );

    if( !p_widget->psz_text){
        free( p_widget );
        return js_error( J, "no memory" );
    }

    p_widget->b_checked = js_toboolean( J, 2 );

    vlcjs_create_widget_inner( J, 2, p_widget );
}

/**
 * Create a drop-down list (non editable)
 * Arguments: (none)
 * Qt: QComboBox
 * @todo make it editable?
 **/
static void vlcjs_dialog_add_dropdown( js_State *J )
{
    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );
    p_widget->type = EXTENSION_WIDGET_DROPDOWN;

    vlcjs_create_widget_inner( J, 0, p_widget );
}

/**
 * Create a list panel (multiple selection)
 * Arguments: (none)
 * Qt: QListWidget
 **/
static void vlcjs_dialog_add_list( js_State *J )
{
    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );
    p_widget->type = EXTENSION_WIDGET_LIST;

    vlcjs_create_widget_inner( J, 0, p_widget );
}

/**
 * Create an image label
 * Arguments: (string) url
 * Qt: QLabel with setPixmap( QPixmap& )
 **/
static void vlcjs_dialog_add_image( js_State *J )
{
    /* Verify arguments */
    if( !js_isstring( J, 1 ) )
    {
        js_error( J, "usage: dialog.add_image(filename)" );
        return;
    }

    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );
    p_widget->type = EXTENSION_WIDGET_IMAGE;
    p_widget->psz_text = strdup( js_tostring( J, 1 ) );

    if( !p_widget->psz_text ){
        free( p_widget );
        return js_error( J, "no memory" );
    }

    vlcjs_create_widget_inner( J, 1, p_widget );
}

/**
 * Create a spinning icon
 * Arguments: (int) loop count to play: 0 means stopped, -1 means infinite.
 * Qt: SpinningIcon (custom widget)
 **/
static void vlcjs_dialog_add_spin_icon( js_State *J )
{
    /* Verify arguments */
    if( !js_isstring( J, 1 ) )
    {
        js_error( J, "usage: dialog.spin_icon(spin_count)" );
        return;
    }
    extension_widget_t *p_widget = malloc( sizeof( extension_widget_t ) );
    p_widget->type = EXTENSION_WIDGET_SPIN_ICON;
    p_widget->i_spin_loops = js_tointeger( J, 1 );

    vlcjs_create_widget_inner( J, 1, p_widget );
}

/**
 * Internal helper to finalize the creation of a widget
 * @param L Lua State
 * @param i_args Number of arguments before "row" (1 or more) including the this object
 * @param p_widget The widget to add
 **/
static void vlcjs_create_widget_inner( js_State *J, int i_args,
                                       extension_widget_t *p_widget )
{
    int arg = i_args;

    /* Get dialog */
    extension_dialog_t **pp_dlg =
            (extension_dialog_t**) js_touserdata( J, 0, "dialog" );
    if( !pp_dlg || !*pp_dlg )
    {
        js_error( J, "Can't get pointer to dialog" );
        return;
    }
    extension_dialog_t *p_dlg = *pp_dlg;

    /* Set parent dialog */
    p_widget->p_dialog = p_dlg;

    /* Set common arguments: col, row, hspan, vspan, width, height */
    if( js_isnumber( J, arg ) )
        p_widget->i_column = js_tointeger( J, arg );
    else goto end_of_args;
    if( js_isnumber( J, ++arg ) )
        p_widget->i_row = js_tointeger( J, arg );
    else goto end_of_args;
    if( js_isnumber( J, ++arg ) )
        p_widget->i_horiz_span = js_tointeger( J, arg );
    else goto end_of_args;
    if( js_isnumber( J, ++arg ) )
        p_widget->i_vert_span = js_tointeger( J, arg );
    else goto end_of_args;
    if( js_isnumber( J, ++arg ) )
        p_widget->i_width = js_tointeger( J, arg );
    else goto end_of_args;
    if( js_isnumber( J, ++arg ) )
        p_widget->i_height = js_tointeger( J, arg );
    else goto end_of_args;

end_of_args:
    vlc_mutex_lock( &p_dlg->lock );

    /* Add the widget to the dialog descriptor */
    AddWidget( p_dlg, p_widget );

    vlc_mutex_unlock( &p_dlg->lock );

    /* Create meta table */
    extension_widget_t **pp_widget = malloc( sizeof( extension_widget_t* ) );
    *pp_widget = p_widget;
    js_currentfunction( J );
    js_getproperty( J, -1, "prototype" );
    js_newuserdata( J, "widget", pp_widget, NULL );
    vlcjs_register( J, vlcjs_widget_reg );

    vlcjs_SetDialogUpdate( J, 1 );

    // the widget object is at the top of stack and it returned
}

static void vlcjs_widget_set_text( js_State *J )
{
    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    /* Verify arguments */
    if( !js_isstring( J, 1 ) )
    {
        js_error( J, "usage: widget.set_text(text)" );
        return;
    }

    /* Verify widget type */
    switch( p_widget->type )
    {
        case EXTENSION_WIDGET_LABEL:
        case EXTENSION_WIDGET_BUTTON:
        case EXTENSION_WIDGET_HTML:
        case EXTENSION_WIDGET_TEXT_FIELD:
        case EXTENSION_WIDGET_PASSWORD:
        case EXTENSION_WIDGET_DROPDOWN:
        case EXTENSION_WIDGET_CHECK_BOX:
            break;
        case EXTENSION_WIDGET_LIST:
        case EXTENSION_WIDGET_IMAGE:
        default:
            js_error( J, "method set_text not valid for this widget" );
            return;
    }

    vlc_mutex_lock( &p_widget->p_dialog->lock );

    /* Update widget */
    p_widget->b_update = true;
    free( p_widget->psz_text );
    p_widget->psz_text = strdup( js_tostring( J, 1 ) );

    vlc_mutex_unlock( &p_widget->p_dialog->lock );

    if( !p_widget->psz_text ) return js_error( J, "no memory" );

    vlcjs_SetDialogUpdate( J, 1 );
    js_pushundefined( J);
}

static void vlcjs_widget_get_text( js_State *J )
{
    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    /* Verify widget type */
    switch( p_widget->type )
    {
        case EXTENSION_WIDGET_LABEL:
        case EXTENSION_WIDGET_BUTTON:
        case EXTENSION_WIDGET_HTML:
        case EXTENSION_WIDGET_TEXT_FIELD:
        case EXTENSION_WIDGET_PASSWORD:
        case EXTENSION_WIDGET_DROPDOWN:
        case EXTENSION_WIDGET_CHECK_BOX:
            break;
        case EXTENSION_WIDGET_LIST:
        case EXTENSION_WIDGET_IMAGE:
        default:
            js_error( J, "method get_text not valid for this widget" );
            return;
    }

    extension_dialog_t *p_dlg = p_widget->p_dialog;
    vlc_mutex_lock( &p_dlg->lock );

    if( p_widget->psz_text )
        js_pushstring( J, p_widget->psz_text );
        
    vlc_mutex_unlock( &p_dlg->lock );

}

static void vlcjs_widget_get_checked( js_State *J )
{
    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_CHECK_BOX )
    {
        js_error( J, "method get_checked not valid for this widget" );
        return;
    }

    vlc_mutex_lock( &p_widget->p_dialog->lock );
    js_pushboolean( J, p_widget->b_checked );
    vlc_mutex_unlock( &p_widget->p_dialog->lock );
}

static void vlcjs_widget_add_value( js_State *J )
{
    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_DROPDOWN
        && p_widget->type != EXTENSION_WIDGET_LIST )
        return js_error( J, "method add_value not valid for this widget" );

    if( !js_isstring( J, 1 ) )
        return js_error( J, "usage: widget.add_value(text, id = 0)" );

    struct extension_widget_value_t *p_value,
        *p_new_value = malloc( sizeof( struct extension_widget_value_t ) );

    if( !p_new_value ) return js_error( J, "no memory" );
    
    p_new_value->psz_text = strdup( js_tostring( J, 1 ) );

    if( !p_new_value->psz_text ){
        free( p_new_value );
        return js_error( J, "no memory" );
    }

    p_new_value->i_id = js_tointeger( J, 2 ); // defaults to 0

    vlc_mutex_lock( &p_widget->p_dialog->lock );

    if( !p_widget->p_values )
    {
        p_widget->p_values = p_new_value;
        if( p_widget->type == EXTENSION_WIDGET_DROPDOWN )
            p_new_value->b_selected = true;
    }
    else
    {
        for( p_value = p_widget->p_values;
             p_value->p_next != NULL;
             p_value = p_value->p_next )
        { /* Do nothing, iterate to find the end */ }
        p_value->p_next = p_new_value;
    }

    p_widget->b_update = true;
    vlc_mutex_unlock( &p_widget->p_dialog->lock );

    vlcjs_SetDialogUpdate( J, 1 );

    js_pushundefined( J );
}

static void vlcjs_widget_get_value( js_State *J )
{
    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_DROPDOWN )
        return js_error( J, "method get_value not valid for this widget" );

    vlc_mutex_lock( &p_widget->p_dialog->lock );

    struct extension_widget_value_t *p_value;
    for( p_value = p_widget->p_values;
         p_value != NULL;
         p_value = p_value->p_next )
    {
        if( p_value->b_selected )
        {
            js_newobject( J );
            js_pushnumber( J, p_value->i_id );
            js_setproperty( J, -2, "id" );
            js_pushstring( J, p_value->psz_text );
            js_setproperty( J, -2, "value" );
            vlc_mutex_unlock( &p_widget->p_dialog->lock );
            // object at top of the stack is returned
            return;
        }
    }

    vlc_mutex_unlock( &p_widget->p_dialog->lock );

    js_newobject( J );
    js_pushnumber( J, -1 );
    js_setproperty( J, -2, "id" );
    js_pushnull( J );
    js_setproperty( J, -2, "value" );
    // object at top of the stack is returned
}

static void vlcjs_widget_clear( js_State *J )
{
    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_DROPDOWN
        && p_widget->type != EXTENSION_WIDGET_LIST )
        return js_error( J, "method clear not valid for this widget" );

    struct extension_widget_value_t *p_value, *p_next;

    vlc_mutex_lock( &p_widget->p_dialog->lock );

    for( p_value = p_widget->p_values;
         p_value != NULL;
         p_value = p_next )
    {
        p_next = p_value->p_next;
        free( p_value->psz_text );
        free( p_value );
    }

    p_widget->p_values = NULL;
    p_widget->b_update = true;

    vlc_mutex_unlock( &p_widget->p_dialog->lock );

    vlcjs_SetDialogUpdate( J, 1 );
    js_pushundefined( J );
}

static void vlcjs_widget_get_selection( js_State *J )
{
    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_LIST )
        return js_error( J, "method get_selection not valid for this widget" );

    /* Create empty table */
    js_newobject( J );

    vlc_mutex_lock( &p_widget->p_dialog->lock );

    struct extension_widget_value_t *p_value;
    for( p_value = p_widget->p_values;
         p_value != NULL;
         p_value = p_value->p_next )
    {
        if( p_value->b_selected )
        {
            js_pushstring( J, p_value->psz_text );
            char * i_id;
            sprintf( i_id, "%d", p_value->i_id );
            js_setproperty( J, -2, i_id );
        }
    }

    vlc_mutex_unlock( &p_widget->p_dialog->lock );
    // use Object.keys(obj) to obtain a list of keys
}

static void vlcjs_widget_set_checked( js_State *J )
{
    /* Get dialog */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_CHECK_BOX )
        return js_error( J, "method set_checked not valid for this widget" );

    /* Verify arguments */
    if( !js_isboolean( J, 1 ) )
        return js_error( J, "usage: widget.set_checked(bool)" );

    vlc_mutex_lock( &p_widget->p_dialog->lock );

    bool b_old_check = p_widget->b_checked;
    p_widget->b_checked = js_toboolean( J, 1 );

    vlc_mutex_unlock( &p_widget->p_dialog->lock );

    if( b_old_check != p_widget->b_checked )
    {
        /* Signal interface of the change */
        p_widget->b_update = true;
        vlcjs_SetDialogUpdate( J, 1 );
    }

    js_pushundefined( J );
}

static void vlcjs_widget_animate( js_State *J )
{
    /* Get dialog */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_SPIN_ICON )
        return js_error( J, "method animate not valid for this widget" );

    /* Verify arguments */
    vlc_mutex_lock( &p_widget->p_dialog->lock );
    if( !js_isnumber( J, 1 ) )
        p_widget->i_spin_loops = -1;
    else
        p_widget->i_spin_loops = js_tointeger( J, 1 );
    vlc_mutex_unlock( &p_widget->p_dialog->lock );

    /* Signal interface of the change */
    p_widget->b_update = true;
    vlcjs_SetDialogUpdate( J, 1 );

    js_pushundefined( J );
}

static void vlcjs_widget_stop( js_State *J )
{
    /* get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 0, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    if( p_widget->type != EXTENSION_WIDGET_SPIN_ICON )
        return js_error( J, "method stop not valid for this widget" );

    vlc_mutex_lock( &p_widget->p_dialog->lock );

    bool b_needs_update = p_widget->i_spin_loops != 0;
    p_widget->i_spin_loops = 0;

    vlc_mutex_unlock( &p_widget->p_dialog->lock );

    if( b_needs_update )
    {
        /* Signal interface of the change */
        p_widget->b_update = true;
        vlcjs_SetDialogUpdate( J, 1 );
    }

    js_pushundefined( J );
}

/**
 * Delete a widget from a dialog
 * Remove it from the list once it has been safely destroyed by the interface
 * @note This will always update the dialog
 **/
static void vlcjs_dialog_delete_widget( js_State *J )
{
    /* Get dialog */
    extension_dialog_t **pp_dlg =
            (extension_dialog_t**) js_touserdata( J, 0, "dialog" );
    if( !pp_dlg || !*pp_dlg )
        return js_error( J, "Can't get pointer to dialog" );
    extension_dialog_t *p_dlg = *pp_dlg;

    /* Get widget */
    if( !js_isuserdata( J, 1, "widget" ) )
        return js_error( J, "Argument to del_widget is not a widget" );

    /* Get widget */
    extension_widget_t **pp_widget =
            (extension_widget_t **) js_touserdata( J, 1, "widget" );
    if( !pp_widget || !*pp_widget )
    {
        js_error( J, "Can't get pointer to widget" );
        return;
    }
    extension_widget_t *p_widget = *pp_widget;

    /* Delete widget */
    *pp_widget = NULL;
    // if( p_widget->type == EXTENSION_WIDGET_BUTTON )
    // {
    //     /* Remove button action from registry */
    //     lua_pushlightuserdata( L, p_widget );
    //     lua_pushnil( L );
    //     lua_settable( L, LUA_REGISTRYINDEX );
    // }

    vlc_object_t *p_mgr = vlcjs_get_this( J );

    p_widget->b_kill = true;

    vlcjs_SetDialogUpdate( J, 0 ); // Reset update flag
    int i_ret = vlc_ext_dialog_update( p_mgr, p_dlg );

    if( i_ret != VLC_SUCCESS )
    {
        return js_error( J, "Could not delete widget" );
    }

    vlc_mutex_lock( &p_dlg->lock );

    /* Same remarks as for dialog delete.
     * If the dialog is deleted or about to be deleted, then there is no
     * need to wait on this particular widget that already doesn't exist
     * anymore in the UI */
    while( p_widget->p_sys_intf != NULL && !p_dlg->b_kill
           && p_dlg->p_sys_intf != NULL )
    {
        vlc_cond_wait( &p_dlg->cond, &p_dlg->lock );
    }

    i_ret = DeleteWidget( p_dlg, p_widget );

    vlc_mutex_unlock( &p_dlg->lock );

    if( i_ret != VLC_SUCCESS )
    {
        return js_error( J, "Could not remove widget from list" );
    }

    js_pushundefined( J );
}


/*
 * Below this line, no Lua specific code.
 * Extension helpers.
 */


/**
 * Add a widget to the widget list of a dialog
 * @note Must be entered with lock on dialog
 **/
static void AddWidget( extension_dialog_t *p_dialog,
                       extension_widget_t *p_widget )
{
    ARRAY_APPEND( p_dialog->widgets, p_widget );
}

/**
 * Remove a widget from the widget list of a dialog
 * @note The widget MUST have been safely killed before
 * @note Must be entered with lock on dialog
 **/
static int DeleteWidget( extension_dialog_t *p_dialog,
                         extension_widget_t *p_widget )
{
    int pos = -1;
    bool found = false;
    extension_widget_t *p_iter;
    ARRAY_FOREACH( p_iter, p_dialog->widgets )
    {
        pos++;
        if( p_iter == p_widget )
        {
            found = true;
            break;
        }
    }

    if( !found )
        return VLC_EGENERIC;

    ARRAY_REMOVE( p_dialog->widgets, pos );

    /* Now free the data */
    free( p_widget->p_sys );
    struct extension_widget_value_t *p_value = p_widget->p_values;
    while( p_value )
    {
        free( p_value->psz_text );
        struct extension_widget_value_t *old = p_value;
        p_value = p_value->p_next;
        free( old );
    }
    free( p_widget->psz_text );
    free( p_widget );

    return VLC_SUCCESS;
}

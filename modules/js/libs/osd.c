/*****************************************************************************
 * osd.c: Generic js interface functions
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include <vlc_vout.h>
#include <vlc_vout_osd.h>

#include "general.h"

/*****************************************************************************
 * OSD
 *****************************************************************************/
static int vlc_osd_icon_from_string( const char *psz_name )
{
    static const struct
    {
        int i_icon;
        const char *psz_name;
    } pp_icons[] =
        { { OSD_PAUSE_ICON, "pause" },
          { OSD_PLAY_ICON, "play" },
          { OSD_SPEAKER_ICON, "speaker" },
          { OSD_MUTE_ICON, "mute" },
          { 0, NULL } };
    int i;
    for( i = 0; pp_icons[i].psz_name; i++ )
    {
        if( !strcmp( psz_name, pp_icons[i].psz_name ) )
            return pp_icons[i].i_icon;
    }
    return 0;
}

static void vlcjs_osd_icon( js_State *J )
{
    const char *psz_icon = js_tostring( J, 1 );
    int i_icon = vlc_osd_icon_from_string( psz_icon );
    int i_chan;
    if( js_isdefined( J, 2 ) && js_isnumber( J, 2 ) )
        i_chan = js_tointeger( J, 2 );
    else
        i_chan = VOUT_SPU_CHANNEL_OSD;

    if( !i_icon )
        return js_error( J, "\"%s\" is not a valid osd icon.", psz_icon );

    vout_thread_t *p_vout = vlcjs_get_vout( J );
    if( p_vout )
    {
        vout_OSDIcon( p_vout, i_chan, i_icon );
        vout_Release( p_vout );
    }
    js_pushundefined( J );
}

static int vlc_osd_position_from_string( const char *psz_name )
{
    static const struct
    {
        int i_position;
        const char *psz_name;
    } pp_icons[] =
        { { 0,                                              "center"       },
          { SUBPICTURE_ALIGN_LEFT,                          "left"         },
          { SUBPICTURE_ALIGN_RIGHT,                         "right"        },
          { SUBPICTURE_ALIGN_TOP,                           "top"          },
          { SUBPICTURE_ALIGN_BOTTOM,                        "bottom"       },
          { SUBPICTURE_ALIGN_TOP   |SUBPICTURE_ALIGN_LEFT,  "top-left"     },
          { SUBPICTURE_ALIGN_TOP   |SUBPICTURE_ALIGN_RIGHT, "top-right"    },
          { SUBPICTURE_ALIGN_BOTTOM|SUBPICTURE_ALIGN_LEFT,  "bottom-left"  },
          { SUBPICTURE_ALIGN_BOTTOM|SUBPICTURE_ALIGN_RIGHT, "bottom-right" },
          { 0, NULL } };
    int i;
    for( i = 0; pp_icons[i].psz_name; i++ )
    {
        if( !strcmp( psz_name, pp_icons[i].psz_name ) )
            return pp_icons[i].i_position;
    }
    return 0;
}

static void vlcjs_osd_message( js_State *J )
{
    const char *psz_message = js_tostring( J, 1 );
    int i_chan;
    if( js_isdefined( J, 2 ) && js_isnumber( J, 2) )
        i_chan = js_tointeger( J, 2 );
    else
        i_chan = VOUT_SPU_CHANNEL_OSD;
    
    const char *psz_position;
    if( js_isdefined( J, 3 ) && js_isstring( J, 3) )
        psz_position = js_tostring( J, 3 );
    else
        psz_position = "top-right";
    
    vlc_tick_t duration;
    if( js_isdefined( J, 4 ) && js_isnumber( J, 4) )
        duration = js_tointeger( J, 4 );
    else
        duration = VLC_TICK_FROM_SEC( 1 );

    vout_thread_t *p_vout = vlcjs_get_vout( J );
    if( p_vout )
    {
        vout_OSDText( p_vout, i_chan, vlc_osd_position_from_string( psz_position ),
                      duration, psz_message );
        vout_Release( p_vout );
    }
    js_pushundefined( J );
}

static int vlc_osd_slider_type_from_string( const char *psz_name )
{
    static const struct
    {
        int i_type;
        const char *psz_name;
    } pp_types[] =
        { { OSD_HOR_SLIDER, "horizontal" },
          { OSD_VERT_SLIDER, "vertical" },
          { 0, NULL } };
    int i;
    for( i = 0; pp_types[i].psz_name; i++ )
    {
        if( !strcmp( psz_name, pp_types[i].psz_name ) )
            return pp_types[i].i_type;
    }
    return 0;
}

static void vlcjs_osd_slider( js_State *J )
{
    int i_position = js_tointeger( J, 1 );
    const char *psz_type = js_tostring( J, 2 );
    int i_type = vlc_osd_slider_type_from_string( psz_type );
    int i_chan;
    if( js_isdefined( J, 3 ) && js_isnumber( J, 3) )
        i_chan = js_tointeger( J, 3 );
    else
        i_chan = VOUT_SPU_CHANNEL_OSD;
    
    if( !i_type )
        return js_error( J, "\"%s\" is not a valid slider type.",
                           psz_type );

    vout_thread_t *p_vout = vlcjs_get_vout( J );
    if( p_vout )
    {
        vout_OSDSlider( p_vout, i_chan, i_position, i_type );
        vout_Release( p_vout );
    }
    js_pushundefined( J );
}

static void vlcjs_spu_channel_register( js_State *J )
{
    vout_thread_t *p_vout = vlcjs_get_vout( J );
    if( !p_vout )
        return js_error( J, "Unable to find vout." );

    int i_chan = vout_RegisterSubpictureChannel( p_vout );
    vout_Release( p_vout );
    js_pushnumber( J, i_chan );
}

static void vlcjs_spu_channel_clear( js_State *J )
{
    int i_chan = js_tointeger( J, 1 );
    vout_thread_t *p_vout = vlcjs_get_vout( J );
    if( !p_vout )
        return js_error( J, "Unable to find vout." );

    vout_FlushSubpictureChannel( p_vout, i_chan );
    vout_Release(p_vout);
    js_pushundefined( J );
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_osd_reg[] = {
    { "icon", vlcjs_osd_icon },
    { "message", vlcjs_osd_message },
    { "slider", vlcjs_osd_slider },
    { "channel_register", vlcjs_spu_channel_register },
    { "channel_clear", vlcjs_spu_channel_clear },
    { NULL, NULL }
};

void vlcjs_register_osd( js_State *J )
{
    js_getglobal( J, "vlc" );
    vlcjs_register( J, vlcjs_osd_reg );
    js_setproperty( J, -2, "osd" );
    js_pop( J, 1 );
}

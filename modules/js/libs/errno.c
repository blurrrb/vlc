/*****************************************************************************
 * errno.c
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Hugo Beauzée-Luyssen <hugo@beauzee.fr>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "general.h"

void vlcjs_register_errno( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );

#define ADD_ERRNO_VALUE( value )  \
    js_pushnumber( J, value );  \
    js_setproperty( J, -2, #value );

    ADD_ERRNO_VALUE( ENOENT );
    ADD_ERRNO_VALUE( EEXIST );
    ADD_ERRNO_VALUE( EACCES );
    ADD_ERRNO_VALUE( EINVAL );

#undef ADD_ERRNO_VALUE

    js_setproperty( J, -2, "errno" );
    js_pop( J, 1 ); // pop the vlc object from the stack 
}

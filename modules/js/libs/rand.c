/*****************************************************************************
 * rand.c: random number/bytes generation functions
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "general.h"

static void vlcjs_rand_number( js_State *J )
{
    long rand = vlc_lrand48();
    js_pushnumber( J, rand );
}

static void vlcjs_rand_bytes( js_State *J )
{
    int i_size = js_tointeger( J, 1 );
    char* p_buff = malloc( i_size * sizeof( *p_buff ) );
    if ( unlikely( p_buff == NULL ) )
    {
        js_error( J, "error allocating memory" );
        return;
    }
    vlc_rand_bytes( p_buff, i_size );
    js_pushlstring( J, p_buff, i_size );
    free( p_buff );
}

static const vlcjs_Reg vlcjs_rand_reg[] = {
    { "number", vlcjs_rand_number },
    { "bytes", vlcjs_rand_bytes },
    { NULL, NULL }
};

void vlcjs_register_rand( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_rand_reg );
    js_setproperty( J, -2, "rand" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

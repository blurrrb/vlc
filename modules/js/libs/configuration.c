/*****************************************************************************
 * configuration.c: Generic js<->vlc config interface
 *****************************************************************************
 * Copyright (C) 2009-2019 VideoLAN and authors
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "general.h"

/*****************************************************************************
 * Config handling
 *****************************************************************************/
static void vlcjs_config_get( js_State *J )
{
    const char *psz_name = js_tostring( J, 1 );
    switch( config_GetType( psz_name ) )
    {
        case VLC_VAR_STRING:
        {
            char *psz = config_GetPsz( psz_name );
            js_pushstring( J, psz );
            free( psz );
            return;
        }

        case VLC_VAR_INTEGER:
            js_pushnumber( J, config_GetInt( psz_name ) );
            return;

        case VLC_VAR_BOOL:
            js_pushboolean( J, config_GetInt( psz_name ) );
            return;

        case VLC_VAR_FLOAT:
            js_pushnumber( J, config_GetFloat( psz_name ) );
            return;

        default:
            js_error( J, "unknown property" );
    }
}

static void vlcjs_config_set( js_State *J )
{
    const char *psz_name = js_tostring( J, 1 );
    switch( config_GetType( psz_name ) )
    {
        case VLC_VAR_STRING:
            config_PutPsz( psz_name, js_tostring( J, 2 ) );
            break;

        case VLC_VAR_INTEGER:
            config_PutInt( psz_name, js_tointeger( J, 2 ) );
            break;

        case VLC_VAR_BOOL:
            config_PutInt( psz_name, js_toboolean( J, 2 ) );
            break;

        case VLC_VAR_FLOAT:
            config_PutFloat( psz_name, js_tonumber( J, 2 ) );
            break;

        default:
            js_error( J, "error assigning value" );
    }
    js_pushundefined( J );
}

/*****************************************************************************
 * Directories configuration
 *****************************************************************************/
static void vlcjs_datadir( js_State *J )
{
    char *psz_data = config_GetSysPath(VLC_PKG_DATA_DIR, NULL);
    js_pushstring( J, psz_data );
    free( psz_data );
}

static void vlcjs_userdatadir( js_State *J )
{
    char *dir = config_GetUserDir( VLC_USERDATA_DIR );
    js_pushstring( J, dir );
    free( dir );
}

static void vlcjs_homedir( js_State *J )
{
    char *home = config_GetUserDir( VLC_HOME_DIR );
    js_pushstring( J, home );
    free( home );
}

static void vlcjs_configdir( js_State *J )
{
    char *dir = config_GetUserDir( VLC_CONFIG_DIR );
    js_pushstring( J, dir );
    free( dir );
}

static void vlcjs_cachedir( js_State *J )
{
    char *dir = config_GetUserDir( VLC_CACHE_DIR );
    js_pushstring( J, dir );
    free( dir );
}


static void vlcjs_datadir_list( js_State *J )
{
    const char *psz_dirname = js_tostring( J, 1 );
    char **ppsz_dir_list = NULL;
    int i = 0;

    if( vlcjs_dir_list( psz_dirname, &ppsz_dir_list ) != VLC_SUCCESS )
    {
        js_pushnull( J );
        return;
    }
    js_newarray( J );
    for( char **ppsz_dir = ppsz_dir_list; *ppsz_dir; ppsz_dir++ )
    {
        js_pushstring( J, *ppsz_dir );
        js_setindex( J, -2, i++ );
    }
    vlcjs_dir_list_free( ppsz_dir_list );
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_config_reg[] = {
    { "get", vlcjs_config_get },
    { "set", vlcjs_config_set },
    { "datadir", vlcjs_datadir },
    { "userdatadir", vlcjs_userdatadir },
    { "homedir", vlcjs_homedir },
    { "configdir", vlcjs_configdir },
    { "cachedir", vlcjs_cachedir },
    { "datadir_list", vlcjs_datadir_list },
    { NULL, NULL }
};

void vlcjs_register_config( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_config_reg );
    js_setproperty( J, -2, "config" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

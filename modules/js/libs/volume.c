/*****************************************************************************
 * volume.c
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Pierre d'Herbemont <pdherbemont # videolan.org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <math.h>
#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_meta.h>
#include <vlc_playlist.h>
#include <vlc_player.h>
#include <vlc_aout.h>

#include "general.h"

/*****************************************************************************
 * Volume related
 *****************************************************************************/
static void vlcjs_volume_set( js_State *J )
{
    vlc_player_t *player = vlcjs_get_player( J );

    int i_volume = js_tointeger( J, 1 );
    if (i_volume < 0) i_volume = 0; // if negative volume provided, set it to 0

    float volume = i_volume / (float) AOUT_VOLUME_DEFAULT;
    vlc_player_aout_SetVolume( player, volume );
    js_pushundefined( J );
}

static void vlcjs_volume_get( js_State *J )
{
    vlc_player_t *player = vlcjs_get_player( J );

    float volume = vlc_player_aout_GetVolume(player);

    long i_volume = lroundf(volume * AOUT_VOLUME_DEFAULT);
    js_pushnumber( J, i_volume );
}

static void vlcjs_volume_up( js_State *J )
{
    vlc_player_t *player = vlcjs_get_player( J );

    float volume;
    int steps;
    // steps = arg[1] if provided else 1
    if( js_isdefined( J, 1 ) && js_isnumber( J, 1) ) steps = js_tointeger( J, 1 );
    else steps = 1;

    int res = vlc_player_aout_IncrementVolume(player, steps, &volume);

    long i_volume = ( res == VLC_SUCCESS ) ? lroundf(volume * AOUT_VOLUME_DEFAULT) : 0;
    js_pushnumber( J, i_volume );
}

static void vlcjs_volume_down( js_State *J )
{
    vlc_player_t *player = vlcjs_get_player( J );

    float volume;
    int steps;

    // steps = arg[1] if provided else 1
    if( js_isdefined( J, 1 ) && js_isnumber( J, 1) ) steps = js_tointeger( J, 1 );
    else steps = 1;

    int res = vlc_player_aout_DecrementVolume(player, steps, &volume);

    long i_volume = res == VLC_SUCCESS ? lroundf(volume * AOUT_VOLUME_DEFAULT) : 0;
    js_pushnumber( J, i_volume );
}

/*****************************************************************************
 * register volume object
 *****************************************************************************/
static const vlcjs_Reg vlcjs_volume_reg[] = {
    { "get", vlcjs_volume_get },
    { "set", vlcjs_volume_set },
    { "up", vlcjs_volume_up },
    { "down", vlcjs_volume_down },
    { NULL, NULL }
};

void vlcjs_register_volume( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_volume_reg );
    js_setproperty( J, -2, "volume" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

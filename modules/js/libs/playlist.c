/*****************************************************************************
 * playlist.c
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"

static void vlcjs_playlist_prev(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    vlc_playlist_Lock(playlist);
    vlc_playlist_Prev(playlist);
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_next(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    vlc_playlist_Lock(playlist);
    vlc_playlist_Next(playlist);
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_skip(js_State *J)
{
    int n = js_tointeger( J, 1 );
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    vlc_playlist_Lock(playlist);
    if (n < 0) {
        for (int i = 0; i < -n; i++)
            vlc_playlist_Prev(playlist);
    } else {
        for (int i = 0; i < n; ++i)
            vlc_playlist_Next(playlist);
    }
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_play(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    vlc_playlist_Lock(playlist);
    
    if (vlc_playlist_GetCurrentIndex(playlist) == -1 &&
            vlc_playlist_Count(playlist) > 0)
        vlc_playlist_GoTo(playlist, 0);
    vlc_playlist_Start(playlist);
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_pause(js_State *J)
{
    /* this is in fact a toggle pause */
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    vlc_player_t *player = vlcjs_get_player(J);

    vlc_player_Lock(player);
    if (vlc_player_GetState(player) != VLC_PLAYER_STATE_PAUSED)
        vlc_player_Pause(player);
    else
        vlc_player_Resume(player);
    vlc_player_Unlock(player);
    js_pushundefined(J);
}

static void vlcjs_playlist_stop(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    vlc_playlist_Lock(playlist);
    vlc_playlist_Stop(playlist);
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_clear( js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    vlc_playlist_Lock(playlist);
    vlc_playlist_Clear(playlist);
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_repeat_helper(js_State *J,
                               enum vlc_playlist_playback_repeat enabled_mode)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    int top = js_gettop(J);
    if (top > 1)
    {
        js_error(J, "wrong arguments");
        return;
    }

    vlc_playlist_Lock(playlist);

    bool enable;
    if (top == 0)
    {
        /* no value provided, toggle the current */
        enum vlc_playlist_playback_repeat repeat =
                vlc_playlist_GetPlaybackRepeat(playlist);
        enable = repeat != enabled_mode;
    }
    else
    {
        /* use the provided value */
        enable = js_toboolean(J, 1);
    }

    enum vlc_playlist_playback_repeat new_repeat = enable
                                    ? enabled_mode
                                    : VLC_PLAYLIST_PLAYBACK_REPEAT_NONE;

    vlc_playlist_SetPlaybackRepeat(playlist, new_repeat);

    vlc_playlist_Unlock(playlist);

    js_pushboolean(J, enable);
}

static void vlcjs_playlist_repeat(js_State *J)
{
    vlcjs_playlist_repeat_helper(J, VLC_PLAYLIST_PLAYBACK_REPEAT_CURRENT);
}

static void vlcjs_playlist_loop(js_State *J)
{
    return vlcjs_playlist_repeat_helper(J, VLC_PLAYLIST_PLAYBACK_REPEAT_ALL);
}

static void vlcjs_playlist_get_repeat(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    enum vlc_playlist_playback_repeat repeat =
            vlc_playlist_GetPlaybackRepeat(playlist);
    bool result = repeat != VLC_PLAYLIST_PLAYBACK_REPEAT_NONE;
    vlc_playlist_Unlock(playlist);

    js_pushboolean(J, result);
}

static void vlcjs_playlist_get_loop(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    enum vlc_playlist_playback_repeat repeat =
            vlc_playlist_GetPlaybackRepeat(playlist);
    bool result = repeat == VLC_PLAYLIST_PLAYBACK_REPEAT_ALL;
    vlc_playlist_Unlock(playlist);

    js_pushboolean(J, result);
}

static void vlcjs_playlist_random(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    int top = js_gettop(J);
    if (top > 1)
    {
        js_error(J, "wrong arguments");
        return;
    }

    vlc_playlist_Lock(playlist);

    bool enable;
    if (top == 0)
    {
        enum vlc_playlist_playback_order order =
                vlc_playlist_GetPlaybackOrder(playlist);
        enable = order != VLC_PLAYLIST_PLAYBACK_ORDER_RANDOM;
    }
    else
    {
        /* use the provided value */
        enable = js_toboolean(J, 1);
    }

    enum vlc_playlist_playback_order new_order = enable
                                    ? VLC_PLAYLIST_PLAYBACK_ORDER_RANDOM
                                    : VLC_PLAYLIST_PLAYBACK_ORDER_NORMAL;

    vlc_playlist_SetPlaybackOrder(playlist, new_order);

    vlc_playlist_Unlock(playlist);

    js_pushboolean(J, enable);
}

static void vlcjs_playlist_get_random(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    enum vlc_playlist_playback_order order =
            vlc_playlist_GetPlaybackOrder(playlist);
    bool result = order == VLC_PLAYLIST_PLAYBACK_ORDER_RANDOM;
    vlc_playlist_Unlock(playlist);

    js_pushboolean(J, result);
}

static void vlcjs_playlist_gotoitem(js_State *J)
{
    uint64_t id = js_tointeger(J, 1);
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    ssize_t index = vlc_playlist_IndexOfId(playlist, id);
    if (index == -1)
    {
        vlc_playlist_Unlock(playlist);
        js_error(J, "no item with given id");
        return;
    }
    else
    {
        vlc_playlist_GoTo(playlist, index);
    }
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_delete(js_State *J)
{
    uint64_t id = js_tointeger(J, 1);
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    ssize_t index = vlc_playlist_IndexOfId(playlist, id);
    if( index != -1 )
    {
        vlc_playlist_RemoveOne(playlist, index);
    }
    else
    {
        vlc_playlist_Unlock(playlist);
        js_error(J, "no item with given id");
        return;
    }
    
    vlc_playlist_Unlock(playlist);
    js_pushundefined(J);
}

static void vlcjs_playlist_move(js_State *J)
{
    uint64_t item_id = js_tointeger(J, 1);
    uint64_t target_id = js_tointeger(J, 2);
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    ssize_t item_index = vlc_playlist_IndexOfId(playlist, item_id);
    ssize_t target_index = vlc_playlist_IndexOfId(playlist, target_id);
    if (item_index == -1 || target_index == -1)
    {
        vlc_playlist_Unlock(playlist);
        js_error(J, "wrong id");
        return;
    }
    else
    {
        /* if the current item was before the target, moving it shifts the
         * target item by one */
        size_t new_index = item_index <= target_index ? target_index
                                                      : target_index + 1;
        vlc_playlist_MoveOne(playlist, item_index, new_index);
    }
    vlc_playlist_Unlock(playlist);

    js_pushundefined(J);
}

static void vlcjs_playlist_add_common(js_State *J, bool play)
{
    vlc_object_t *obj = vlcjs_get_this(J);
    vlc_playlist_t *playlist = vlcjs_get_playlist(J);
    int count = 0, i;

    /* playlist */
    if (!js_isarray(J, -1))
    {
        msg_Warn(obj, "Playlist items should be in an array.");
        return;
    }

    vlc_playlist_Lock(playlist);

    /* playlist nil */
    for(i = 0; i < js_getlength(J, 1); i++)
    {
        js_getindex(J, 1, i);
        input_item_t *item = vlcjs_read_input_item(obj, J);
        if (item != NULL)
        {
            int ret = vlc_playlist_AppendOne(playlist, item);
            if (ret == VLC_SUCCESS)
            {
                count++;
                if (play)
                {
                    size_t last = vlc_playlist_Count(playlist) - 1;
                    vlc_playlist_PlayAt(playlist, last);
                }
            }
            input_item_Release(item);
        }
        /* pop the value, keep the key for the next js_next() call */
        js_pop(J, 1);
    }
    /* playlist */

    vlc_playlist_Unlock(playlist);

    js_pushnumber(J, count);
}

static void vlcjs_playlist_add(js_State *J)
{
    vlcjs_playlist_add_common(J, true);
}

static void vlcjs_playlist_enqueue(js_State *J)
{
    vlcjs_playlist_add_common(J, false);
}

static void push_playlist_item(js_State *J, vlc_playlist_item_t *item)
{
    js_newobject(J);

    js_pushnumber(J, vlc_playlist_item_GetId(item));
    js_setproperty(J, -2, "id");

    input_item_t *media = vlc_playlist_item_GetMedia(item);

    /* Apart from nb_played, these fields unfortunately duplicate
       fields already available from the input item */
    char *name = input_item_GetTitleFbName(media);
    js_pushstring(J, name);
    free(name);
    js_setproperty(J, -2, "name");

    js_pushstring(J, media->psz_uri);
    js_setproperty(J, -2, "path");

    if( media->i_duration < 0 )
        js_pushnumber(J, -1);
    else
        js_pushnumber(J, secf_from_vlc_tick(media->i_duration));
    js_setproperty(J, -2, "duration");

    // jsopen_input_item(J, media);
}

static void vlcjs_playlist_get(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );
    uint64_t item_id = js_tointeger(J, 1);

    vlc_playlist_Lock(playlist);
    ssize_t index = vlc_playlist_IndexOfId(playlist, item_id);
    vlc_playlist_item_t *item = index != -1 ? vlc_playlist_Get(playlist, index)
                                            : NULL;
    if (item)
        push_playlist_item(J, item);
    else
        js_pushnull(J);
    vlc_playlist_Unlock(playlist);
}

static void vlcjs_playlist_list(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);

    size_t count = vlc_playlist_Count(playlist);
    js_newarray(J);

    for (size_t i = 0; i < count; ++i)
    {
        push_playlist_item(J, vlc_playlist_Get(playlist, i));
        js_setindex(J, -2, i);
    }

    vlc_playlist_Unlock(playlist);
    // array item is at top of stack and returned automatically
}

static void vlcjs_playlist_current(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    ssize_t current = vlc_playlist_GetCurrentIndex(playlist);
    int id;
    if (current != -1)
    {
        vlc_playlist_item_t *item = vlc_playlist_Get(playlist, current);
        id = vlc_playlist_item_GetId(item);
    }
    else
        id = -1;
    vlc_playlist_Unlock(playlist);

    js_pushnumber(J, id);
}

static void vlcjs_playlist_current_item(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    vlc_playlist_Lock(playlist);
    ssize_t index = vlc_playlist_GetCurrentIndex(playlist);
    vlc_playlist_item_t *item = index != -1 ? vlc_playlist_Get(playlist, index)
                                            : NULL;
    if (item)
        push_playlist_item(J, item);
    else
        js_pushnull(J);
    vlc_playlist_Unlock(playlist);
}

static bool vlc_sort_key_from_string(const char *keyname,
                                     enum vlc_playlist_sort_key *key)
{
    static const struct
    {
        const char *keyname;
        enum vlc_playlist_sort_key key;
    } map[] = {
        { "title",    VLC_PLAYLIST_SORT_KEY_TITLE },
        { "artist",   VLC_PLAYLIST_SORT_KEY_ARTIST },
        { "genre",    VLC_PLAYLIST_SORT_KEY_GENRE },
        { "duration", VLC_PLAYLIST_SORT_KEY_DURATION },
        { "album",    VLC_PLAYLIST_SORT_KEY_ALBUM },
    };
    for (size_t i = 0; i < ARRAY_SIZE(map); ++i)
    {
        if (!strcmp(keyname, map[i].keyname))
        {
            *key = map[i].key;
            return true;
        }
    }
    return false;
}

static void vlcjs_playlist_sort( js_State *J )
{
    vlc_playlist_t *playlist = vlcjs_get_playlist( J );

    const char *keyname = js_tostring(J, 1);

    if (!strcmp(keyname, "random"))
    {
        /* sort randomly -> shuffle */
        vlc_playlist_Lock(playlist);
        vlc_playlist_Shuffle(playlist);
        vlc_playlist_Unlock(playlist);
    }
    else
    {
        struct vlc_playlist_sort_criterion criterion;
        if (!vlc_sort_key_from_string(keyname, &criterion.key))
        {
            js_error(J, "Invalid search key.");
            return;
        }
        criterion.order = js_toboolean(J, 2)
                        ? VLC_PLAYLIST_SORT_ORDER_DESCENDING
                        : VLC_PLAYLIST_SORT_ORDER_ASCENDING;

        vlc_playlist_Lock(playlist);
        vlc_playlist_Sort(playlist, &criterion, 1);
        vlc_playlist_Unlock(playlist);
    }
    js_pushundefined(J);
}

static void vlcjs_playlist_status(js_State *J)
{
    vlc_playlist_t *playlist = vlcjs_get_playlist(J);

    vlc_player_t *player = vlcjs_get_player(J);
    vlc_player_Lock(player);
    enum vlc_player_state state = vlc_player_GetState(player);
    vlc_player_Unlock(player);

    switch (state)
    {
        case VLC_PLAYER_STATE_STOPPED:
            js_pushstring(J, "stopped");
            break;
        case VLC_PLAYER_STATE_STARTED:
            js_pushstring(J, "started");
            break;
        case VLC_PLAYER_STATE_PLAYING:
            js_pushstring(J, "playing");
            break;
        case VLC_PLAYER_STATE_PAUSED:
            js_pushstring(J, "paused");
            break;
        case VLC_PLAYER_STATE_STOPPING:
            js_pushstring(J, "stopping");
            break;
        default:
            js_pushstring(J, "unknown");
    }
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_playlist_reg[] = {
    { "prev", vlcjs_playlist_prev },
    { "next", vlcjs_playlist_next },
    { "skip", vlcjs_playlist_skip },
    { "play", vlcjs_playlist_play },
    { "pause", vlcjs_playlist_pause },
    { "stop", vlcjs_playlist_stop },
    { "clear", vlcjs_playlist_clear },
    { "repeat", vlcjs_playlist_repeat },
    { "loop", vlcjs_playlist_loop },
    { "random", vlcjs_playlist_random },
    { "get_repeat", vlcjs_playlist_get_repeat },
    { "get_loop", vlcjs_playlist_get_loop },
    { "get_random", vlcjs_playlist_get_random },
    { "gotoitem", vlcjs_playlist_gotoitem },
    { "add", vlcjs_playlist_add },
    { "enqueue", vlcjs_playlist_enqueue },
    { "get", vlcjs_playlist_get },
    { "list", vlcjs_playlist_list },
    { "current", vlcjs_playlist_current },
    { "current_item", vlcjs_playlist_current_item },
    { "sort", vlcjs_playlist_sort },
    { "status", vlcjs_playlist_status },
    { "delete", vlcjs_playlist_delete },
    { "move", vlcjs_playlist_move },
    { NULL, NULL }
};

void vlcjs_register_playlist( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_playlist_reg );
    js_setproperty( J, -2, "playlist" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

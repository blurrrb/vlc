/*****************************************************************************
 * video.c: Generic js interface functions
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"

/*****************************************************************************
 * Vout control, using new vlc_player api
 *****************************************************************************/
static void vlcjs_fullscreen( js_State *J )
{
    vlc_player_t *player = vlcjs_get_player( J );
    if( !player )
    {
        js_error( J, "error getting player" );
        return;
    }
    
    if( js_gettop( J ) == 1 )
        vlc_player_vout_ToggleFullscreen( player );
    else
    {
        const char *s = js_tostring( J, 1 );

        if( s && !strcmp(s, "on") )
            vlc_player_vout_SetFullscreen( player, true );
        else if( s && !strcmp(s, "off") )
            vlc_player_vout_SetFullscreen( player, false );
    }
    js_pushboolean( J, vlc_player_vout_IsFullscreen( player ) );
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_video_reg[] = {
    { "fullscreen", vlcjs_fullscreen },
    { NULL, NULL }
};

void vlcjs_register_video( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_video_reg );
    js_setproperty( J, -2, "video" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

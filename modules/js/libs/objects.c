/*****************************************************************************
 * objects.c: Generic js<->vlc object wrapper
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"

/*****************************************************************************
 * Generic vlc_object_t wrapper creation
 *****************************************************************************/

static void vlcjs_push_object(js_State *J, vlc_object_t *p_obj,
                                  int (*release)(js_State *))
{
    vlc_object_t **udata =
        (vlc_object_t **) malloc(sizeof (vlc_object_t *));
    if( !udata ) return js_error( J, "memory error" );

    *udata = p_obj;

    js_pushundefined( J );
    js_newuserdata( J, "vlc_object", udata, release);
}

static void vlcjs_objects_get_libvlc( js_State *J )
{
    libvlc_int_t *p_libvlc = vlc_object_instance(vlcjs_get_this( J ));
    vlcjs_push_object(J, p_libvlc, NULL);
}

static void vlcjs_objects_get_playlist( js_State *J )
{
    vlc_playlist_t *playlist = vlcjs_get_playlist(J);
    vlcjs_push_object(J, playlist, NULL);
}

static void vlcjs_objects_get_player( js_State *J )
{
    vlc_player_t *player = vlcjs_get_player(J);
    vlcjs_push_object(J, player, NULL);
}

static void vlcjs_vout_release(js_State *J, void *data)
{
    vlc_object_t **pp = data;

    vout_Release((vout_thread_t *)*pp);
}

static void vlcjs_objects_get_vout( js_State *J )
{
    vout_thread_t *vout = vlcjs_get_vout(J);
    vlcjs_push_object(J, vout, vlcjs_vout_release);
}

static void vlcjs_aout_release(js_State *J, void *data)
{
    vlc_object_t **pp = data;
    aout_Release((audio_output_t *)*pp);
}

static void vlcjs_objects_get_aout( js_State *J )
{
    audio_output_t *aout = vlcjs_get_aout(J);
    vlcjs_push_object(J, aout, vlcjs_aout_release);
}

/*****************************************************************************
 *
 *****************************************************************************/
static const vlcjs_Reg vlcjs_object_reg[] = {
    { "playlist", vlcjs_objects_get_playlist },
    { "player", vlcjs_objects_get_player },
    { "libvlc", vlcjs_objects_get_libvlc },
    { "vout", vlcjs_objects_get_vout},
    { "aout", vlcjs_objects_get_aout},
    { NULL, NULL }
};

void vlcjs_register_objects( js_State *J )
{
    js_getglobal(J, "vlc");
    js_newobject(J);
    vlcjs_register(J, vlcjs_object_reg);
    js_setproperty(J, -2, "objects");
    js_pop(J, 1);
}

/*****************************************************************************
 * Copyright (C) 2009-2019 VideoLAN and authors
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "concurrency.h"
#include <vlc_threads.h>

/***********************************************
 * queue data structure
 ***********************************************/


// is called form another thread. All other functions run on the extension thread
void vlcjs_registerCallback( js_State *J, char *fn_ref, char *this_ref, char *data_ref )
{
    /* this method is equivalent to queue.enqueue */
    vlcjs_queue *queue = vlcjs_get( J, "queue" );
    vlcjs_queue_element *new_element = malloc( sizeof( vlcjs_queue_element ) );
    if( !new_element )
    {
        js_error( J, "memory error" );
        return;
    }
    new_element->fn = fn_ref;
    new_element->this = this_ref; // can be null
    new_element->data = data_ref; // can be null
    new_element->next = NULL;

    vlc_mutex_lock( &queue->lock );
    if( queue->back == NULL && queue->front == NULL )
    {
        // queue is empty
        queue->front = queue->back = new_element;
    }
    else
    {
        queue->back->next = new_element;
        queue->back = new_element;    
    }
    vlc_cond_signal( &queue->cond );
    vlc_mutex_unlock( &queue->lock );
}


void vlcjs_setupLoop( js_State *J, vlc_mutex_t *exec_lock )
{
    vlcjs_queue *queue = (vlcjs_queue *) malloc( sizeof( vlcjs_queue ) );
    if( !queue )
    {
        msg_Err( vlcjs_get_this( J ), "memory error" );
        return;
    }
    queue->front = queue->back = NULL;
    queue->stop_loop = false;
    vlc_mutex_init( &queue->lock );
    vlc_cond_init( &queue->cond );
    queue->execution_lock = exec_lock;
    vlcjs_set( J, queue, "queue" );
}

// should never be invoked explicitely
void vlcjs_serviceCallback( js_State *J )
{
    vlcjs_queue *queue = vlcjs_get( J, "queue" );
    vlc_mutex_lock( &queue->lock );
    vlcjs_queue_element *element_to_service = queue->front;
    if( queue->front == queue->back )
        // last element serviced
        queue->front = queue->back = NULL;
    else
        queue->front = queue->front->next;
    vlc_mutex_unlock( &queue->lock );

    // service the event and free it from memory
    js_getregistry( J, (char *) element_to_service->fn );

    if( element_to_service->this != NULL )
        js_getregistry( J, (char *) element_to_service->this );
    else
        js_pushundefined( J );

    if( element_to_service->data != NULL)    
        js_getregistry( J, (char *) element_to_service->data );
    else
        js_pushnull( J );
    

    if( queue->execution_lock ) vlc_mutex_lock( queue->execution_lock );
    if( js_pcall( J, 1 ) )
        msg_Info( vlcjs_get_this( J ), js_tostring( J, -1 ) );
    js_pop( J, 1 ); // pop the returned object from stack
    if( queue->execution_lock ) vlc_mutex_unlock( queue->execution_lock );

    free(element_to_service);

}

void *vlcjs_Loop( void *data )
{
    js_State *J = data;
    vlcjs_queue *queue = vlcjs_get( J, "queue" );
    msg_Info(vlcjs_get_this(J), "ins loop");

    while( true )
    {
        vlc_mutex_lock( &queue->lock );

        if( queue->front == NULL && queue->back == NULL && !queue->stop_loop )
            vlc_cond_wait( &queue->cond, &queue->lock );
        if( queue->stop_loop )
        {
            vlc_mutex_unlock( &queue->lock );
            break;
        }
        vlc_mutex_unlock( &queue->lock );
        vlcjs_serviceCallback( J );
    }
    return NULL;
}

void vlcjs_enterLoop( js_State *J )
{
    vlcjs_queue *queue = vlcjs_get( J, "queue" );
    vlc_clone( &queue->thread, vlcjs_Loop, J, VLC_THREAD_PRIORITY_LOW );
}

void vlcjs_terminateLoop( js_State *J )
{
    // stops loop immediately and frees all calbacks waiting to be 
    vlcjs_queue *queue = vlcjs_get( J, "queue" );
    vlc_mutex_lock( &queue->lock );
    queue->stop_loop = true;
    vlc_mutex_unlock( &queue->lock );
    vlc_cond_signal( &queue->cond );

    vlc_join( queue->thread, NULL );

    // at this point the loop will have stopped and the thread destroyed.
    // now begin freeing elements in the queue from memory

    vlcjs_queue_element *p_ele = queue->front, *next;
    while( p_ele != NULL )
    {
        next = p_ele->next;
        free( p_ele );
        p_ele = next;
    }

    vlc_cond_destroy( &queue->cond );
    vlc_mutex_destroy( &queue->lock );
    free( queue );
}
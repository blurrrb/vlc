/*****************************************************************************
 * vlm.c: Generic JS VLM wrapper
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include <vlc_vlm.h>

#include "general.h"

/*****************************************************************************
 *
 *****************************************************************************/
#ifdef ENABLE_VLM
static void vlcjs_vlm_delete( js_State *, void *data );
static void vlcjs_vlm_execute_command( js_State * );

static const vlcjs_Reg vlcjs_vlm_reg[] = {
    { "execute_command", vlcjs_vlm_execute_command },
    { NULL, NULL }
};

static void vlcjs_vlm_new( js_State *J )
{
    vlc_object_t *p_this = vlcjs_get_this( J );
    vlm_t *p_vlm = vlm_New( vlc_object_instance(p_this), NULL );
    if( !p_vlm )
        return js_error( J, "Cannot start VLM." );

    vlm_t **pp_vlm = malloc( sizeof( vlm_t * ) );
    if( !pp_vlm ) return js_error( J, "memory error" );
    js_pushundefined( J );
    *pp_vlm = p_vlm;
    js_newuserdata( J, "vlm", pp_vlm, vlcjs_vlm_delete );

    vlcjs_register( J, vlcjs_vlm_reg );
    
    // user data object is returned
}

static void vlcjs_vlm_delete( js_State *J, void *data )
{
    vlm_t **pp_vlm = data;
    vlm_Delete( *pp_vlm );
    *pp_vlm = NULL;
}

static void push_message( js_State *J, vlm_message_t *message )
{
    js_newobject( J );
    js_pushstring( J, message->psz_name );
    js_setproperty( J, -2, "name" );
    if( message->i_child > 0 )
    {
        int i;
        js_newarray( J );
        for( i = 0; i < message->i_child; i++ )
        {
            push_message( J, message->child[i] );
            js_setindex( J, -2, i );
        }
        js_setproperty( J, -2, "children" );
    }
    if ( message->psz_value )
    {
        js_pushstring( J, message->psz_value );
        js_setproperty( J, -2, "value" );
    }
    // object at top of the stack is returned
}

static void vlcjs_vlm_execute_command( js_State *J )
{
    vlm_t **pp_vlm = (vlm_t**)js_touserdata( J, 0, "vlm" );
    const char *psz_command = js_tostring( J, 1 );
    vlm_message_t *message;
    int i_ret;
    i_ret = vlm_ExecuteCommand( *pp_vlm, psz_command, &message );
    push_message( J, message );
    vlm_MessageDelete( message );
    if( i_ret != VLC_SUCCESS )
        return js_error( J, "error" );
}

#else
static void vlcjs_vlm_new( js_State *J )
{
    return js_error( J, "Cannot start VLM because it was disabled when compiling VLC." );
}
#endif

/*****************************************************************************
 *
 *****************************************************************************/
void vlcjs_register_vlm( js_State *J )
{
    js_getglobal( J, "vlc" );
    js_newcfunction( J, vlcjs_vlm_new, "vlm", 0 );
    js_setproperty( J, -2, "vlm" );
    js_pop( J, 1 );
}

/*****************************************************************
 * js <-> c functions
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Hugo Beauzée-Luyssen <hugo@beauzee.fr>
 *          Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <errno.h>
#include <math.h>
#include <stdlib.h>

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_charset.h>
#include <vlc_input.h>
#include <vlc_plugin.h>
#include <vlc_player.h>
#include <vlc_playlist.h>
#include <vlc_vout.h>
#include <vlc_aout.h>
#include <vlc_meta.h>
#include <vlc_interface.h>
#include <vlc_actions.h>
#include <vlc_interrupt.h>
#include <vlc_rand.h>
#include <vlc_fixups.h>
#include <vlc_httpd.h>
#include <vlc_fs.h>
#include <vlc_stream.h>
#include <vlc_stream_extractor.h>
#include <vlc_memstream.h>

#include <sys/stat.h>

#include "../js.h"


/****************************************************
 * general functions and data structures
 ****************************************************/

void vlcjs_set(js_State*, void*, const char*);
void* vlcjs_get( js_State*, const char*);

void vlcjs_set_this( js_State*, void * );
vlc_object_t* vlcjs_get_this( js_State* );

vlc_playlist_t *vlcjs_get_playlist( js_State* );
vlc_player_t *vlcjs_get_player( js_State* );
vout_thread_t *vlcjs_get_vout( js_State *);
audio_output_t *vlcjs_get_aout( js_State *);

typedef struct
{
    const char* fn_name;
    void ( *fn )( js_State* );
} vlcjs_Reg;

void vlcjs_register( js_State*, const vlcjs_Reg* );

/****************************************************
 * extra functions used internally
 ****************************************************/

int vlcjs_dir_list( const char *, char *** );
void vlcjs_dir_list_free( char ** );
char *vlcjs_find_file( const char *, const char * );

void vlcjs_read_options( vlc_object_t *, js_State *, int *, char *** );
void vlcjs_read_meta_data( vlc_object_t *, js_State *, input_item_t * );
void vlcjs_read_custom_meta_data( vlc_object_t *, js_State *, input_item_t *);

input_item_t *vlcjs_read_input_item(vlc_object_t *, js_State *);

/*****************************************************************************
 * Will execute func on all scripts in jsdirname, and stop if func returns
 * success.
 *****************************************************************************/
int vlcjs_scripts_batch_execute( vlc_object_t *,
                                  const char *,
                                  int (*func)(vlc_object_t *, const char *));
char *vlcjs_find_file( const char *, const char *);
int vlcjs_dofile( vlc_object_t *, js_State *, const char * );


int vlcjs_fd_init( js_State *, vlcjs_dtable_t * );
void vlcjs_fd_interrupt( vlcjs_dtable_t * );
void vlcjs_fd_cleanup( vlcjs_dtable_t * );
struct vlc_interrupt *vlcjs_set_interrupt( js_State * );

/**********************************************************************************
 * misc
 * *******************************************************************************/
int vlcjs_add_modules_path( js_State *J, const char *psz_filename );
/*****************************************************************************
 * Messaging facilities
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef  _GNU_SOURCE
#   define  _GNU_SOURCE
#endif

#include "general.h"

static void vlcjs_msg_dbg( js_State *J )
{
    int i, i_top = js_gettop( J );
    vlc_object_t *p_this = vlcjs_get_this( J );
    for( i = 1; i < i_top; i++ )
        msg_Dbg( p_this, "%s", js_tostring( J, i ) );
    js_pushundefined( J );
}

static void vlcjs_msg_warn( js_State *J )
{
    int i, i_top = js_gettop( J );
    vlc_object_t *p_this = vlcjs_get_this( J );
    for( i = 1; i < i_top; i++ )
        msg_Warn( p_this, "%s", js_tostring( J, i ) );
    js_pushundefined( J );
}

static void vlcjs_msg_err( js_State *J )
{
    int i, i_top = js_gettop( J );
    vlc_object_t *p_this = vlcjs_get_this( J );
    for( i = 1; i < i_top; i++ )
        msg_Err( p_this, "%s", js_tostring( J, i ) );
    js_pushundefined( J );
}

static void vlcjs_msg_info( js_State *J )
{
    int i, i_top = js_gettop( J );
    vlc_object_t *p_this = vlcjs_get_this( J );
    for( i = 1; i < i_top; i++ )
        msg_Info( p_this, "%s", js_tostring( J, i ) );
    js_pushundefined( J );
}

/*****************************************************************************
 * Registring the functions
 *****************************************************************************/

const vlcjs_Reg vlcjs_msg_reg[] = {
    { "dbg", vlcjs_msg_dbg },
    { "warn", vlcjs_msg_warn },
    { "err", vlcjs_msg_err },
    { "info", vlcjs_msg_info },
    { NULL, NULL }
};

void vlcjs_register_msg( js_State *J )
{
    js_getglobal( J, "vlc" ); // push the vlc object to stack
    js_newobject( J );
    vlcjs_register( J, vlcjs_msg_reg );
    js_setproperty( J, -2, "msg" );
    js_pop( J, 1 ); // pop the vlc object from stack
}

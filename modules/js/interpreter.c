/*****************************************************************************
 * interpreter.c: A JS interpreter to test VLCJS bindings
 *****************************************************************************
 * Copyright (C) 2009-2019 VideoLAN and authors
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>,
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <unistd.h>

#include "libs/concurrency.h"

typedef struct
{
    js_State *J;
    vlc_mutex_t execution_lock;
    vlc_thread_t thread;
} vlcjs_sys_t;

void print( js_State *J )
{
    fprintf(stdout, "%s\n", js_tostring(J, 1));
    js_pushundefined( J );
}

static void *Run( void *data )
{
    intf_thread_t *p_intf = data;
    vlcjs_sys_t *p_sys = p_intf->p_sys;
    js_State *J = p_sys->J;
    vlc_mutex_lock( &p_sys->execution_lock );
    char buff[1024];
    vlc_tick_sleep(10000);

    fprintf( stdout, "Welcome to JS Interpreter\n");

    // code to test out event loop
    js_getglobal( J, "print" );
    char *fn = strdup(js_ref( J ));
    if( !fn ){
        vlc_mutex_unlock( &p_sys->execution_lock );
        return;
    }
    char *n;
    int i;
    vlcjs_enterLoop( J );
    for(i=0;i<10;i++)
    {
        js_pushnumber( J, i);
        n = strdup(js_ref( J ));
        if( !n ) continue;
        vlcjs_registerCallback( J, fn, NULL, n);
    }
    vlc_mutex_unlock( &p_sys->execution_lock );

    vlcjs_queue *queue = vlcjs_get( J, "queue" );

    while( true )
    {   
        fprintf( stdout, "> ");
        fgets( buff, 1024, stdin );
        if( queue->stop_loop ){
            return NULL;
        }
        if( strcmp(buff, "exit\n") == 0)
        {
            break;
        }
        vlc_mutex_lock( &p_sys->execution_lock );
        js_dostring( J, buff );
        vlc_mutex_unlock( &p_sys->execution_lock );
    }

    return NULL;
}


int Open_JS_Intf( vlc_object_t *obj )
{
    intf_thread_t *p_intf = (intf_thread_t*)obj;
    js_State *J;

    vlcjs_sys_t *p_sys = malloc( sizeof(vlcjs_sys_t) );

    J = js_newstate( NULL, NULL, JS_STRICT );
    
    if( !J )
    {
        msg_Err( p_intf, "Could not create new JS State" );
        free( p_sys );
        return VLC_ENOMEM;
    }
    p_sys->J = J;
    vlc_mutex_init( &p_sys->execution_lock );
    p_intf->p_sys = p_sys;
    
    /* set the runtime object */
    vlcjs_set_this( J, p_intf );
    vlcjs_setupLoop( J, &p_sys->execution_lock );

    /* register the "vlc" object inside js runtime */

    js_newobject( J );
    js_setglobal( J, "vlc" );

    /* register submodules */
    vlcjs_register_msg( J );
    vlcjs_register_volume( J );
    vlcjs_register_io( J );
    vlcjs_register_config( J );
    vlcjs_register_video( J );
    // vlcjs_register_misc( J );
    vlcjs_register_strings( J );

    js_newcfunction( J, print, "print", 0);
    js_setglobal( J, "print" );

    vlc_clone( &p_sys->thread, Run, p_intf, VLC_THREAD_PRIORITY_LOW );
    
    return VLC_SUCCESS;
}

void Close_JS_Intf( vlc_object_t *p_this )
{
    intf_thread_t *p_intf = (intf_thread_t*)p_this;
    vlcjs_sys_t *p_sys = p_intf->p_sys;
    vlcjs_terminateLoop( p_sys->J );
    vlc_join( p_sys->thread, NULL );
    vlc_mutex_destroy( &p_sys->execution_lock );
    free( p_sys );
}
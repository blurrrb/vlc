/*****************************************************************************
 * extension.c: JS Extensions (meta data, web information, ...)
 *****************************************************************************
 * Copyright (C) 2007-2019 the VideoLAN team
 *
 * Authors: Antoine Cellerier <dionoea at videolan tod org>,
 *          Thomas Guillem <tguillem at videolan dot org>,
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "libs/general.h"
#include "extension.h"
#include "assert.h"

#include <vlc_common.h>
#include <vlc_interface.h>
#include <vlc_events.h>
#include <vlc_dialog.h>

/* Functions to register */
static const vlcjs_Reg p_reg[] =
{
    { NULL, NULL }
};

/*
 * Extensions capabilities
 * Note: #define and ppsz_capabilities must be in sync
 */
static const char caps[][20] = {
#define EXT_HAS_MENU          (1 << 0)   ///< Hook: menu
    "menu",
#define EXT_TRIGGER_ONLY      (1 << 1)   ///< Hook: trigger. Not activable
    "trigger",
#define EXT_INPUT_LISTENER    (1 << 2)   ///< Hook: input_changed
    "input-listener",
#define EXT_META_LISTENER     (1 << 3)   ///< Hook: meta_changed
    "meta-listener",
#define EXT_PLAYING_LISTENER  (1 << 4)   ///< Hook: status_changed
    "playing-listener",
};

static int ScanExtensions( extensions_manager_t *p_this );
static int ScanJSCallback( vlc_object_t *p_this, const char *psz_script);
static int Control( extensions_manager_t *, int, va_list );
static int GetMenuEntries( extensions_manager_t *p_mgr, extension_t *p_ext,
                    char ***pppsz_titles, uint16_t **ppi_ids );
static js_State* GetJSState( extensions_manager_t *p_mgr,
                               extension_t *p_ext );
static int TriggerMenu( extension_t *p_ext, int id );
static int TriggerExtension( extensions_manager_t *p_mgr,
                             extension_t *p_ext );
static void WatchTimerCallback( void* );

static void vlcjs_extension_deactivate( js_State *J );
static void vlcjs_extension_keep_alive( js_State *J );

/* Interactions */
static int vlcjs_extension_dialog_callback( vlc_object_t *p_this,
                                             char const *psz_var,
                                             vlc_value_t oldval,
                                             vlc_value_t newval,
                                             void *p_data );

/* Input item callback: vlc_InputItemMetaChanged */
static void inputItemMetaChanged( const vlc_event_t *p_event,
                                  void *data );


/**
 * Module entry-point
 **/
int Open_Extension( vlc_object_t *p_this )
{
    msg_Dbg( p_this, "Opening JS Extension module" );

    extensions_manager_t *p_mgr = ( extensions_manager_t* ) p_this;


    vlc_mutex_init( &p_mgr->lock );

    /* Scan available JS Extensions */
    if( ScanExtensions( p_mgr ) != VLC_SUCCESS )
    {
        msg_Err( p_mgr, "Can't load extensions modules" );
        return VLC_EGENERIC;
    }

    // Create the dialog-event variable
    var_Create( p_this, "dialog-event", VLC_VAR_ADDRESS );
    var_AddCallback( p_this, "dialog-event",
                     vlcjs_extension_dialog_callback, NULL );

    p_mgr->pf_control = Control;
    p_mgr->p_sys = NULL;
    return VLC_SUCCESS;
}

/**
 * Module unload function
 **/
void Close_Extension( vlc_object_t *p_this )
{
    extensions_manager_t *p_mgr = ( extensions_manager_t* ) p_this;

    var_DelCallback( p_this, "dialog-event",
                     vlcjs_extension_dialog_callback, NULL );
    var_Destroy( p_mgr, "dialog-event" );

    extension_t *p_ext = NULL;

    /* Free extensions' memory */
    ARRAY_FOREACH( p_ext, p_mgr->extensions )
    {
        if( !p_ext )
            break;

        vlc_mutex_lock( &p_ext->p_sys->command_lock );
        if( p_ext->p_sys->b_activated == true && p_ext->p_sys->p_progress_id == NULL )
        {
            p_ext->p_sys->b_exiting = true;
            // QueueDeactivateCommand will signal the wait condition.
            QueueDeactivateCommand( p_ext );
        }
        else
        {
            if ( p_ext->p_sys->J != NULL )
                vlcjs_fd_interrupt( &p_ext->p_sys->dtable );
            // however here we need to manually signal the wait cond, since no command is queued.
            p_ext->p_sys->b_exiting = true;
            vlc_cond_signal( &p_ext->p_sys->wait );
        }
        vlc_mutex_unlock( &p_ext->p_sys->command_lock );

        if( p_ext->p_sys->b_thread_running == true )
            vlc_join( p_ext->p_sys->thread, NULL );

        /* Clear JS State */
        if( p_ext->p_sys->J )
        {
            js_freestate( p_ext->p_sys->J );
            vlcjs_fd_cleanup( &p_ext->p_sys->dtable );
        }

        free( p_ext->psz_name );
        free( p_ext->psz_title );
        free( p_ext->psz_author );
        free( p_ext->psz_description );
        free( p_ext->psz_shortdescription );
        free( p_ext->psz_url );
        free( p_ext->psz_version );
        free( p_ext->p_icondata );

        vlc_mutex_destroy( &p_ext->p_sys->running_lock );
        vlc_mutex_destroy( &p_ext->p_sys->command_lock );
        vlc_cond_destroy( &p_ext->p_sys->wait );
        vlc_timer_destroy( p_ext->p_sys->timer );

        free( p_ext->p_sys );
        free( p_ext );
    }

    vlc_mutex_destroy( &p_mgr->lock );

    ARRAY_RESET( p_mgr->extensions );
}

/**
 * Batch scan all JS files in folder "extensions"
 * @param p_mgr This extensions_manager_t object
 **/
static int ScanExtensions( extensions_manager_t *p_mgr )
{
    int i_ret =
        vlcjs_scripts_batch_execute( VLC_OBJECT( p_mgr ),
                                      "extensions",
                                      &ScanJSCallback);

    if( !i_ret )
        return VLC_EGENERIC;

    return VLC_SUCCESS;
}

/**
 * Dummy JS function: does nothing
 * @note This function can be used to replace "require" while scanning for
 * extensions
 * Even the built-in libraries are not loaded when calling descriptor()
 **/
static void vlcjs_dummy_require( js_State *J )
{
    js_pushundefined(J);
}

/**
 * Replacement for "require", adding support for packaged extensions
 * @note Loads modules in the modules/ folder of a package
 **/
static void vlcjs_extension_require( js_State *J )
{
    const char *psz_module = js_tostring( J, 1 );
    vlc_object_t *p_this = vlcjs_get_this( J );
    extension_t *p_ext = vlcjs_extension_get( J );
    msg_Dbg( p_this, "loading module '%s' from extension package",
             psz_module );
    char *psz_fullpath, *psz_package, *sep;
    psz_package = strdup( p_ext->psz_name );
    if( !psz_package ){
        return js_error( J, "memory error" );
    }
    sep = strrchr( psz_package, '/' );
    if( !sep )
    {
        free( psz_package );
        return js_error( J, "could not find package name" );
    }
    if( -1 == asprintf( &psz_fullpath,
                        "%s/modules/%s.js", psz_package, psz_module ) )
    {
        free( psz_package );
        return 1;
    }
    int i_ret = vlcjs_dofile( p_this, J, psz_fullpath );
    free( psz_fullpath );
    free( psz_package );
    if( i_ret != 0 )
    {
        return js_error( J, "unable to load module '%s' from package",
                           psz_module );
    }
    js_pushundefined( J );
}

/**
 * Batch scan all JS files in folder "extensions": callback
 * @param p_this This extensions_manager_t object
 * @param psz_filename Name of the script to run
 **/
int ScanJSCallback( vlc_object_t *p_this, const char *psz_filename)
{
    extensions_manager_t *p_mgr = ( extensions_manager_t* ) p_this;
    bool b_ok = false;

    msg_Dbg( p_mgr, "Scanning JS script %s", psz_filename );

    /* Experimental: read .vlcjs packages (Zip archives) */
    char *psz_script = NULL;
    int i_flen = strlen( psz_filename );
    if( !strncasecmp( psz_filename + i_flen - 6, ".vlcjs", 6 ) )
    {
        msg_Dbg( p_this, "reading JS script in a zip archive" );
        psz_script = calloc( 1, i_flen + 6 + 12 + 1 );
        if( !psz_script )
            return 0;
        strcpy( psz_script, "zip://" );
        strncat( psz_script, psz_filename, i_flen + 19 );
        strncat( psz_script, "!/script.js", i_flen + 19 );
    }
    else
    {
        psz_script = strdup( psz_filename );
        if( !psz_script )
            return 0;
    }

    /* Create new script descriptor */
    extension_t *p_ext = ( extension_t* ) calloc( 1, sizeof( extension_t ) );
    if( !p_ext )
    {
        free( psz_script );
        return 0;
    }

    p_ext->psz_name = psz_script;
    p_ext->p_sys = (extension_sys_t*) calloc( 1, sizeof( extension_sys_t ) );
    if( !p_ext->p_sys || !p_ext->psz_name )
    {
        free( p_ext->psz_name );
        free( p_ext->p_sys );
        free( p_ext );
        return 0;
    }
    p_ext->p_sys->p_mgr = p_mgr;

    /* Watch timer */
    if( vlc_timer_create( &p_ext->p_sys->timer, WatchTimerCallback, p_ext ) )
    {
        free( p_ext->psz_name );
        free( p_ext->p_sys );
        free( p_ext );
        return 0;
    }

    /* Mutexes and conditions */
    vlc_mutex_init( &p_ext->p_sys->command_lock );
    vlc_mutex_init( &p_ext->p_sys->running_lock );
    vlc_cond_init( &p_ext->p_sys->wait );

    /* Prepare JS state */
    js_State *J = js_newstate( NULL, NULL, JS_STRICT);
    js_newcfunction( J, vlcjs_extension_require, NULL, 0);
    js_setglobal(J, "require");

    /* Let's run it */
    if( vlcjs_dofile( p_this, J, psz_script ) ) 
    {
        msg_Warn( p_mgr, "Error loading script %s: %s", psz_script,
                  js_tostring( J, -1 ) );
        js_pop( J, 1 );
        goto exit;
    }

    /* Scan script for capabilities */
    js_getglobal( J, "descriptor" );

    if( !js_iscallable( J, -1 ) )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function descriptor() not found", psz_script );
        goto exit;
    }
    
    js_pushundefined( J ); // the this object

    if( js_pcall( J, 0) )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function descriptor(): %s", psz_script,
                  js_tostring( J, -1 ) );
        goto exit;
    }

    if( js_gettop( J ) )
    {
        if( js_isobject( J, -1 ) )
        {
            /* Get caps */
            js_getproperty( J, -1, "capabilities" );
            if( js_isarray( J, -1 ) )
            {
                int i;
                for(i=0; i<js_getlength(J, -1); i++)
                {
                    js_getindex( J, -1, i );
                    const char *psz_cap = js_tostring( J, -1 );
                    bool found = false;
                    /* Find this capability's flag */
                    for( size_t i = 0; i < sizeof(caps)/sizeof(caps[0]); i++ )
                    {
                        if( !strcmp( caps[i], psz_cap ) )
                        {
                            /* Flag it! */
                            p_ext->p_sys->i_capabilities |= 1 << i;
                            found = true;
                            break;
                        }
                    }
                    if( !found )
                    {
                        msg_Warn( p_mgr, "Extension capability '%s' unknown in"
                                  " script %s", psz_cap, psz_script );
                    }
                    /* Removes 'value'; keeps 'key' for next iteration */
                    js_pop( J, 1 );
                }
            }
            else
            {
                msg_Warn( p_mgr, "In script %s, function descriptor() "
                              "did not return a table of capabilities.",
                              psz_script );
            }
            js_pop( J, 1 );

            /* Get title */
            js_getproperty( J, -1, "title" );
            if( js_isstring( J, -1 ) )
            {
                p_ext->psz_title = strdup( js_tostring( J, -1 ) );

            }
            else
            {
                msg_Dbg( p_mgr, "In script %s, function descriptor() "
                                "did not return a string as title.",
                                psz_script );
                p_ext->psz_title = strdup( psz_script );
            }
            js_pop( J, 1 );

            if( !p_ext->psz_title ){
                return VLC_ENOMEM;
            }

            /* Get author */
            js_getproperty( J, -1, "author" );
            p_ext->psz_author = js_tostring( J, -1 );
            js_pop( J, 1 );

            /* Get description */
            js_getproperty( J, -1, "description" );
            p_ext->psz_description = js_tostring( J, -1 );
            js_pop( J, 1 );

            /* Get short description */
            js_getproperty( J, -1, "shortdesc" );
            p_ext->psz_shortdescription = js_tostring( J, -1 );
            js_pop( J, 1 );

            /* Get URL */
            js_getproperty( J, -1, "url" );
            p_ext->psz_url = js_tostring( J, -1 );
            js_pop( J, 1 );

            /* Get version */
            js_getproperty( J, -1, "version" );
            p_ext->psz_version = js_tostring( J, -1 );
            js_pop( J, 1 );

            /* Get icon data */
            js_getproperty( J, -1, "icon" );
            if( !js_isnull( J, -1 ) && js_isstring( J, -1 ) )
            {
                char *icon_path = js_tostring( J, -1 );
                int len = strlen( icon_path );
                p_ext->p_icondata = malloc( len );
                if( p_ext->p_icondata )
                {
                    p_ext->i_icondata_size = len;
                    memcpy( p_ext->p_icondata, icon_path, len );
                }
            }
            js_pop( J, 1 );
        }
        else
        {
            msg_Warn( p_mgr, "In script %s, function descriptor() "
                      "did not return a table!", psz_script );
            goto exit;
        }
    }
    else
    {
        msg_Err( p_mgr, "Script %s went completely foobar", psz_script );
        goto exit;
    }

    msg_Dbg( p_mgr, "Script %s has the following capability flags: 0x%x",
             psz_script, p_ext->p_sys->i_capabilities );

    b_ok = true;
exit:
    js_freestate( J );
    if( !b_ok )
    {
        free( p_ext->psz_name );
        free( p_ext->psz_title );
        free( p_ext->psz_url );
        free( p_ext->psz_author );
        free( p_ext->psz_description );
        free( p_ext->psz_shortdescription );
        free( p_ext->psz_version );
        vlc_mutex_destroy( &p_ext->p_sys->command_lock );
        vlc_mutex_destroy( &p_ext->p_sys->running_lock );
        vlc_cond_destroy( &p_ext->p_sys->wait );
        free( p_ext->p_sys );
        free( p_ext );
    }
    else
    {
        /* Add the extension to the list of known extensions */
        ARRAY_APPEND( p_mgr->extensions, p_ext );
    }

    /* Continue batch execution */
    return VLC_EGENERIC;
}

static int Control( extensions_manager_t *p_mgr, int i_control, va_list args )
{
    extension_t *p_ext = NULL;
    bool *pb = NULL;
    uint16_t **ppus = NULL;
    char ***pppsz = NULL;
    int i = 0;

    switch( i_control )
    {
        case EXTENSION_ACTIVATE:
            p_ext = va_arg( args, extension_t* );
            return Activate( p_mgr, p_ext );

        case EXTENSION_DEACTIVATE:
            p_ext = va_arg( args, extension_t* );
            return Deactivate( p_mgr, p_ext );

        case EXTENSION_IS_ACTIVATED:
            p_ext = va_arg( args, extension_t* );
            pb = va_arg( args, bool* );
            vlc_mutex_lock( &p_ext->p_sys->command_lock );
            *pb = p_ext->p_sys->b_activated;
            vlc_mutex_unlock( &p_ext->p_sys->command_lock );
            break;

        case EXTENSION_HAS_MENU:
            p_ext = va_arg( args, extension_t* );
            pb = va_arg( args, bool* );
            *pb = ( p_ext->p_sys->i_capabilities & EXT_HAS_MENU ) ? 1 : 0;
            break;

        case EXTENSION_GET_MENU:
            p_ext = va_arg( args, extension_t* );
            pppsz = va_arg( args, char*** );
            ppus = va_arg( args, uint16_t** );
            if( p_ext == NULL )
                return VLC_EGENERIC;
            return GetMenuEntries( p_mgr, p_ext, pppsz, ppus );

        case EXTENSION_TRIGGER_ONLY:
            p_ext = va_arg( args, extension_t* );
            pb = va_arg( args, bool* );
            *pb = ( p_ext->p_sys->i_capabilities & EXT_TRIGGER_ONLY ) ? 1 : 0;
            break;

        case EXTENSION_TRIGGER:
            p_ext = va_arg( args, extension_t* );
            return TriggerExtension( p_mgr, p_ext );

        case EXTENSION_TRIGGER_MENU:
            p_ext = va_arg( args, extension_t* );
            i = va_arg( args, int );
            return TriggerMenu( p_ext, i );

        case EXTENSION_SET_INPUT:
        {
            p_ext = va_arg( args, extension_t* );
            input_item_t *p_item = va_arg( args, struct input_item_t * );

            if( p_ext == NULL )
                return VLC_EGENERIC;
            vlc_mutex_lock( &p_ext->p_sys->command_lock );
            if ( p_ext->p_sys->b_exiting == true )
            {
                vlc_mutex_unlock( &p_ext->p_sys->command_lock );
                return VLC_EGENERIC;
            }
            vlc_mutex_unlock( &p_ext->p_sys->command_lock );

            vlc_mutex_lock( &p_ext->p_sys->running_lock );

            // Change input
            input_item_t *old = p_ext->p_sys->p_item;
            if( old )
            {
                // Untrack meta fetched events
                if( p_ext->p_sys->i_capabilities & EXT_META_LISTENER )
                {
                    vlc_event_detach( &old->event_manager,
                                      vlc_InputItemMetaChanged,
                                      inputItemMetaChanged,
                                      p_ext );
                }
                input_item_Release( old );
            }

            p_ext->p_sys->p_item = p_item ? input_item_Hold(p_item) : NULL;

            // Tell the script the input changed
            if( p_ext->p_sys->i_capabilities & EXT_INPUT_LISTENER )
            {
                PushCommandUnique( p_ext, CMD_SET_INPUT );
            }

            // Track meta fetched events
            if( p_ext->p_sys->p_item &&
                p_ext->p_sys->i_capabilities & EXT_META_LISTENER )
            {
                vlc_event_attach( &p_item->event_manager,
                                  vlc_InputItemMetaChanged,
                                  inputItemMetaChanged,
                                  p_ext );
            }

            vlc_mutex_unlock( &p_ext->p_sys->running_lock );
            break;
        }
        case EXTENSION_PLAYING_CHANGED:
        {
            p_ext = va_arg( args, extension_t* );
            assert( p_ext->psz_name != NULL );
            i = va_arg( args, int );
            if( p_ext->p_sys->i_capabilities & EXT_PLAYING_LISTENER )
            {
                PushCommand( p_ext, CMD_PLAYING_CHANGED, i );
            }
            break;
        }
        case EXTENSION_META_CHANGED:
        {
            p_ext = va_arg( args, extension_t* );
            PushCommand( p_ext, CMD_UPDATE_META );
            break;
        }
        default:
            msg_Warn( p_mgr, "Control '%d' not yet implemented in Extension",
                      i_control );
            return VLC_EGENERIC;
    }

    return VLC_SUCCESS;
}

int vlcjs_ExtensionActivate( extensions_manager_t *p_mgr, extension_t *p_ext )
{
    assert( p_mgr != NULL && p_ext != NULL );
    return vlcjs_ExecuteFunction( p_mgr, p_ext, "activate", JS_END );
}

int vlcjs_ExtensionDeactivate( extensions_manager_t *p_mgr, extension_t *p_ext )
{
    assert( p_mgr != NULL && p_ext != NULL );

    if( p_ext->p_sys->b_activated == false )
        return VLC_SUCCESS;

    vlcjs_fd_interrupt( &p_ext->p_sys->dtable );

    // Unset and release input objects
    if( p_ext->p_sys->p_item )
    {
        if( p_ext->p_sys->i_capabilities & EXT_META_LISTENER )
            vlc_event_detach( &p_ext->p_sys->p_item->event_manager,
                              vlc_InputItemMetaChanged,
                              inputItemMetaChanged,
                              p_ext );
        input_item_Release(p_ext->p_sys->p_item);
        p_ext->p_sys->p_item = NULL;
    }

    int i_ret = vlcjs_ExecuteFunction( p_mgr, p_ext, "deactivate", JS_END );

    if ( p_ext->p_sys->J == NULL )
        return VLC_EGENERIC;
    js_freestate( p_ext->p_sys->J );
    p_ext->p_sys->J = NULL;

    return i_ret;
}

int vlcjs_ExtensionWidgetClick( extensions_manager_t *p_mgr,
                              extension_t *p_ext,
                              extension_widget_t *p_widget )
{
    if( !p_ext->p_sys->J )
        return VLC_SUCCESS;

    js_State *J = GetJSState( p_mgr, p_ext );
    if( p_widget->p_sys != NULL ){
        js_getregistry( J, (char *) p_widget->p_sys );
        return vlcjs_ExecuteFunction( p_mgr, p_ext, NULL, JS_END );
    }
    return VLC_EGENERIC;
}


/**
 * Get the list of menu entries from an extension script
 * @param p_mgr
 * @param p_ext
 * @param pppsz_titles Pointer to NULL. All strings must be freed by the caller
 * @param ppi_ids Pointer to NULL. Must be freed by the caller.
 * @note This function is allowed to run in the UI thread. This means
 *       that it MUST respond very fast.
 * @todo Remove the menu() hook and provide a new function vlc.set_menu()
 **/
static int GetMenuEntries( extensions_manager_t *p_mgr, extension_t *p_ext,
                    char ***pppsz_titles, uint16_t **ppi_ids )
{
    assert( *pppsz_titles == NULL );
    assert( *ppi_ids == NULL );

    vlc_mutex_lock( &p_ext->p_sys->command_lock );
    if( p_ext->p_sys->b_activated == false || p_ext->p_sys->b_exiting == true )
    {
        vlc_mutex_unlock( &p_ext->p_sys->command_lock );
        msg_Dbg( p_mgr, "Can't get menu of an unactivated/dying extension!" );
        return VLC_EGENERIC;
    }
    vlc_mutex_unlock( &p_ext->p_sys->command_lock );

    vlc_mutex_lock( &p_ext->p_sys->running_lock );

    int i_ret = VLC_EGENERIC;
    js_State *J = GetJSState( p_mgr, p_ext );

    if( ( p_ext->p_sys->i_capabilities & EXT_HAS_MENU ) == 0 )
    {
        msg_Dbg( p_mgr, "can't get a menu from an extension without menu!" );
        goto exit;
    }

    js_getglobal( J, "menu" );

    if( !js_iscallable( J, -1 ) )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function menu() not found", p_ext->psz_name );
        goto exit;
    }

    js_pushundefined( J );

    if( js_pcall( J, 0 ) )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function menu(): %s", p_ext->psz_name,
                  js_tostring( J, -1 ) );
        goto exit;
    }

    if( js_gettop( J ) )
    {
        if( js_isarray( J, -1 ) )
        {
            /* Get table size */
            size_t i_size = js_getlength( J, -1 );
            *pppsz_titles = ( char** ) calloc( i_size+1, sizeof( char* ) );
            *ppi_ids = ( uint16_t* ) calloc( i_size+1, sizeof( uint16_t ) );

            /* Walk table */
            size_t i_idx = 0;
            while( i_idx < i_size )
            {
                js_getindex( J, -1, i_idx );
                if( !js_isstring( J, -1 ) )
                {
                    msg_Warn( p_mgr, "In script %s, an entry in "
                              "the menu table is invalid!", p_ext->psz_name );
                    goto exit;
                }
                (*pppsz_titles)[ i_idx ] = strdup( js_tostring( J, -1 ) );
                (*ppi_ids)[ i_idx ] = i_idx & 0xFFFF;
                js_pop( J, 1 );

                if( !(*pppsz_titles)[ i_idx ] ) return VLC_ENOMEM;
                i_idx++;
            }
        }
        else
        {
            msg_Warn( p_mgr, "Function menu() in script %s "
                      "did not return a list", p_ext->psz_name );
            goto exit;
        }
    }
    else
    {
        msg_Warn( p_mgr, "Script %s went completely foobar", p_ext->psz_name );
        goto exit;
    }

    i_ret = VLC_SUCCESS;

exit:
    vlc_mutex_unlock( &p_ext->p_sys->running_lock );
    if( i_ret != VLC_SUCCESS )
    {
        msg_Dbg( p_mgr, "Something went wrong in %s (%s:%d)",
                 __func__, __FILE__, __LINE__ );
    }
    return i_ret;
}

/* Must be entered with the Lock on Extension */
static js_State* GetJSState( extensions_manager_t *p_mgr,
                               extension_t *p_ext )
{
    assert( p_ext != NULL );
    js_State *J = p_ext->p_sys->J;

    if( !J )
    {
        J = js_newstate(NULL, NULL, JS_STRICT);
        if( !J )
        {
            msg_Err( p_mgr, "Could not create new JS State" );
            return NULL;
        }
        vlcjs_set_this( J, p_mgr );
        intf_thread_t *intf = (intf_thread_t *) vlc_object_parent(p_mgr);
        vlc_playlist_t *playlist = vlc_intf_GetMainPlaylist(intf);
        vlcjs_extension_set( J, p_ext );

        js_newobject( J );
        vlcjs_register( J, p_reg );
        js_setglobal( J, "vlc" );
        vlcjs_register_msg( J );

        /* Load more libraries */
        vlcjs_register_config( J );
        vlcjs_register_dialog( J, p_ext );
        if( vlcjs_fd_init( J, &p_ext->p_sys->dtable ) )
        {
            js_freestate( J );
            return NULL;
        }
        vlcjs_register_objects( J );
        vlcjs_register_osd( J );
        vlcjs_register_playlist( J );
        vlcjs_register_stream( J );
        vlcjs_register_strings( J );
        vlcjs_register_video( J );
        vlcjs_register_vlm( J );
        vlcjs_register_volume( J );
        vlcjs_register_xml( J );
        vlcjs_register_io( J );
        vlcjs_register_errno( J );
        vlcjs_register_rand( J );

        /* Register extension specific functions */
        js_getglobal( J, "vlc" );
        js_newcfunction( J, vlcjs_extension_deactivate, NULL, 0 );
        js_setproperty( J, -2, "deactivate" );
        js_newcfunction( J, vlcjs_extension_keep_alive, NULL, 0 );
        js_setproperty( J, -2, "keep_alive" );

        /* Setup the module search path */
        if( !strncmp( p_ext->psz_name, "zip://", 6 ) )
        {
            /* Load all required modules manually */
            js_newcfunction( J, vlcjs_extension_require, NULL, 0 );
            js_setglobal( J, "require" );
        }
        else
        {
            if( vlcjs_add_modules_path( J, p_ext->psz_name ) )
            {
                msg_Warn( p_mgr, "Error while setting the module "
                          "search path for %s", p_ext->psz_name );
                vlcjs_fd_cleanup( &p_ext->p_sys->dtable );
                js_freestate( J );
                return NULL;
            }
        }
        /* Load and run the script(s) */
        if( vlcjs_dofile( VLC_OBJECT( p_mgr ), J, p_ext->psz_name ) )
        {
            msg_Warn( p_mgr, "Error loading script %s: %s", p_ext->psz_name,
                      js_tostring( J, -1 ) );
            vlcjs_fd_cleanup( &p_ext->p_sys->dtable );
            js_freestate( J );
            return NULL;
        }

        p_ext->p_sys->J = J;
    }

    return J;
}

int vlcjs_ExecuteFunction( extensions_manager_t *p_mgr, extension_t *p_ext,
                            const char *psz_function, ... )
{
    va_list args;
    va_start( args, psz_function );
    int i_ret = vlcjs_ExecuteFunctionVa( p_mgr, p_ext, psz_function, args );
    va_end( args );
    return i_ret;
}

/**
 * Execute a function in a JS script
 * @param psz_function Name of global function to execute. If NULL, assume
 *                     that the function object is already on top of the
 *                     stack.
 * @return < 0 in case of failure, >= 0 in case of success
 * @note It's better to call this function from a dedicated thread
 * (see extension_thread.c)
 **/
int vlcjs_ExecuteFunctionVa( extensions_manager_t *p_mgr, extension_t *p_ext,
                           const char *psz_function, va_list args )
{
    int i_ret = VLC_SUCCESS;
    int i_args = 0;
    assert( p_mgr != NULL );
    assert( p_ext != NULL );

    js_State *J = GetJSState( p_mgr, p_ext );
    if( !J )
        return -1;

    if( psz_function )
        js_getglobal( J, psz_function );

    if( !js_iscallable( J, -1 ) )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function %s() not found", p_ext->psz_name, psz_function );
        js_pop( J, 1 );
        goto exit;
    }

    js_pushundefined( J ); // this object

    js_datatype_e type = JS_END;
    while( ( type = va_arg( args, int ) ) != JS_END )
    {
        if( type == JS_NUM )
        {
            js_pushnumber( J , va_arg( args, int ) );
        }
        else if( type == JS_TEXT )
        {
            js_pushstring( J , va_arg( args, char* ) );
        }
        else
        {
            msg_Warn( p_mgr, "Undefined argument type %d to js function %s"
                   "from script %s", type, psz_function, p_ext->psz_name );
            if( i_args > 0 )
                js_pop( J, i_args );
            goto exit;
        }
        i_args ++;
    }

    // Start actual call to JS
    if( js_pcall( J, i_args ) )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function %s(): %s", p_ext->psz_name, psz_function,
                  js_tostring( J, -1 ) );
        i_ret = VLC_EGENERIC;
    }

    i_ret |= vlcjs_DialogFlush( J );

exit:
    return i_ret;

}

static inline int TriggerMenu( extension_t *p_ext, int i_id )
{
    return PushCommand( p_ext, CMD_TRIGGERMENU, i_id );
}

int vlcjs_ExtensionTriggerMenu( extensions_manager_t *p_mgr,
                              extension_t *p_ext, int id )
{
    int i_ret = VLC_SUCCESS;
    js_State *J = GetJSState( p_mgr, p_ext );

    if( !J )
        return VLC_EGENERIC;

    vlcjs_register_dialog( J, p_ext );

    js_getglobal( J, "trigger_menu" );
    if( !js_iscallable( J, -1 ) )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function trigger_menu() not found", p_ext->psz_name );
        return VLC_EGENERIC;
    }
    js_pushundefined( J ); // this object

    /* Pass id as unique argument to the function */
    js_pushnumber( J, id );

    if( js_pcall( J, 1 ) != 0 )
    {
        msg_Warn( p_mgr, "Error while running script %s, "
                  "function trigger_menu(): %s", p_ext->psz_name,
                  js_tostring( J, -1 ) );
        i_ret = VLC_EGENERIC;
    }

    i_ret |= vlcjs_DialogFlush( J );
    if( i_ret < VLC_SUCCESS )
    {
        msg_Dbg( p_mgr, "Something went wrong in %s (%s:%d)",
                 __func__, __FILE__, __LINE__ );
    }

    return i_ret;
}

/** Directly trigger an extension, without activating it
 * This is NOT multithreaded, and this code runs in the UI thread
 * @param p_mgr
 * @param p_ext Extension to trigger
 * @return Value returned by the js function "trigger"
 **/
static int TriggerExtension( extensions_manager_t *p_mgr,
                             extension_t *p_ext )
{
    int i_ret = vlcjs_ExecuteFunction( p_mgr, p_ext, "trigger", JS_END );

    /* Close js state for trigger-only extensions */
    if( p_ext->p_sys->J )
    {
        vlcjs_fd_cleanup( &p_ext->p_sys->dtable );
        js_freestate( p_ext->p_sys->J );
    }
    p_ext->p_sys->J = NULL;

    return i_ret;
}

/** Set extension associated to the current script
 * @param J current js_State
 * @param p_ext the extension
 */
void vlcjs_extension_set( js_State *J, extension_t *p_ext )
{
    js_newuserdata( J, "void*", p_ext, NULL);
    js_setregistry( J, "p_ext" );
}

/** Retrieve extension associated to the current script
 * @param J current js_State
 * @return Extension pointer
 **/
extension_t *vlcjs_extension_get( js_State *J )
{
    js_getregistry( J, "p_ext" );
    extension_t *p_ext = (extension_t*) js_touserdata( J, -1, "void*" );
    js_pop( J, 1 );
    return p_ext;
}

/** Deactivate an extension by order from the extension itself
 * @param J js_State
 * @note This is an asynchronous call. A script calling vlc.deactivate() will
 * be executed to the end before the last call to deactivate() is done.
 **/
void vlcjs_extension_deactivate( js_State *J )
{
    extension_t *p_ext = vlcjs_extension_get( J );
    vlc_mutex_lock( &p_ext->p_sys->command_lock );
    QueueDeactivateCommand( p_ext );
    vlc_mutex_unlock( &p_ext->p_sys->command_lock );
    js_pushundefined( J );
}

/** Keep an extension alive. This resets the watch timer to 0
 * @param J js_State
 * @note This is the "vlc.keep_alive()" function
 **/
static void vlcjs_extension_keep_alive( js_State *J )
{
    extension_t *p_ext = vlcjs_extension_get( J );

    vlc_mutex_lock( &p_ext->p_sys->command_lock );
    if( p_ext->p_sys->p_progress_id != NULL )
    {
        vlc_dialog_release( p_ext->p_sys->p_mgr, p_ext->p_sys->p_progress_id );
        p_ext->p_sys->p_progress_id = NULL;
    }
    vlc_timer_schedule( p_ext->p_sys->timer, false, WATCH_TIMER_PERIOD,
                        VLC_TIMER_FIRE_ONCE );
    vlc_mutex_unlock( &p_ext->p_sys->command_lock );

    js_pushundefined( J );
}

/** Callback for the variable "dialog-event"
 * @param p_this Current object owner of the extension and the dialog
 * @param psz_var "dialog-event"
 * @param oldval Unused
 * @param newval Address of the dialog
 * @param p_data Unused
 **/
static int vlcjs_extension_dialog_callback( vlc_object_t *p_this,
                                             char const *psz_var,
                                             vlc_value_t oldval,
                                             vlc_value_t newval,
                                             void *p_data )
{
    /* psz_var == "dialog-event" */
    ( void ) psz_var;
    ( void ) oldval;
    ( void ) p_data;

    extension_dialog_command_t *command = newval.p_address;
    assert( command != NULL );
    assert( command->p_dlg != NULL);

    extension_t *p_ext = command->p_dlg->p_sys;
    assert( p_ext != NULL );

    extension_widget_t *p_widget = command->p_data;

    switch( command->event )
    {
        case EXTENSION_EVENT_CLICK:
            assert( p_widget != NULL );
            PushCommandUnique( p_ext, CMD_CLICK, p_widget );
            break;
        case EXTENSION_EVENT_CLOSE:
            PushCommandUnique( p_ext, CMD_CLOSE );
            break;
        default:
            msg_Dbg( p_this, "Received unknown UI event %d, discarded",
                     command->event );
            break;
    }

    return VLC_SUCCESS;
}

/** Callback on vlc_InputItemMetaChanged event
 **/
static void inputItemMetaChanged( const vlc_event_t *p_event,
                                  void *data )
{
    assert( p_event && p_event->type == vlc_InputItemMetaChanged );

    extension_t *p_ext = ( extension_t* ) data;
    assert( p_ext != NULL );

    PushCommandUnique( p_ext, CMD_UPDATE_META );
}

/** Watch timer callback
 * The timer expired, JS may be stuck, ask the user what to do now
 **/
static void WatchTimerCallback( void *data )
{
    extension_t *p_ext = data;
    extensions_manager_t *p_mgr = p_ext->p_sys->p_mgr;

    vlc_mutex_lock( &p_ext->p_sys->command_lock );

    for( struct command_t *cmd = p_ext->p_sys->command;
         cmd != NULL;
         cmd = cmd->next )
        if( cmd->i_command == CMD_DEACTIVATE )
        {   /* We have a pending Deactivate command... */
            if( p_ext->p_sys->p_progress_id != NULL )
            {
                vlc_dialog_release( p_mgr, p_ext->p_sys->p_progress_id );
                p_ext->p_sys->p_progress_id = NULL;
            }
            KillExtension( p_mgr, p_ext );
            vlc_mutex_unlock( &p_ext->p_sys->command_lock );
            return;
        }

    if( p_ext->p_sys->p_progress_id == NULL )
    {
        p_ext->p_sys->p_progress_id =
            vlc_dialog_display_progress( p_mgr, true, 0.0,
                                         _( "Yes" ),
                                         _( "Extension not responding!" ),
                                         _( "Extension '%s' does not respond.\n"
                                         "Do you want to kill it now? " ),
                                         p_ext->psz_title );
        if( p_ext->p_sys->p_progress_id == NULL )
        {
            KillExtension( p_mgr, p_ext );
            vlc_mutex_unlock( &p_ext->p_sys->command_lock );
            return;
        }
        vlc_timer_schedule( p_ext->p_sys->timer, false, VLC_TICK_FROM_MS(100),
                            VLC_TIMER_FIRE_ONCE );
    }
    else
    {
        if( vlc_dialog_is_cancelled( p_mgr, p_ext->p_sys->p_progress_id ) )
        {
            vlc_dialog_release( p_mgr, p_ext->p_sys->p_progress_id );
            p_ext->p_sys->p_progress_id = NULL;
            KillExtension( p_mgr, p_ext );
            vlc_mutex_unlock( &p_ext->p_sys->command_lock );
            return;
        }
        vlc_timer_schedule( p_ext->p_sys->timer, false, VLC_TICK_FROM_MS(100),
                            VLC_TIMER_FIRE_ONCE );
    }
    vlc_mutex_unlock( &p_ext->p_sys->command_lock );
}

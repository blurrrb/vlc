/*****************************************************************************
 * youtube.js: playlist parser for youtube
 *****************************************************************************
 * Copyright (C) 2019 VideoLAN and authors
 *
 * Authors: Thomas Guillem <tguillem at videolan dot org>,
 *          Aakash Singh <17aakashsingh1999 at gmail dot com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

function extract(string, pattern, value){
    if(!value) value = '$1';
    var res = string.match(pattern);
    if(!res) return null;
    return res[0].replace(pattern, value);
}

function get_url_param(url, name){
    return extract(url, RegExp('[?&]' + name + '=([^&]*)'));
}


function get_fmt(fmt_list){
    var prefres = 720;
    if(prefres < 0) return null;
    
    var fmts = fmt_list.split(',');
    var fmt;
    for(var i in fmts){
        var itag = extract(fmts[i], /(\d+)\/\d+x\d+/);
        var height = parseInt(extract(fmts[i], /\d+\/\d+x(\d+)/));
        fmt = itag;
        if(height <= prefres) break;
    }
    return fmt;
}

function pick_url(url_map, fmt){
    var streams = url_map.split(',');
    var url;
    for(var i in streams){
        var stream = streams[i];
        var itag = extract(stream, /itag=(\d+)/)
        if(itag && itag == fmt){
            url = extract(stream, /url=([^&,]+)/);
            if(url){
                url = vlc.strings.decode_uri(url);
                return url;
            }
        }
    }
    return url;
}

function probe(){
    var res = (
        (vlc.access == 'http' || vlc.access == 'https') &&
        (
            /www\.youtube\.com/.test(vlc.path) ||
            /gaming\.youtube\.com/.test(vlc.path)
        ) &&
        (
            /\/watch\?/.test(vlc.path) ||
            /\/live$/.test(vlc.path) ||
            /\/live\?/.test(vlc.path) ||
            /\/get_video_info\?/.test(vlc.path) ||
            /\/v\//.test(vlc.path) ||
            /\/embed\//.test(vlc.path)
        )
    );
    return res;
}

function parse(){
    if(/^gaming\.youtube\.com/.test(vlc.path)){
        var url = vlc.path.replace(/^gaming\.youtube\.com/, 'www.youtube.com');
        return [ {path: vlc.access + '://' + url} ];
    }
    if(/\/watch\?/.test(vlc.path) || /\/live$/.test(vlc.path) || /\/live\?/.test(vlc.path)){
        var fmt = get_url_param(vlc.path, 'fmt');
        var line, description, arturl, artist, fmt_list, path, url_map, path, name;
        while(true){
            line = vlc.readline();
            if(line == null) break;

            if(/<meta property="og:title"/.test(line)){
                name = extract(line, /content="(.+)"/);
                name = vlc.strings.resolve_xml_special_chars(name);
            }

            if(/<meta property="og:image"/.test(line)){
                arturl = extract(line, /content="([^"]*)"/);
                arturl = vlc.strings.resolve_xml_special_chars(arturl);
            }
            if(/"author": *"([^"]*)"/.test(line)){
                artist = extract(line, /"author": *"([^"]*)"/)
            }
            if(/ytplayer\.config/.test(line)){
                if(!fmt){
                    fmt_list = extract(line, /"fmt_list": *"([^"]*)"/);

                    if(fmt_list){
                        fmt_list = fmt_list.replace(/\\\//g, '/');
                        fmt = get_fmt(fmt_list);
                    }
                }
                url_map = extract(line, /"url_encoded_fmt_stream_map": *"([^"]*)"/);
                if(url_map){
                    url_map = url_map.replace(/\\u0026/g, '&');
                    path = pick_url(url_map, fmt);
                }
                if(!path){
                    var hlsvp = extract(line, /\\"hlsManifestUrl\\": *\\"([^"]*)\\"/);
                    
                    if(hlsvp){
                        path = hlsvp.replace(/\\\//g, '/');
                    }
                }
            }
        }
        vlc.msg.info('artist: ' + artist, 'name: ' + name, 'arturl: ' + arturl, 'path: ' + path);
        return [
            {
                path    : path,
                artist  : artist,
                name    : name,
                arturl  : arturl
            }
        ];
    }
}
